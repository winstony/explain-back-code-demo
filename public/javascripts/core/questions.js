var Questions = Class.create({
	initialize: function(data) {
		this.selectedId = '';
		this.wsRoutes = data.wsRoutes;

		this.answerURL = data.answerURL || '';
		this.getExplanationURL = data.getExplanationURL || '';

		this.questionBodyId = data.questionBodyId || 'question-body';
		this.questionBodyCorrectId = data.questionBodyCorrectId || 'question-body-correct';
		this.questionBodyAnswerId = data.questionBodyAnswerId || 'question-body-answer';

		this.answerButtonId = data.answerButtonId || 'q_answer_button';
		this.nextQuestionId = data.nextQuestionId || 'q_next_button';

		this.explanationButtonId = data.explanationButtonId || 'notification-bar-explanation';
		this.notificationCloseButtonId = data.notificationCloseButtonId || 'notification-bar-close';
		this.notificationBarId = data.notificationBarId || 'notification-bar';

		this.questionAreaId = data.questionAreaId || 'question-area';
		this.questionOverlayId = data.questionOverlayId || 'question-overlay';
		this.questionSectionId = data.questionSectionId || 'question-section';
		this.questionExplanationId = data.questionExplanationId || 'explanation-section';

		this.questionRulesId = data.questionRulesId || 'question-rules';
		this.questionTagsId = data.questionTagsId || 'question-tags';

		this.statsLockedId = data.statsLockedId || 'stats-locked-id';
		this.registerNowId = data.registerNowId || 'register-now-id';
		this.menuRightId = data.menuRightId || 'menu-right-id';
		this.navRightId = data.navRightId || 'cbp-spmenu-s2';

		this.notificationResultCode = '';
		this.notificationCurrentClassName = '';
		this.notificationCurrentText = '';
		this.showExplanation = 0;
		this.explanationData = null;

		this.niceGapClassName = data.niceGapClassName || 'nice-gap';
		this.headerLoaderClassName = data.headerLoaderClassName || 'header-loader';

		this.loggedIn = data.loggedIn;
		if (wsRoutes) {
			this.wsstatistics(wsRoutes.controllers.core.StatisticsWS);
		}
	//	this.showHardAnswerId = 'hq_show_hard_button';
	//	this.checkHardAnswerId = 'hq_check_hard_button';

		this.init();
	},
	showStats: function(data) {
		if (typeof data.r != 'undefined' && 
			typeof data.w != 'undefined' && 
			typeof data.s != 'undefined') {
			console.log("sdfsdf");
			[$("stats_right"),$("menu_right")].each(function(itm){
				console.log("1");
				itm.update(data.r);
			});
			[$("stats_wrong"),$("menu_wrong")].each(function(itm){
				itm.update(data.w);
			});
			[$("stats_skipped"),$("menu_skipped")].each(function(itm){
				itm.update(data.s);
			});
		}
	},
	wsstatistics: function(routes) {
		var ref = this;
		if (this.loggedIn) {
			var wsStats = new WebSocket(routes.indexWS().webSocketURL());
			wsStats.onmessage = function(msg) {
				try { 
					ref.showStats(msg.data.evalJSON());
				} catch(ex) {
					// @TODO: add javascript validation or logging 
					console.log(ex);
					console.log(msg);
				}
			};
			wsStats.onclose = function(msg) {
				console.log(msg);
			};
			wsStats.onerror = function(msg) {
				console.log(msg);
			};
		}
	},
	init: function() {
		this.selectAnswer();
		this.listenAnswerButton();
		this.listenExplanationButton();
		this.listenNotificationCloseButton();
		this.listenMenuRight();
		this.listenRegisterNow();

//		this.listenSubmitButtonWithForm( this.checkHardAnswerId, 'check_hard_answer_form');
//		this.listenSubmitButtonWithForm( this.showHardAnswerId, 'show_hard_answer_form');
//		this.listenSubmitButtonWithForm( this.nextQuestionId, 'next_quest_form');
	},
	listenRegisterNow: function(){
		 if ($(this.registerNowId) && $(this.statsLockedId)) {
			 var regs = $(this.registerNowId).select('a');
			 var ref = this;
			 regs.each(function(reg) {
				 reg.observe('mouseover', function() {
					 if (!$(ref.statsLockedId).hasClassName('icon-unlock'))
					 	$(ref.statsLockedId).addClassName('icon-unlock');
				 });
				 reg.observe('mouseout', function() {
					 if ($(ref.statsLockedId).hasClassName('icon-unlock'))
					 	$(ref.statsLockedId).removeClassName('icon-unlock');
				 });
			 });
		 }

	},
	listenMenuRight:function() {
		if ($(this.menuRightId) && $(this.navRightId)) {
			var ref = this;
			var cl = "cbp-spmenu-open";
			$(this.menuRightId).observe('click',function(e) {
				e.stop();
				if ($(ref.navRightId).hasClassName(cl)) {
					$(ref.navRightId).removeClassName(cl);
					$(ref.menuRightId).select('i')[0].setStyle({
						color: "#82D9CE"
					});
				} else {
					$(ref.menuRightId).select('i')[0].setStyle({
						color: "#ffffff"
					});
					$(ref.navRightId).addClassName(cl);
				}

			});
		}
	},
	selectAnswer: function() {
		if ($('answers')) {
			var answerDivs = $('answers').select('div');
			//console.log(answerDivs);
			var ref = this;
			answerDivs.each(function(div) {
				div.observe('click',function(e) {
					e.stop();
					var sel_id = this.id;//.replace(/z_/i,'')

					var updateQuestionFunction = null;
					var updateQuestionDetails = div.select('text#choice-text')[0].innerHTML.split(',');
					var detailIndex = 0;

					var addAnswerShadowFunction = function(id) {
						$(id).addClassName('answer_shadow');
						ref.circleChoose(true,id);
						updateQuestionFunction = ref.fillGaps;
					}

					if (sel_id != ref.selectedId) {
						$(sel_id.replace(/z_/i,'')).checked = true;

						if ($(ref.selectedId)) {
							$(ref.selectedId).removeClassName('answer_shadow');
							$(ref.selectedId.replace(/z_/i,'')).checked = false;
							ref.circleChoose(false,ref.selectedId);
						}

						addAnswerShadowFunction(sel_id);
						ref.selectedId = sel_id;
					} else {
						$(ref.selectedId.replace(/z_/i,'')).checked = !$(ref.selectedId.replace(/z_/i,'')).checked;
						if ($(ref.selectedId.replace(/z_/i,'')).checked) {
							addAnswerShadowFunction(ref.selectedId);
						} else {
							$(ref.selectedId).removeClassName('answer_shadow');
							ref.circleChoose(false,ref.selectedId);
							updateQuestionFunction = ref.clearGaps;
						}
					}
					if (updateQuestionFunction != null)
						$(ref.questionBodyId).
							select('strong').
							each(function(strong){
								updateQuestionFunction(ref, strong, updateQuestionDetails[detailIndex]);
								++detailIndex;
							});
				});
			});
		}
	},
	clearGaps: function(ref, strongElement, detail) {
		strongElement.addClassName(ref.niceGapClassName);
		strongElement.update('_');
	},
	fillGaps: function(ref, strongElement, detail) {
		strongElement.update(detail);
		strongElement.removeClassName(ref.niceGapClassName);

	},
	circleChoose: function(bool,selectedId) {
		var span = $(selectedId).select("span")[0];
		var answerButton = $(this.answerButtonId);

		if (bool) {
			if (!answerButton.hasClassName("circle")) answerButton.addClassName("circle");
			span.removeClassName('not_choosed');
			span.addClassName('choosed');
		} else {
			if (answerButton.hasClassName("circle")) answerButton.removeClassName("circle");
			span.removeClassName('choosed');
			span.addClassName('not_choosed');
		}
	},
	listenAnswerButton: function() {
		if ($(this.answerButtonId)) {
			var ref = this;
			var questionOverlay = $(this.questionOverlayId);
			var notificationBar = $(this.notificationBarId);
			$(this.answerButtonId).observe('click',function(e) {
				if (ref.answerURL) {
					ref.showExplanation = 0;
					ref.explanationData = null;
					//console.log($('form').serialize(true));
					if (questionOverlay) questionOverlay.show();
					if (notificationBar) notificationBar.addClassName(ref.headerLoaderClassName);
					new Ajax.Request(ref.answerURL,{
						method:'POST',
						parameters: JSON.stringify($('form').serialize(true)),
						contentType: "application/json",
						requestHeaders: {Accept: 'application/json'},
						onSuccess: function(transport){
							try {
								var result = transport.responseJSON;

								if (typeof result.status != 'undefined') {
									ref.notificationResultCode = result.status;
									ref.notificationCurrentText = (result.message ? result.message : '');
									ref.notificationCurrentClassName = result.status == -1 ? 'warning' : (result.status == 1 ? 'success' : 'danger');
									ref.showExplanation = result.status == 0 ? 1 : 0;
									ref.toggleNotifications(result.status,1);
								}

								ref.toggleNotificationBarExplanation(result.show_rules);

								if (result.show_rules == 1) {
									ref.explanationData = result;
									ref.showExplanationArea();
								}
								if (typeof result.question != 'undefined' &&
									typeof result.answers != 'undefined') {
									if ($(ref.questionBodyId)) $(ref.questionBodyId).update(result.question);
									if ($(ref.answerButtonId).hasClassName("circle")) $(ref.answerButtonId).removeClassName("circle");
									if ($('answers')) $('answers').update(result.answers);
									/**
									 * @toDo make live action available (jQuery?)
									 */
									ref.selectAnswer();
								}
							} catch (exc) {
								console.log(ref.answerURL);
							}
							if (questionOverlay){
								setTimeout(function(){
									if (notificationBar) notificationBar.removeClassName(ref.headerLoaderClassName);
									questionOverlay.hide();
								}, 250);
							}
						},
						onFailure: function(transport){
							console.log(transport.responseText);
							alert('Something went wrong...');
						}
					});
				}
			});
		}
	},
	showExplanationArea: function() {
		if (this.explanationData != null) {
			var html = '';
			var result = this.explanationData;
			/**
			 * @toDo fix this overlay call
			 */
			//if (questionOverlay) questionOverlay.hide();
			var correctArea = $(this.questionBodyCorrectId);
			if (correctArea) {
				if (result.correct_answer) correctArea.update(result.correct_answer);
			}
			var answerArea = $(this.questionBodyAnswerId);
			if (answerArea) {
				if (result.prev_answer) answerArea.update(result.prev_answer);
			}

			if ($(this.questionRulesId)) {
				html = '';
				result.rules.each(function(rule){
					html += '<li>' + rule + '</li>';
				});
				$(this.questionRulesId).update(html);
			}
			if ($(this.questionTagsId)) {
				html = '';
				result.tags.each(function(tag){
					html += '<a class="q_tag" href="#">' + tag + '</a>';
				});
				$(this.questionTagsId).update(html);
			}
			if ($(this.questionSectionId)) $(this.questionSectionId).hide();
			if ($(this.questionExplanationId)) $(this.questionExplanationId).show();
		} else {
			throw new Error("Explanation Data is EMPTY");
		}
	},
	toggleNotificationBarExplanation:function(show) {
		var notificationBarExplanation = $(this.explanationButtonId);

		if (notificationBarExplanation) {
			["gumby-icon-doc-text-inv","gumby-icon-doc-text"].each(function(name) {
				if (notificationBarExplanation.hasClassName(name)) {
					notificationBarExplanation.removeClassName(name);
				}
			});

			if (typeof show != 'undefined') {
				switch (show) {
					case 1:
						notificationBarExplanation.addClassName("gumby-icon-doc-text");
						break;
					case 0:
						notificationBarExplanation.addClassName("gumby-icon-doc-text-inv");
						break;
				}
				notificationBarExplanation.show();
			} else {
				notificationBarExplanation.hide();
			}
		}
	},
	hideNotificationBar:function() {
		this.toggleNotifications(undefined, 1);
	},
	toggleNotifications:function(show, closeBar) {
		var ref = this;
		var notificationBar = $(this.notificationBarId);
		var notificationBarText = $('notification-bar-text');
		var notificationBarClose = $('notification-bar-close');
		var notificationBarExplanation = $(this.explanationButtonId);

		var questionArea = $(this.questionAreaId);

		if (notificationBar && questionArea) {
			if (typeof show != 'undefined') {
				//fix if already visible
				notificationBarClose.show();
				notificationBarText.update(this.notificationCurrentText);
			} else {
				if (closeBar) {
					notificationBarClose.hide();
					notificationBarText.update('');
					notificationBarExplanation.hide();
				}
			}

			[notificationBar, questionArea].each(function(area){
				if (!(closeBar == 0 && area == notificationBar)) {
					["danger", "success", "warning"].each(function(name) {
						if (area.hasClassName(name)) {
							area.removeClassName(name);
						}
					});
				}
				if (
					typeof show != 'undefined' && !(ref.showExplanation == 0 && area == questionArea) &&
					!area.hasClassName(ref.notificationCurrentClassName)
				) {
					area.addClassName(ref.notificationCurrentClassName);
					//notificationBar.addClassName("move-up");
					// @toDO remove all "move-up" in code
				}
			});
		}

	},
	listenExplanationButton:function() {
		var ref = this;
		var explanationButton = $(this.explanationButtonId);
		var explanationToggleArray = [explanationButton,$(this.notificationBarId)];
		explanationToggleArray.each(function(button){
			if (button) {
				button.observe('click', function(e) {
					e.stop();
					if (ref.notificationResultCode != -1) {
						if (explanationButton.hasClassName('gumby-icon-doc-text')) {
							ref.showExplanation = 0;
							ref.toggleNotificationBarExplanation(0);
							ref.toggleNotifications(undefined,0);
							if ($(ref.questionSectionId)) $(ref.questionSectionId).show();
							if ($(ref.questionExplanationId)) $(ref.questionExplanationId).hide();
						} else {
							if (ref.explanationData == null) {
								if (ref.getExplanationURL) {
									new Ajax.Request(ref.getExplanationURL,{
										method:'GET',
										parameters:{},
										contentType: "application/json",
										requestHeaders: {Accept: 'application/json'},
										onSuccess:function(transport){
											  try{
												  ref.explanationData = transport.responseJSON;
												  ref.showExplanationArea();
												  ref._showExplanationArea();
											  } catch(exc) {
												  console.log(transport.responseText);
												  alert('Something went wrong...');
											  }
										}
									});
								}
							} else {
								ref._showExplanationArea();
							}
						}
					}
				});
			}
		});
	},
	_showExplanationArea:function() {
		this.showExplanation = 1;
		this.toggleNotificationBarExplanation(1);
		this.toggleNotifications(this.notificationResultCode,0);
		if ($(this.questionSectionId)) $(this.questionSectionId).hide();
		if ($(this.questionExplanationId)) $(this.questionExplanationId).show();
	},
	listenNotificationCloseButton:function() {
		var ref = this;
		var notificationCloseButtonArray = [$(this.notificationCloseButtonId), $(this.nextQuestionId)];
		notificationCloseButtonArray.each(function(button){
			if (button) {
				button.observe('click',function(e){
					e.stop();
					ref.hideNotificationBar();
					if ($(ref.questionExplanationId)) $(ref.questionExplanationId).hide();
					if ($(ref.questionSectionId)) $(ref.questionSectionId).show();
				});
			}
		});
	}
//	listenSubmitButtonWithForm: function( submitButtonId, submitFormId) {
//	  if ($(submitButtonId)) {
//		  $(submitButtonId).observe('click', function(e) {
//			  e.stop();
//			  if ($(submitFormId)) {
//				  $(submitFormId).submit();
//			  }
//		  });
//	  }
//	}
});