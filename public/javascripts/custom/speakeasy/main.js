/*global define*/

'use strict';

// Declare here that jquery is the jquery.min version - other locales can be easily substituted.

// define('jquery', ['jquery.min.js'], function() {
//     // console.log("jquery loaded");
//     return $;
// });

// define('underscorejs',['underscore-min.js'],function() {
//     // console.log("underscorejs loaded");
//     return _;
// });

define("jsroutes", ["/custom/speakeasy/jsroutes"], function() {
    return jsRoutes;
});

require(['jquery', 'underscorejs', 'jsroutes'], function($, _) {
    var customJsRoutes = jsRoutes.controllers.custom.Speakeasy;
    var questions = [];
    var questionsOnPage = 2;
    var totalQuestions = 0;
    var nextButton = $('#btn-post-answer');
    var testResult = $('#test-result');
    var spinner = $('#question-spinner');
    var failLogo = $('#fail-result');
    var qlist = $('#question-list');
    var qform = $('#question-form');
    var logo = $('#logo');
    var progressBar = $('#test-progress-bar');
    var testAlert = $('#test-alert');
    var isInIFrame = (window.location != window.parent.location) ? true : false;
    if (!isInIFrame) {
        $("#iframecss").remove();
    }
    customJsRoutes.items().ajax({
        type: "GET",
        dataType: "json",
        success: function (data, textStatus) {
            questions = data.questions;
            if (typeof questions != 'undefined' && questions.length != 0) {
                totalQuestions = questions.length;
                startNewPage(questions);
            } else {
                showResult("",false);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            // console.log(xhr.status);
            // console.log(thrownError);
            showResult("",false);
        }
    });
    function startNewPage(rest) {
        process(_.first(rest, questionsOnPage), _.rest(rest, questionsOnPage));
    }
    function updateProgressBar(restlength) {
        var currentProgress = (totalQuestions - restlength)/totalQuestions * 100;
        progressBar.attr('aria-valuenow', currentProgress);
        progressBar.css('width', currentProgress + "%");
    }
    function showResult(data, success) {
        var result = '';
        spinner.hide();
        if (nextButton) nextButton.hide(); 
        if (success && typeof data.result != 'undefined') {
            if (logo) logo.show();
            testResult.find('p').show();
            result = data.result;
        } else {
            failLogo.show();
            result = "Sorry! Something went wrong. Please, try again <a href='/custom/speakeasy'>later</a>.";
        }
        if (testResult) {
            testResult.show();
            testResult.find('h2').html(result);
        }
    }
    function getResults(answers) {
        customJsRoutes.result().ajax({
            type:"POST",
            dataType:"json",
            contentType:"application/json",
            data:JSON.stringify(answers),
            success: function(data, textStatus) {
                showResult(data, typeof data.error != 'undefined' && data.error == 0);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // console.log(xhr.status);
                // console.log(thrownError);
                showResult("",false);
            }
        });
        customJsRoutes.report().ajax({
            type:"POST",
            dataType:"json",
            contentType:"application/json",
            data:JSON.stringify(answers)
        });
    }
    function process(pair, rest) {
        if (rest.length == 0 && pair.length == 0) {
            getResults(_.map(questions, function(q) {
                    return {
                        id:q.id,
                        value:q.answer
                    };
                }));
        } else {
            // console.log(pair);
            if (qlist && nextButton && qlist) {
                customJsRoutes.next().ajax({
                    type: "POST",
                    dataType: "json",
                    contentType: 'application/json',
                    data: JSON.stringify(pair.map(function(q) { return q.id;})),
                    success: function(data, textStatus) {
                        if (typeof  data.html != 'undefined') {
                            spinner.hide();
                            qlist.html(data.html);
                            listenChoices();
                        }
                        nextButton.unbind( "click" );
                        nextButton.click(function() {
                            var answers = qform.serializeArray();
                            // console.log(answers);
                            if (answers.length == qlist.find('li').length) {
                                questions = _.map(questions, function(q){
                                    var f = _.find(answers, function(a) { return a.name == "optionsRadios-question-" + q.order;});
                                    if (f) {
                                        q.answer = f.value;
                                    }
                                    return q;
                                });                             
                                testAlert.hide();
                                updateProgressBar(rest.length);
                                qlist.html("");
                                spinner.show();
                                startNewPage(rest);
                            } else {
                                testAlert.show();
                            }
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        // console.log(xhr.status);
                        // console.log(thrownError);
                        showResult("",false);
                    }
                });
            }
        }
    }
    function listenChoices() {
        qlist.find('li').each(function(index, lii) {
            var selector = "#"+lii.id+" input[name='optionsRadios-"+lii.id+"']";            
            $(selector).change(function () {
                if ($(this).is(":checked")) {
                    _.each(_.zip( $('#q-'+lii.id+' strong'), this.value.split(',')), function(oo){
                        $(oo[0]).removeClass('nice-gap');
                        $(oo[0]).html(oo[1]);
                    });
                }
            });
        });
    }
});