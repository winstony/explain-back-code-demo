package integrational;

import markers.IntegrationalTest;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import client.TestClient;
import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import searchto.SearchTOAssemblerIT;
import to.api.Comment;
import to.api.HistoryElement;
import to.db.notification.CommentNotification;
import to.db.notification.Notification;
import util.CleanSuiteRule;
import util.TestApiModels;
import util.WithApplicationRule;
import config.ConfigIT;
import controllers.api.ChapterFreeCompleteAchievementIT;
import controllers.api.CommentNotificationIT;
import controllers.api.CommentsIT;
import controllers.api.CoursesIT;
import controllers.api.HistoryElementIT;
import controllers.api.PingIT;
import controllers.api.QuestionsIT;
import dao.couchbase.CouchbaseHistoryElementDao;

@RunWith(Categories.class)
@IncludeCategory(IntegrationalTest.class)
@SuiteClasses({
    PingIT.class,
    CoursesIT.class,
    QuestionsIT.class,
    CommentsIT.class,
    SearchTOAssemblerIT.class,
    HistoryElementIT.class,
    CommentNotificationIT.class,
    ConfigIT.class,
    ChapterFreeCompleteAchievementIT.class
})
public class IntegrationalSuite extends WithApplicationRule {

    // This class runs all test cases marked as IntegrationalTest from specified suite classes.
    // You may run this category tests from console
    // activator 'test-only integrational.*'

    @ClassRule
    public static TestRule chain = RuleChain.outerRule(new WithApplicationRule())
                                            .around(new CleanSuiteRule());

}

