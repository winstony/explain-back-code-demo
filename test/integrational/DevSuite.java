package integrational;

import markers.TestInDevelopment;

import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import util.CleanSuiteRule;
import util.WithApplicationRule;
import controllers.api.ChapterFreeCompleteAchievementIT;

@RunWith(Categories.class)
@IncludeCategory(TestInDevelopment.class)
@SuiteClasses({
    ChapterFreeCompleteAchievementIT.class
})
// remove ignore to use this suite
@Ignore
public class DevSuite {

    @ClassRule
    public static TestRule chain = RuleChain.outerRule(new WithApplicationRule())
                                            .around(new CleanSuiteRule());
}

