package client;

import static client.TestClient.POST;
import static client.TestClient.fromJsonContent;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Assert;

import play.libs.Json;
import play.mvc.Result;
import play.test.Helpers;
import to.api.Comment;

public class QuestionCommentsClient {

    private final TestClient client;
    private final String qUuid;

    QuestionCommentsClient(TestClient client, String qUuid) {
        this.client = client;
        this.qUuid = qUuid;
    }

    public Comment post(Comment comment) {
        Result result = postCommentResult(comment);
        Assert.assertThat(
                "Could not create comment " + Helpers.contentAsString(result),
                Helpers.status(result),
                is(200));
        Comment saved = fromJsonContent(result, Comment.class);
        Assert.assertThat("comment id is null", saved.id, not(is(nullValue())));
        Assert.assertThat("isPositive changed in comment", saved.isPositive, is(comment.isPositive));
        Assert.assertThat("commentType changed in comment", saved.commentType, is(comment.commentType));
        Assert.assertThat("parentId changed in comment", saved.parentId, is(comment.parentId));
        Assert.assertThat("text changed in comment", saved.text, is(comment.text));
        Assert.assertThat("comment timestamp is null", saved.timestamp, not(is(nullValue())));
        Assert.assertThat("comment user is null", saved.user, not(is(nullValue())));
        Assert.assertThat("comment user.fullname is null", saved.user.fullName, not(is(nullValue())));
        Assert.assertThat("comment user.id is null", saved.user.id, not(is(nullValue())));
        return saved;
    }

    public Result postCommentResult(Comment comment) {
        return client.apiCall(Helpers.fakeRequest(POST, "/v2/questions/" + qUuid + "/comments")
            .withJsonBody(Json.toJson(comment)));
    }
}
