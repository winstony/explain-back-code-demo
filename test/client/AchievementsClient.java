package client;

import static org.hamcrest.CoreMatchers.is;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.junit.Assert;

import play.mvc.Result;
import play.test.Helpers;
import to.api.achieve.Achievement;

import com.google.common.collect.Lists;

public class AchievementsClient {

    private TestClient testClient;

    public AchievementsClient(TestClient testClient) {
        this.testClient = testClient;
    }

    public List<Achievement> getList() {
        Result result = getListResult();
        Assert.assertThat(Helpers.status(result), is(200));
        return Lists.newArrayList(TestClient.fromJsonContent(result, Achievement[].class));
    }

    private Result getListResult() {
        return testClient.apiCall(Helpers.fakeRequest(TestClient.GET, "/v1/achievements"));
    }

    public Achievement get(String id) {
        Result result = getResult(id);
        Assert.assertThat(Helpers.status(result), is(200));
        return TestClient.fromJsonContent(result, Achievement.class);
    }

    private Result getResult(String id) {
        return testClient.apiCall(Helpers.fakeRequest(TestClient.GET, "/v1/achievements/" + id));
    }

    public void addImageUrl(String id, String url) {
        Result result = addImageUrlResult(id, url);
        Assert.assertThat(Helpers.status(result), is(200));
    }

    private Result addImageUrlResult(String id, String url) {
        try {
            return testClient.apiCall(Helpers.fakeRequest(
                    TestClient.POST,
                    "/v1/achievements/" + id + "/imageUrl?url=" + URLEncoder.encode(url, "UTF-8")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
