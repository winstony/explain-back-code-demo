package client;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;

import com.google.common.collect.Lists;

import play.libs.Json;
import play.mvc.Result;
import play.test.Helpers;
import to.api.HistoryElement;
import to.api.Question;

public class HistoryElementsClient {

    private TestClient testClient;

    public HistoryElementsClient(TestClient testClient) {
        this.testClient = testClient;
    }

    public HistoryElement postCorrectAnswer(Question q) {
        HistoryElement record = new HistoryElement();
        record.question = q;
        record.chosenChoiceIds = q.choices.stream().filter(c -> c.correct).map(c -> c.id).collect(Collectors.toSet());
        return post(record);
    }

    public HistoryElement post(HistoryElement record) {
        Result result = postResult(record);
        Assert.assertThat("Could not save HistoryElement", Helpers.status(result), is(200));
        HistoryElement saved = TestClient.fromJsonContent(result, HistoryElement.class);
        Assert.assertThat("Record id is null", saved.couchbaseId, not(is(nullValue())));
        // TODO more assertions on saved record
        return saved;
    }

    public Result postResult(HistoryElement record) {
        return testClient.apiCall(Helpers.fakeRequest(TestClient.POST, "/v1/historyElements")
                .withJsonBody(Json.toJson(record)));
    }

    public List<HistoryElement> getForChapter(Long courseId, Long chapterId) {
        Result result = getForChapterResult(courseId, chapterId);
        Assert.assertThat("Could not get history elements for course", Helpers.status(result), is(200));
        return Lists.newArrayList(TestClient.fromJsonContent(result, HistoryElement[].class));
    }

    public Result getForChapterResult(Long courseId, Long chapterId) {
        return testClient.apiCall(Helpers.fakeRequest(
                TestClient.GET,
                "/v1/historyElements?courseId=" + courseId + "&chapterId=" + chapterId));
    }

    public List<HistoryElement> getForCourse(Long courseId) {
        Result result = getForCourseResult(courseId);
        Assert.assertThat("Could not get history elements for course", Helpers.status(result), is(200));
        return Lists.newArrayList(TestClient.fromJsonContent(result, HistoryElement[].class));
    }

    public Result getForCourseResult(Long courseId) {
        return testClient.apiCall(Helpers.fakeRequest(TestClient.GET, "/v1/historyElements?courseId=" + courseId));
    }


}
