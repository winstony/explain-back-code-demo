package client;

import static org.hamcrest.CoreMatchers.is;

import org.junit.Assert;

import play.mvc.Result;
import play.test.FakeRequest;
import play.test.Helpers;
import to.api.Notifications;

public class NotificationsClient {
    private final TestClient testClient;

    public NotificationsClient(TestClient testClient) {
        this.testClient = testClient;
    }

    public Notifications comment(Long before) {
        Result result = commentResult(before);
        Assert.assertThat("Could not get CommentNotifications", Helpers.status(result), is(200));
        Notifications notifications = TestClient.fromJsonContent(result, Notifications.class);
        return notifications;
    }

    public Notifications comment() {
        return comment(null);
    }

    public Notifications timeline() {
        return timeline(null);
    }

    public Notifications timeline(Long before) {
        Result result = timelineResult(before);
        Assert.assertThat("Could not get TimelineNotifications", Helpers.status(result), is(200));
        Notifications notifications = TestClient.fromJsonContent(result, Notifications.class);
        return notifications;
    }

    public Result commentResult(Long before) {
        String params = "";
        if (before != null) {
            params = "?before=" + before;
        }
        return testClient.apiCall(new FakeRequest(TestClient.GET, "/v1/notifications/comment" + params));
    }

    private Result timelineResult(Long before) {
        String params = "";
        if (before != null) {
            params = "?before=" + before;
        }
        return testClient.apiCall(new FakeRequest(TestClient.GET, "/v1/notifications/timeline" + params));
    }

}
