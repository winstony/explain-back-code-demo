package client;

import static org.hamcrest.CoreMatchers.is;

import java.util.List;

import org.junit.Assert;

import play.mvc.Result;
import play.test.Helpers;
import to.api.Chapter;

import com.google.common.collect.Lists;

public class ChaptersClient {

    private TestClient testClient;

    String course;

    public ChaptersClient(TestClient testClient, String course) {
        this.testClient = testClient;
        this.course = course;
    }

    public List<Chapter> get() {
        Result result = getResult();
        Assert.assertThat("Get chapters failed for " + course, Helpers.status(result), is(200));
        return Lists.newArrayList(TestClient.fromJsonContent(result, Chapter[].class));
    }

    private Result getResult() {
        return testClient.apiCall(Helpers.fakeRequest(TestClient.GET, "/v1/courses/" + course + "/chapters"));
    }

    public QuestionsQueueClient questions(String chapterAlias) {
        return new QuestionsQueueClient(this, chapterAlias);
    }

    TestClient client() {
        return testClient;
    }

}