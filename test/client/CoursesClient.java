package client;

import static org.hamcrest.CoreMatchers.is;

import java.util.List;

import org.fest.util.Collections;
import org.junit.Assert;

import play.mvc.Result;
import play.test.Helpers;
import to.api.Course;

public class CoursesClient {
    private final TestClient testClient;

    public CoursesClient(TestClient testClient) {
        this.testClient = testClient;
    }

    public List<Course> get() {
        Result result = getResult();
        Assert.assertThat("Get courses failed", Helpers.status(result), is(200));
        return Collections.list(TestClient.fromJsonContent(result, Course[].class));
    }

    public Result getResult() {
        return testClient.apiCall(Helpers.fakeRequest(TestClient.GET, "/v1/courses"));
    }
}