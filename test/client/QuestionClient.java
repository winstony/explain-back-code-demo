package client;

public class QuestionClient {

    private final TestClient client;
    private final String qUuid;

    QuestionClient(TestClient client, String qUuid) {
        this.client = client;
        this.qUuid = qUuid;
    }

    public QuestionCommentsClient comments() {
        return new QuestionCommentsClient(client, qUuid);
    }

}