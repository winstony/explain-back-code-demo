package client;

import static org.hamcrest.CoreMatchers.is;

import java.util.List;

import org.fest.util.Collections;
import org.junit.Assert;

import play.mvc.Result;
import play.test.Helpers;
import to.api.Question;

public class QuestionsQueueClient {
    private final ChaptersClient chaptersClient;

    private String chapter;

    public QuestionsQueueClient(ChaptersClient chaptersClient, String chapter) {
        this.chaptersClient = chaptersClient;
        this.chapter = chapter;
    }

    public List<Question> get(Integer size) {
        Result result = getResult(size);
        Assert.assertThat("Get question failed " + this.chaptersClient.course + "/" + chapter, Helpers.status(result), is(200));
        return Collections.list(TestClient.fromJsonContent(result, Question[].class));
    }

    public Result getResult(Integer size) {
        String route = "/v1/courses/" + this.chaptersClient.course + "/chapters/" + chapter + "/questions";
        if (size != null) {
            route = route + "?size=" + size;
        }
        return client().apiCall(Helpers.fakeRequest(TestClient.GET, route));
    }

    TestClient client() {
        return chaptersClient.client();
    }
}