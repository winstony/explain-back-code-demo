package client;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Assert;

import markers.IntegrationalTest;
import models.core.User;
import models.secure.SchoolUserTo;
import play.libs.Json;
import play.libs.F.Promise;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Http.Cookie;
import play.test.FakeRequest;
import play.test.Helpers;
import scala.concurrent.ExecutionContext;
import securesocial.core.authenticator.AuthenticatorStore;
import securesocial.core.authenticator.CookieAuthenticator;
import securesocial.core.authenticator.CookieAuthenticatorBuilder;
import securesocial.core.authenticator.IdGenerator;
import securesocial.core.services.CacheService;
import security.securesocial.service.MyEnvironment;
import to.api.Comment;
import to.api.HistoryElement;
import to.api.Notifications;

public class TestClient {
    // SecureSocial environment
    public static MyEnvironment env = new MyEnvironment();

    public static final int TIMEOUT = 10000;

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";

    private Cookie cookie;

    // Create client for user
    public TestClient(SchoolUserTo user) {
        if (user != null) {
            this.cookie = fakeCookie(user);
        }
    }

    private static Cookie fakeCookie(SchoolUserTo user) {
        CacheService cacheService = env.cacheService();
        ExecutionContext executionContext = env.executionContext();
        IdGenerator idGenerator = env.idGenerator();
        AuthenticatorStore<CookieAuthenticator<SchoolUserTo>> authenticatorStore =
                new AuthenticatorStore.Default<CookieAuthenticator<SchoolUserTo>>(cacheService, executionContext);
        CookieAuthenticatorBuilder<SchoolUserTo> builder = new CookieAuthenticatorBuilder<SchoolUserTo>(
                authenticatorStore,
                idGenerator);
        CookieAuthenticator<SchoolUserTo> auth = Promise.wrap(builder.fromUser(user)).get(TIMEOUT);

        Cookie cookie = new Cookie(
                CookieAuthenticator.cookieName(),
                auth.id(),
                120,
                CookieAuthenticator.cookiePath(),
                "/", // CookieAuthenticator.cookieDomain(),
                CookieAuthenticator.cookieSecure(),
                CookieAuthenticator.cookieHttpOnly());
        return cookie;
    }

    // ----------------

    @Deprecated
    public Result postCommentResultV1(Comment comment) {
        return apiCall(Helpers.fakeRequest(POST, "/v1/comments").withJsonBody(Json.toJson(comment)));
    }

    public Comment[] getUserComments() {
        Result result = getUserCommentsResult();
        Assert.assertThat("Get user comments failed", Helpers.status(result), is(200));
        Comment[] comments = fromJsonContent(result, Comment[].class);
        return comments;
    }

    public Result getUserCommentsResult() {
        return apiCall(Helpers.fakeRequest(GET, "/v1/userComments"));
    }

    public Comment[] getQuestionComments(Long questionId) {
        Result result = getQuestionCommentsResult(questionId);
        Assert.assertThat("Get question comments failed", Helpers.status(result), is(200));
        Comment[] comments = fromJsonContent(result, Comment[].class);
        return comments;
    }

    public Result getQuestionCommentsResult(Long questionId) {
        return apiCall(Helpers.fakeRequest(GET, "/v1/comments?questionId=" + questionId));
    }

    // ------

    public Result call(FakeRequest request) {
        if (cookie != null) {
            request = request.withCookies(cookie);
        }
        return Helpers.routeAndCall(request, TIMEOUT);
    }

    public Result apiCall(FakeRequest request) {
        if (cookie != null) {
            request = request.withCookies(cookie);
        }
        return Helpers.routeAndCall(withDefaultApiHeaders(request), TIMEOUT);
    }

    private static FakeRequest withDefaultApiHeaders(FakeRequest request) {
        return request.withHeader(Http.HeaderNames.ACCEPT, Http.MimeTypes.JSON)
                .withHeader(Http.HeaderNames.CONTENT_TYPE, Http.MimeTypes.JSON);
    }

    public static <A> A fromJsonContent(Result result, Class<A> clazz) {
        return Json.fromJson(Json.parse(Helpers.contentAsString(result)), clazz);
    }

    public CoursesClient courses() {
        return new CoursesClient(this);
    }

    public ChaptersClient course(String courseAlias) {
        return new ChaptersClient(this, courseAlias);
    }

    public QuestionClient question(String qUuid) {
        return new QuestionClient(this, qUuid);
    }

    public NotificationsClient notifications() {
        return new NotificationsClient(this);
    }

    public AchievementsClient achievements() {
        return new AchievementsClient(this);
    }

    public HistoryElementsClient historyElements() {
        return new HistoryElementsClient(this);
    }

}
