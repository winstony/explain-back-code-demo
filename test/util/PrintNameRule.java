package util;


import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class PrintNameRule extends TestWatcher {
    private long timeStartMs;
    private long timeEndMs;
    private String name;

    public PrintNameRule(String name) {
        this.name = name;
    }

    @Override
    protected void starting(Description description) {
        timeStartMs = System.nanoTime() / 1000 / 1000;
    }

    @Override
    protected void succeeded(Description description) {
        timeEndMs = System.nanoTime() / 1000 / 1000;
        double seconds = (timeEndMs - timeStartMs) / 1000.0;
        System.out.println("    " + name + " - " + description.getClassName() + "." + description.getMethodName() +
                " - " + seconds);
    };
}
