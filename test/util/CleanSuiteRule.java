package util;

import org.junit.rules.ExternalResource;

import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import to.api.Comment;
import to.api.HistoryElement;
import to.api.achieve.Achievement;
import to.db.notification.Notification;
import client.TestClient;
import controllers.api.ChapterFreeCompleteAchievementIT;
import controllers.api.CommentNotificationIT;
import controllers.api.CommentsIT;
import dao.couchbase.CouchbaseHistoryElementDao;

public class CleanSuiteRule extends ExternalResource {

    private static final ALogger log = Logger.of(CleanSuiteRule.class);

    @Override
    protected void before() throws Throwable {
        log.info("Cleaning before test suite started");
        TestClient firstClient = new TestClient(TestApiModels.antonInQuizful());
        TestClient secondClient = new TestClient(TestApiModels.antonInQuizfulSecond());
        clean(firstClient);
        clean(secondClient);
    }


    private static void clean(TestClient client) {
        log.info("Clean comments");
        for (Comment comment : client.getUserComments()) {
            log.warn("Deleting: " + Json.toJson(comment));
            CommentsIT.deleteComment(comment.id);
        }
        log.info("Clean comment notifications");
        while (!client.notifications().comment(null).list.isEmpty()) {
            for (Notification notification : client.notifications().comment(null).list) {
                log.warn("Deleting: " + Json.toJson(notification));
                CommentNotificationIT.deleteNotification(notification.id);
            }
        }
        log.info("Clean timeline notifications");
        while (!client.notifications().timeline(null).list.isEmpty()) {
            for (Notification notification : client.notifications().timeline(null).list) {
                log.warn("Deleting: " + Json.toJson(notification));
                CommentNotificationIT.deleteNotification(notification.id);
            }
        }
        log.info("Clean achievements");
        while (!client.achievements().getList().isEmpty()) {
            for (Achievement a : client.achievements().getList()) {
                log.warn("Deleting: " + Json.toJson(a));
                ChapterFreeCompleteAchievementIT.deleteAchievement(a.id);
            }
        }
        if (false) { // if need to clean history elements
            log.info("Clean history elements in oridnary courses");
            for (Long course : new long[] {0L, -1L, 9L, 10L}) {
                for (HistoryElement record : client.historyElements().getForCourse(course)) {
                    log.warn("Deleting: " + Json.toJson(record));
                    new CouchbaseHistoryElementDao().delete(record.couchbaseId);
                }
            }
        }
        log.info("Clean history elements in one question course");
        for (HistoryElement record : client.historyElements().getForCourse(17L)) {
            log.warn("Deleting: " + Json.toJson(record));
            new CouchbaseHistoryElementDao().delete(record.couchbaseId);
        }
    }

}
