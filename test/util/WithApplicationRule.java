package util;

import org.junit.rules.ExternalResource;

import play.Logger;
import play.Logger.ALogger;
import play.test.FakeApplication;
import play.test.Helpers;

public class WithApplicationRule extends ExternalResource {

    private static final ALogger log = Logger.of(WithApplicationRule.class);

    protected static FakeApplication app;

    private static FakeApplication provideFakeApplication() {
        return Helpers.fakeApplication();
    }

    @Override
    protected void before() {
        app = provideFakeApplication();
        Helpers.start(app);
        log.info("Fake application started");
    }

    @Override
    protected void after() {
        if (app != null) {
            Helpers.stop(app);
            app = null;
            log.info("Fake application stopped");
        }
    }
}
