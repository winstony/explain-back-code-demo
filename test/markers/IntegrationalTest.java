package markers;

import static org.hamcrest.CoreMatchers.is;
import models.core.User;
import models.secure.SocialUserAdapter;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;

import play.Mode;
import play.libs.Json;
import play.libs.F.Promise;
import play.mvc.Http;
import play.mvc.Http.Cookie;
import play.mvc.Result;
import play.test.FakeRequest;
import play.test.Helpers;
import scala.concurrent.ExecutionContext;
import securesocial.core.BasicProfile;
import securesocial.core.authenticator.AuthenticatorStore;
import securesocial.core.authenticator.CookieAuthenticator;
import securesocial.core.authenticator.CookieAuthenticatorBuilder;
import securesocial.core.authenticator.IdGenerator;
import securesocial.core.services.CacheService;
import security.securesocial.service.MyEnvironment;
import util.PrintNameRule;

public class IntegrationalTest {

    @Rule
    public PrintNameRule printNameRule = new PrintNameRule("TEST");

    @BeforeClass
    public static void onlyIfAppExists() {
        try {
            play.Play.application();
            Assume.assumeThat(play.Play.mode(), is(Mode.TEST));
        } catch (Exception e) {
            Assume.assumeNoException(e.getMessage(), e);
        }
    }

}
