package controllers.api;

import static org.hamcrest.CoreMatchers.*;

import java.util.List;

import junitparams.JUnitParamsRunner;
import markers.IntegrationalTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import client.TestClient;
import play.libs.F.Promise;
import play.mvc.Result;
import play.test.Helpers;
import to.api.HistoryElement;
import util.TestApiModels;
import dao.couchbase.CouchbaseHistoryElementDao;
import dao.couchbase.JavaOpResult;
import dao.couchbase.DaoUtils;

@Category(IntegrationalTest.class)
public class HistoryElementIT extends IntegrationalTest {

    @Test
    public void testHistoryElementUnauthenticated() {
        HistoryElement record = TestApiModels.historyElement();
        Result result = new TestClient(null).historyElements().getForCourseResult(record.question.courseId);
        Assert.assertThat(Helpers.status(result), is(401));
    }

    @Test
    public void testHistoryElementsForCourseUnauthenticated() {
        HistoryElement record = TestApiModels.historyElement();
        Result result = new TestClient(null).historyElements().getForChapterResult(
                record.question.courseId,
                record.question.chapterId);
        Assert.assertThat(Helpers.status(result), is(401));
    }

    @Test
    public void testPostHistoryElementUnauthenticated() {
        HistoryElement record = TestApiModels.historyElement();
        Result result = new TestClient(null).historyElements().postResult(record);
        Assert.assertThat(Helpers.status(result), is(401));
    }

    @Test
    public void scenarioAnswerTwoQuestions() {
        TestClient client = new TestClient(TestApiModels.antonInQuizful());

        HistoryElement recordOne = TestApiModels.historyElement();

        HistoryElement recordTwo = TestApiModels.historyElement();
        recordTwo.question.chapterId = recordTwo.question.chapterId - 1;

        testHistoryElementsCountForCourse(client, recordOne.question.courseId, 0);
        testHistoryElementsCountForChapter(client, recordOne.question.courseId, recordOne.question.chapterId, 0);
        testHistoryElementsCountForChapter(client, recordTwo.question.courseId, recordTwo.question.chapterId, 0);

        HistoryElement saved = client.historyElements().post(recordOne);

        testHistoryElementsCountForCourse(client, recordOne.question.courseId, 1);
        testHistoryElementsCountForChapter(client, recordOne.question.courseId, recordOne.question.chapterId, 1);
        testHistoryElementsCountForChapter(client, recordTwo.question.courseId, recordTwo.question.chapterId, 0);

        HistoryElement savedTwo = client.historyElements().post(recordTwo);

        testHistoryElementsCountForCourse(client, recordOne.question.courseId, 2);
        testHistoryElementsCountForChapter(client, recordOne.question.courseId, recordOne.question.chapterId, 1);
        testHistoryElementsCountForChapter(client, recordTwo.question.courseId, recordTwo.question.chapterId, 1);

        // TODO

        // clean
        deleteHistoryElement(saved.couchbaseId);
        deleteHistoryElement(savedTwo.couchbaseId);

        testHistoryElementsCountForCourse(client, recordOne.question.courseId, 0);
        testHistoryElementsCountForChapter(client, recordOne.question.courseId, recordOne.question.chapterId, 0);
        testHistoryElementsCountForChapter(client, recordTwo.question.courseId, recordTwo.question.chapterId, 0);
    }

    private void testHistoryElementsCountForChapter(TestClient client, Long courseId, Long chapterId, int count) {
        List<HistoryElement> records = client.historyElements().getForChapter(courseId, chapterId);
        Assert.assertThat("Count of History elements for chapter failed ", records.size(), is(count));
    }

    private void testHistoryElementsCountForCourse(TestClient client, Long courseId, int count) {
        List<HistoryElement> records = client.historyElements().getForCourse(courseId);
        Assert.assertThat("Count of History elements for course failed ", records.size(), is(count));
    }

    // ------

    public static void deleteHistoryElement(String id) {
        String bucketAlias = CouchbaseHistoryElementDao.HISTORY_ELEMENT_BUCKET_ALIAS();
        JavaOpResult opResult = Promise.wrap(DaoUtils.deleteDocumentQuietly(bucketAlias, id)).get(TestClient.TIMEOUT);
        Assert.assertThat(opResult.success(), is(true));
    }
}
