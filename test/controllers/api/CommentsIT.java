package controllers.api;

import static org.hamcrest.CoreMatchers.*;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import markers.IntegrationalTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import client.TestClient;
import dao.couchbase.CouchbaseCommentDao;
import dao.couchbase.DaoUtils;
import dao.couchbase.JavaOpResult;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Result;
import play.test.FakeRequest;
import play.test.Helpers;
import to.api.Comment;
import to.api.CommentType;
import util.TestApiModels;

@Category(IntegrationalTest.class)
public class CommentsIT extends IntegrationalTest {

    @Test
    public void testQuestionCommentsUnauthenticated() {
        Comment comment = TestApiModels.textComment();
        Result result = new TestClient(null).getQuestionCommentsResult(TestApiModels.QUESTION_ID);
        Assert.assertThat(Helpers.status(result), is(401));
    }

    @Test
    public void testUserCommentsUnauthenticated() {
        Result result = new TestClient(null).getUserCommentsResult();
        Assert.assertThat(Helpers.status(result), is(401));
    }

    @Test
    public void testPostCommentUnauthenticated() {
        Comment comment = TestApiModels.textComment();
        Result result = new TestClient(null).postCommentResultV1(comment);
        Assert.assertThat(Helpers.status(result), is(401));
    }

    @Test
    public void scenarioCreateOneComment() {
        TestClient client = new TestClient(TestApiModels.antonInQuizful());

        Comment newComment = TestApiModels.textComment();
        testQuestionCommentsNumber(client, TestApiModels.QUESTION_ID, 0);
        testUserCommentsNumber(client, 0);
        Comment comment = client.question(newComment.question.uuid).comments().post(newComment);
        testQuestionCommentsNumber(client, TestApiModels.QUESTION_ID, 1);
        testUserCommentsNumber(client, 1);

        // clean
        deleteComment(comment.id);
        testQuestionCommentsNumber(client, TestApiModels.QUESTION_ID, 0);
        testUserCommentsNumber(client, 0);
    };

    // TODO tests for invalid comments do not save

    // TODO tests for comment notifications

    // TODO user posts two comments order is correct, number is correct
    // TODO user posts comment then other user comments on this comment
    // TODO +1 other comment
    // TODO +1 own comment
    // TODO -1 own comment
    // TODO -1 other comment

    private static void testQuestionCommentsNumber(TestClient client, Long questionId, int number) {
        Comment[] comments = client.getQuestionComments(questionId);
        Assert.assertThat("Incorrect count of comments", comments.length, is(number));
    }

    private static void testUserCommentsNumber(TestClient client, int number) {
        Comment[] comments = client.getUserComments();
        Assert.assertThat("Incorrect count of comments", comments.length, is(number));
    }

    public static void deleteComment(String id) {
        String bucketAlias = CouchbaseCommentDao.COMMENTS_BUCKET_ALIAS();
        JavaOpResult opResult = Promise.wrap(DaoUtils.deleteDocumentQuietly(bucketAlias, id)).get(TestClient.TIMEOUT);
        Assert.assertThat(opResult.success(), is(true));
    }

}
