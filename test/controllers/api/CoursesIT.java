package controllers.api;

import static org.hamcrest.CoreMatchers.*;
import static util.TestApiModels.antonInQuizful;

import java.util.List;

import markers.IntegrationalTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import client.TestClient;
import to.api.Chapter;
import to.api.Course;
import to.api.Question;

@Category(IntegrationalTest.class)
public class CoursesIT extends IntegrationalTest {
    @Test
    public void testGetCourses() {
        List<Course> courses = new TestClient(antonInQuizful()).courses().get();
        Assert.assertThat(courses.size(), is(not(0)));
    }

    @Test
    public void testHasJavaCourse() {
        List<Course> courses = new TestClient(antonInQuizful()).courses().get();
        Course java = courses.stream().filter(c -> c.alias.equals("java")).findFirst().orElse(null);
        Assert.assertThat(java, is(not(nullValue())));
    }

    @Test
    public void testGetJavaChapters() {
        List<Chapter> chapters = new TestClient(antonInQuizful()).course("java").get();
        Assert.assertThat(chapters, is(not(nullValue())));
        Assert.assertThat(chapters.size(), is(not(0)));
    }

    @Test
    public void testGetJavaChapterQuestion() {
        TestClient client = new TestClient(antonInQuizful());
        List<Chapter> chapters = client.course("java").get();
        List<Question> questions = client.course("java").questions(chapters.get(0).alias).get(1);
        List<Question> questionsNull = client.course("java").questions(chapters.get(0).alias).get(1);
        List<Question> questionsHundred = client.course("java").questions(chapters.get(0).alias).get(100);
        Assert.assertThat(questions.size(), is(1));
        Assert.assertThat(questionsNull.size(), is(1));
        Assert.assertThat(questionsHundred.size(), is(100));
    }
}
