package controllers.api;

import static org.hamcrest.CoreMatchers.is;
import markers.IntegrationalTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import client.TestClient;
import dao.couchbase.CouchbaseCommentDao;
import dao.couchbase.CouchbaseNotificationDao;
import dao.couchbase.DaoUtils;
import dao.couchbase.JavaOpResult;
import play.libs.F.Promise;
import play.mvc.Result;
import play.test.Helpers;
import to.api.Comment;
import to.api.Notifications;
import util.TestApiModels;

@Category(IntegrationalTest.class)
public class CommentNotificationIT extends IntegrationalTest {

    @Test
    public void testCommentNotificationsUnauthenticated() {
        Result result = new TestClient(null).notifications().commentResult(null);
        Assert.assertThat(Helpers.status(result), is(401));
    }

    @Test
    public void scenarioPostCommentReceiveNotification() throws Exception {

        TestClient firstClient = new TestClient(TestApiModels.antonInQuizful());
        TestClient secondClient = new TestClient(TestApiModels.antonInQuizfulSecond());
        testCommentNotificationsCount(firstClient, 0);
        testCommentNotificationsCount(secondClient, 0);
        Comment c = TestApiModels.textComment();
        Comment firstComment = firstClient.question(c.question.uuid).comments().post(c);
        Thread.sleep(500);
        testCommentNotificationsCount(secondClient, 0);

        Comment secondComment = secondClient.question(c.question.uuid).comments().post(c);
        Thread.sleep(500);

        testCommentNotificationsCount(firstClient, 1);


        Notifications notifications = firstClient.notifications().comment(null);

        deleteNotification(notifications.list.get(0).id);
        CommentsIT.deleteComment(firstComment.id);
        CommentsIT.deleteComment(secondComment.id);
    }

    private void testCommentNotificationsCount(TestClient client, int count) {
        Assert.assertThat("Wrong number of comment notifications",
                client.notifications().comment(null).list.size(),
                is(count));
    }

    public static void deleteNotification(String id) {
        String bucketAlias = CouchbaseNotificationDao.NOTIFICATIONS_BUCKET_ALIAS();
        JavaOpResult opResult = Promise.wrap(DaoUtils.deleteDocumentQuietly(bucketAlias, id)).get(TestClient.TIMEOUT);
        Assert.assertThat(opResult.success(), is(true));
    }
}
