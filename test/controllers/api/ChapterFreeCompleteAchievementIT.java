package controllers.api;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static util.TestApiModels.antonInQuizful;

import java.util.List;

import markers.IntegrationalTest;
import markers.TestInDevelopment;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import play.Logger;
import play.libs.F.Promise;
import to.api.HistoryElement;
import to.api.Notifications;
import to.api.Question;
import to.api.achieve.Achievement;
import to.api.achieve.ChapterFreeCompleteAchievement;
import to.db.notification.AchievementNotification;
import client.TestClient;
import dao.couchbase.CouchbaseCommentDao;
import dao.couchbase.JavaOpResult;
import dao.couchbase.CouchbaseAchievementsDao;
import dao.couchbase.DaoUtils;

@Category({IntegrationalTest.class})
public class ChapterFreeCompleteAchievementIT extends IntegrationalTest {

    private static final Logger.ALogger log = Logger.of(ChapterFreeCompleteAchievementIT.class);

    @Test
    public void scenarioGetChapterFreeCompleteAchievement() throws InterruptedException {
        /*
         * 1. Убедиться что нет нужного ачивмента и нет нужного нотификейшена
         * 2. Ответить на последний вопрос главы,
         * 3. Убедиться что есть нотификейшен и есть ачивмент
         * 4. Удалить их
         */
        TestClient client = new TestClient(antonInQuizful());
        Assert.assertThat("Notification already exists", client.notifications().timeline().list.size(), is(0));
        Assert.assertThat("Achievement already exists", client.achievements().getList().size(), is(0));
        Question q = client.course("oneQuestionCourse").questions("one").get(1).get(0);
        HistoryElement savedRecord = client.historyElements().postCorrectAnswer(q);
        Thread.sleep(1000);

        List<Achievement> achievements = client.achievements().getList();
        Assert.assertThat("Achievement not created", achievements.size(), is(not(0)));
        ChapterFreeCompleteAchievement achievement = achievements.stream()
            .filter(a -> a instanceof ChapterFreeCompleteAchievement)
            .findFirst()
            .map(a -> (ChapterFreeCompleteAchievement) a).get();
        Assert.assertEquals("Wrong chapter", achievement.chapterId, q.chapterId);
        Assert.assertEquals("Wrong course", achievement.courseId, q.courseId);

        Notifications timeline = client.notifications().timeline();
        Assert.assertThat("Notification not created", timeline.list.size(), is(not(0)));
        AchievementNotification notification = timeline.list.stream()
                .filter(n -> n instanceof AchievementNotification)
                .map(n -> ((AchievementNotification) n))
                .filter(n -> n.achievementId.equals(achievement.id))
                .findFirst().get();

        testAddImageUrl(client, achievement);

        // ответим на единственный вопрос правильно еще раз - все должно сработать нормально, не должно быть новых
        // нотификейшена или ачивмента.
        HistoryElement record2 = client.historyElements().postCorrectAnswer(q);
        Thread.sleep(1000);
        ChapterFreeCompleteAchievement achievement2 = client.achievements().getList().stream()
                .filter(a -> a instanceof ChapterFreeCompleteAchievement)
                .findFirst()
                .map(a -> (ChapterFreeCompleteAchievement) a).get();

        Assert.assertEquals(achievement.timestamp, achievement2.timestamp);
        AchievementNotification notification2 = client.notifications().timeline().list.stream()
                .filter(n -> n instanceof AchievementNotification)
                .map(n -> ((AchievementNotification) n))
                .filter(n -> n.achievementId.equals(achievement.id))
                .findFirst().get();
        Assert.assertEquals(notification.updatedTime, notification2.updatedTime);

        deleteAchievement(achievement.id);
        CommentNotificationIT.deleteNotification(notification.id);
        HistoryElementIT.deleteHistoryElement(savedRecord.couchbaseId);
        HistoryElementIT.deleteHistoryElement(record2.couchbaseId);
    }

    private void testAddImageUrl(TestClient client, Achievement achievement) {
        String url = "http://my.url.explaime.co/image.jpg";
        client.achievements().addImageUrl(achievement.id, url);
        Assert.assertEquals(client.achievements().get(achievement.id).imageUrl, url);
    }

    public static void deleteAchievement(String id) {
        String bucketAlias = CouchbaseAchievementsDao.AchievementsBucketAlias();
        JavaOpResult opResult = Promise.wrap(DaoUtils.deleteDocumentQuietly(bucketAlias, id)).get(TestClient.TIMEOUT);
        Assert.assertThat(opResult.success(), is(true));
    }

}
