package controllers.api;

import static org.hamcrest.CoreMatchers.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import client.TestClient;
import play.mvc.Result;
import play.test.Helpers;
import util.TestApiModels;
import markers.IntegrationalTest;

@Category(IntegrationalTest.class)
public class QuestionsIT extends IntegrationalTest {

    @Test
    public void testNegativeSize() {
        TestClient client = new TestClient(TestApiModels.antonInQuizful());
        Result result = client.course("java").questions("one").getResult(-1);
        Assert.assertThat(Helpers.status(result), is(400));
    }

    @Test
    public void testZeroSize() {
        TestClient client = new TestClient(TestApiModels.antonInQuizful());
        Result result = client.course("java").questions("one").getResult(0);
        Assert.assertThat(Helpers.status(result), is(400));
    }

    @Test
    public void testThousandSize() {
        TestClient client = new TestClient(TestApiModels.antonInQuizful());
        Result result = client.course("java").questions("one").getResult(1000);
        Assert.assertThat(Helpers.status(result), is(400));
    }
}
