package controllers.api;

import static org.hamcrest.CoreMatchers.is;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import client.TestClient;
import play.mvc.Result;
import play.test.FakeRequest;
import play.test.Helpers;
import util.TestApiModels;
import markers.IntegrationalTest;

@Category(IntegrationalTest.class)
public class PingIT extends IntegrationalTest {

    @Test
    public void testPing() {
        Result result = new TestClient(TestApiModels.antonInQuizful()).apiCall(new FakeRequest("GET", "/pingsecure"));
        Assert.assertThat(Helpers.status(result), is(200));
    }

    @Test
    public void testPingSecure() {
        // not authenticated
        FakeRequest fakeRequest = new FakeRequest("GET", "/pingsecure").withHeader(
                "Accept",
                "application/json; charset=utf-8 ");
        Result result = Helpers.routeAndCall(fakeRequest, 1000);
        int status = Helpers.status(result);
        Assert.assertThat(status, is(401));
    }
}
