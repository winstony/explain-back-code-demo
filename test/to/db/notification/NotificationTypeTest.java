package to.db.notification;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import markers.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;

import config.ObjectMapperHolder;
import play.libs.Json;

public class NotificationTypeTest extends UnitTest {
    private Map<Class, Notification> classes = new HashMap<>();

    @Before
    public void init() {
        Json.setObjectMapper(ObjectMapperHolder.getInstance());
        classes.put(NewCommentNotification.class, new NewCommentNotification("quuid", "commentId"));
        classes.put(AchievementNotification.class, new AchievementNotification("achievementId"));
    }

    @Test
    public void testNotificationDeserialization() {
        for (NotificationType type : NotificationType.values()) {
            Notification notification = classes.get(type.getNotificationClass());
            Assert.assertEquals(type, notification.getNotificationType());
            JsonNode json = Json.toJson(notification);
            Notification fromJson1 = Json.fromJson(json, Notification.class);
            Assert.assertEquals(fromJson1.getClass(), notification.getClass());
            if (notification instanceof TimelineNotification) {
                TimelineNotification fromJson2 = Json.fromJson(json, TimelineNotification.class);
                Assert.assertEquals(fromJson2.getClass(), notification.getClass());
            }
            if (notification instanceof CommentNotification) {
                CommentNotification fromJson2 = Json.fromJson(json, CommentNotification.class);
                Assert.assertEquals(fromJson2.getClass(), notification.getClass());
            }

        }
    }

}
