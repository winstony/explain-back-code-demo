package config;

import java.lang.reflect.Field;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import play.libs.Json;
import markers.IntegrationalTest;

@Category(IntegrationalTest.class)
public class ConfigIT extends IntegrationalTest {
    @Test
    public void testObjectMapperIsOverridden()
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Field field = Json.class.getDeclaredField("objectMapper");
        field.setAccessible(true);
        Assert.assertEquals(field.get(Json.class), ObjectMapperHolder.getInstance());
    }
}
