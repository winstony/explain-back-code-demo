package service.queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import to.api.HistoryElement;
import to.api.Question;

public class RepeatIncorrectQueueBuilderTest {

    private List<Question> questionsPool = new ArrayList<>();
    private long userId = 200L;

    @Before
    public void init() {
        for (int i = 0; i < 10; i++) {
            Question question = new Question();
            question.id = i + 1L;
            questionsPool.add(question);
        }
    }

    @Test
    public void testShowsAllQuestions() {
        List<Question> queue =
                new RepeatIncorrectQueueBuilder(Collections.emptyList(), questionsPool, userId).buildQueue(10);
        Set<Long> ids = queue.stream().map(q -> q.id).collect(Collectors.toSet());
        Set<Long> poolIds = questionsPool.stream().map(q -> q.id).collect(Collectors.toSet());
        Assert.assertThat(ids, CoreMatchers.is(poolIds));
    }

    @Test
    public void testShowsIncorrectQuestion() {
        List<HistoryElement> records = new ArrayList<>();
        for (Question q: questionsPool) {
            HistoryElement record = new HistoryElement();
            record.question = q;
            record.answeredAt = System.currentTimeMillis() - 1000 + q.id;
            record.isCorrect = q.id == 4 ? false : true;
            records.add(record);
        }
        List<Question> queue = new RepeatIncorrectQueueBuilder(records, questionsPool, userId).buildQueue(1);
        Set<Long> ids = queue.stream().map(q -> q.id).collect(Collectors.toSet());
        Assert.assertThat(ids.iterator().next(), CoreMatchers.is(4L));
    }

    @Test
    public void testShowsIncorrectInOrder() {
        List<HistoryElement> records = new ArrayList<>();
        for (Question q : questionsPool) {
            HistoryElement record = new HistoryElement();
            record.question = q;
            record.answeredAt = System.currentTimeMillis() - 1000 + q.id;
            record.isCorrect = q.id == 4 || q.id == 6 ? false : true;
            records.add(record);
        }
        List<Question> queue = new RepeatIncorrectQueueBuilder(records, questionsPool, userId).buildQueue(2);
        Assert.assertThat(queue.get(0).id, CoreMatchers.is(4L));
        Assert.assertThat(queue.get(1).id, CoreMatchers.is(6L));
    }

    @Test
    public void testShowsUnansweredQuestion() {
        List<HistoryElement> records = new ArrayList<>();
        for (Question q: questionsPool) {
            if (q.id == 10) {
                continue;
            }
            HistoryElement record = new HistoryElement();
            record.question = q;
            record.answeredAt = System.currentTimeMillis() - 1000 + q.id;
            record.isCorrect = false;
            records.add(record);
        }
        List<Question> queue = new RepeatIncorrectQueueBuilder(records, questionsPool, userId).buildQueue(1);
        Set<Long> ids = queue.stream().map(q -> q.id).collect(Collectors.toSet());
        Assert.assertThat(ids.iterator().next(), CoreMatchers.is(10L));
    }
}
