package searchto;

import java.util.*;

import markers.IntegrationalTest;
import models.core.Choice;
import models.core.Question;
import models.core.TestRule;
import models.core.Tag;
import models.core.ChoiceText;
import models.core.User;
import models.core.Topic;
import play.test.*;
import util.PrintNameRule;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.junit.rules.MethodRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import static org.junit.Assert.*;
import static play.test.Helpers.*;

@Category(IntegrationalTest.class)
public class SearchTOAssemblerIT extends IntegrationalTest {

    @Test
    public void testGetTOFromChoice() {
        Choice choice = new Choice();
        ChoiceText choiceText = new ChoiceText("test text");
        choice.id = 1L;
        choice.choiceText = choiceText;
        ChoiceSearchTO choiceTO = SearchTOAssembler.getTOFromChoice(choice);
        assertEquals(SearchTOAssembler.getTOFromChoice(null), null);
        assertEquals(choiceTO.getId(), choice.id);
        assertEquals(choiceTO.getChoiceText(), choice.choiceText.text);
    }

    @Test
    public void testGetTOFromQuestion() {
        Question question = new Question();
        User user = new User();
        user.id = 7L;
        question.id = 5L;
        question.author = user;
        question.question = "test text";
        QuestionSearchTO questionTO = SearchTOAssembler.getTOFromQuestion(question);
        assertEquals(SearchTOAssembler.getTOFromQuestion(null), null);
        assertEquals(questionTO.getId(), question.id);
        assertEquals(questionTO.getAuthorId(), question.author.id);
        assertEquals(questionTO.getQuestion(), question.question);
    }

    @Test
    public void testGetTOFromRule() {
        // init rule
        TestRule rule = new TestRule();
        rule.id = 8L;
        rule.rule = "test rule";
        Topic ruletopic = new Topic();
        ruletopic.id = 15L;
        rule.topic = ruletopic;
        // creating list of tags
        List<Tag> tags = new ArrayList<Tag>();
        // tag1
        Tag tag1 = new Tag();
        Topic topic = new Topic();
        topic.id = 7L;
        tag1.id = 2L;
        tag1.name = "tag1";
        tag1.topic = topic;
        // tag2
        Tag tag2 = new Tag();
        Topic topic2 = new Topic();
        topic2.id = 9L;
        tag2.id = 4L;
        tag2.name = "tag2";
        tag2.topic = topic2;

        tags.add(tag1);
        tags.add(tag2);
        rule.tags = tags;

        RuleSearchTO ruleTO = SearchTOAssembler.getTOFromRule(rule);
        assertEquals(SearchTOAssembler.getTOFromRule(null), null);
        assertEquals(ruleTO.getId(), rule.id);
        assertEquals(ruleTO.getRule(), rule.rule);
        assertEquals(ruleTO.getTags().get(0).getId(), rule.tags.get(0).id);
        assertEquals(ruleTO.getTags().get(1).getId(), rule.tags.get(1).id);
        assertEquals(ruleTO.getTopicId(), rule.topic.id);
    }

    @Test
    public void testGetTOFromTag() {
        Tag tag = new Tag();
        Topic topic = new Topic();
        topic.id = 7L;
        tag.id = 2L;
        tag.name = "tag";
        tag.topic = topic;
        TagSearchTO tagTO = SearchTOAssembler.getTOFromTag(tag);
        assertEquals(tagTO.getId(), tag.id);
        assertEquals(tagTO.getTag(), tag.name);
        assertEquals(tagTO.getTopicId(), tag.topic.id);
        assertEquals(SearchTOAssembler.getTOFromTag(null), null);
    }
}
