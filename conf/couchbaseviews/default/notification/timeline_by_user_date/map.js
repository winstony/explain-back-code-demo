function (doc, meta) {
  if (doc.type && doc.type == "timeline_notification"
      && doc.userId != null
      && doc.updatedTime != null ) {
    emit([doc.userId, doc.updatedTime],
             {
                 unseen: !doc.seen,
                 unread: !doc.read,
             }
        );
  }
}