function(key, values, rereduce) {
  var result = {unseen: 0, unread: 0};
  for (i = 0; i < values.length; i++) {
        result.unseen += values[i].unseen;
        result.unread += values[i].unread;
  }
  return(result);
}