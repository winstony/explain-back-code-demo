function (doc, meta) {
  if (doc.type && doc.type == "achievement"
      && doc.userId != null
      && doc.timestamp != null ) {
    emit([doc.userId, doc.timestamp], null);
  }
}