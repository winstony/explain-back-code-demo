function (doc, meta) {
 if ( meta.type == "json"
       && doc.type
       && doc.type == "history_element"
       && doc.question != null
       && doc.question.id != null
      ) {
    var result = {};
    result.total = 1;
    result.questionId = doc.question.id;
    result.choices = {};
    result.correct = doc.isCorrect ? 1 : 0;
    doc.chosenChoiceIds.forEach( function ( choiceId ) {
      result.choices[choiceId] = 1;
    });
    emit(doc.question.id, result);
  }
}