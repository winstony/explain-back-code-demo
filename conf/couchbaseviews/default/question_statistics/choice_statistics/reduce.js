function(key, values, rereduce) {
  var result = {};
  result.total = 0;
  result.correct = 0;
  result.choices = {};
  values.forEach( function ( value ) {
    result.correct += value.correct;
    result.total += value.total;
    result.questionId = value.questionId;
    var choiceIds = Object.keys(value.choices);
    choiceIds.forEach(function(choiceId) {
      if ( result.choices[choiceId] == null ) {
        result.choices[choiceId] = 0;
      }
      result.choices[choiceId] += value.choices[choiceId];
    });
  });
  return result;
}