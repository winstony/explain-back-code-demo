function (doc, meta) {
    if (doc.type && doc.type == "comment"
            && doc.user != null
            && doc.user.id != null
            && doc.user.schoolId != null
            && doc.questionId != null
            && doc.question != null
            && doc.question.courseId != null
            && doc.timestamp != null
            && !doc.deleted
            ) {
        emit([
                doc.user.schoolId,
                doc.question.courseId,
                doc.timestamp
             ],
             null
            );
    }
}