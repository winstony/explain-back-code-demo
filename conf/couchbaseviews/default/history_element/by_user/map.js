function (doc, meta) {
    if (doc.type && doc.type == "history_element" 
            && doc.userId != null
            && doc.question 
            && doc.question.id != null
            && doc.question.courseId != null
            && doc.question.chapterId != null
            && doc.answeredAt
            ) {
        emit([
                doc.userId, 
                doc.question.courseId, 
                doc.question.chapterId,
                doc.answeredAt
             ],
             {
               correct: doc.isCorrect,
               score: doc.score,
               time: doc.answeredAt
             }
            );
    }
}