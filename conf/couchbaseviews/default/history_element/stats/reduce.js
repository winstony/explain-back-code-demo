function(key, values, rereduce) {
  var result = {score: 0, correct:0, total:0, lastCorrect:false, lastTime:0};
  for(i=0; i < values.length; i++) {
    if(rereduce) {
        result.total = result.total + values[i].total;
        result.correct = result.correct + values[i].correct;
        result.score = result.score + values[i].score;
    if (values[i].lastTime > result.lastTime) {
            result.lastTime = values[i].lastTime;
        result.lastCorrect = values[i].lastCorrect;
    }
    } else {
        result.total = result.total + 1;
        result.correct = result.correct + values[i].correct; 
        result.score = result.score + values[i].score;
    if (values[i].time > result.lastTime) {
            result.lastTime = values[i].time;
        result.lastCorrect = values[i].correct;
    }
    }
  }
  return(result);
}