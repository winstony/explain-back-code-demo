# --- add column "email" to school table

# --- !Ups

ALTER TABLE school ADD COLUMN email VARCHAR(255);

# --- !Downs

ALTER TABLE school DROP COLUMN email;