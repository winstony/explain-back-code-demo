# --- alter table question

# --- !Ups
ALTER TABLE question
ADD (quizful_id VARCHAR(20) NULL, question_type VARCHAR(20) NULL, language VARCHAR(2) NOT NULL DEFAULT 'en');

  

# --- !Downs

ALTER TABLE question
DROP quizful_id, DROP question_type, DROP language;