# --- add is_free to chapters

# --- !Ups

ALTER TABLE chapter ADD COLUMN is_free BOOLEAN NOT NULL DEFAULT false;

# --- !Downs

ALTER TABLE chapter DROP COLUMN is_free;
