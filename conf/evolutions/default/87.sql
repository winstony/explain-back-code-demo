# --- add username into user, change country to country_id

# --- !Ups

ALTER TABLE user ADD COLUMN username VARCHAR(255);
ALTER TABLE user CHANGE COLUMN country country_id BIGINT;
ALTER TABLE user ADD CONSTRAINT FOREIGN KEY fk_user_country (country_id) REFERENCES country(id);


# --- !Downs

ALTER TABLE user DROP FOREIGN KEY fk_user_country;
ALTER TABLE user CHANGE COLUMN country_id country VARCHAR(100);
ALTER TABLE user DROP COLUMN username;
