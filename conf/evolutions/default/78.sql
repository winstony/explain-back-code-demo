# --- alter table user_settings add id column:

# --- !Ups
ALTER TABLE user_settings ADD COLUMN id BIGINT(20) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (id);

# --- !Downs
ALTER TABLE user_settings CHANGE COLUMN id id BIGINT(20) NOT NULL ;

ALTER TABLE user_settings DROP PRIMARY KEY;

ALTER TABLE user_settings DROP COLUMN id;
