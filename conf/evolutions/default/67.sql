# --- add alias columns to topic, school, course

# --- !Ups

ALTER TABLE topic ADD COLUMN alias VARCHAR(50);
ALTER TABLE school ADD COLUMN alias VARCHAR(50);
ALTER TABLE course ADD COLUMN alias VARCHAR(50);

# --- !Downs

ALTER TABLE topic DROP COLUMN alias;
ALTER TABLE school DROP COLUMN alias;
ALTER TABLE course DROP COLUMN alias;