# --- add "deleted" to tables: payment_system, product, ps_product

# --- !Ups

ALTER TABLE payment_system ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE product ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE ps_product ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;

# --- !Downs

ALTER TABLE payment_system DROP COLUMN deleted;
ALTER TABLE product DROP COLUMN deleted;
ALTER TABLE ps_product DROP COLUMN deleted;
