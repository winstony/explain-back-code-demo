# --- Rename service to course

# --- !Ups

RENAME TABLE service TO course;
RENAME TABLE service_admin TO course_admin;

ALTER TABLE course_admin DROP FOREIGN KEY fk_service_admin_01;
ALTER TABLE course_admin CHANGE service_id course_id BIGINT;
ALTER TABLE course_admin ADD CONSTRAINT fk_service_admin_01 FOREIGN KEY (course_id) references course(id) ON DELETE cascade ON UPDATE restrict;

RENAME TABLE user_service TO user_course;

ALTER TABLE user_course DROP FOREIGN KEY fk_user_service_02;
ALTER TABLE user_course CHANGE service_id course_id BIGINT;
ALTER TABLE user_course ADD CONSTRAINT fk_user_service_02 FOREIGN KEY (course_id) references course(id) ON DELETE cascade ON UPDATE restrict;

ALTER TABLE task DROP FOREIGN KEY fk_task_01;
ALTER TABLE task CHANGE service_id course_id BIGINT;
ALTER TABLE Task ADD CONSTRAINT fk_task_01 FOREIGN KEY (course_id) references course(id) ON DELETE restrict ON UPDATE restrict;

ALTER TABLE question DROP FOREIGN KEY fk_question_service;
ALTER TABLE question CHANGE service_id course_id BIGINT;
ALTER TABLE question ADD CONSTRAINT fk_question_01 FOREIGN KEY (course_id) references course(id) ON DELETE SET NULL ON UPDATE restrict;


ALTER TABLE chapter DROP FOREIGN KEY fk_chaper_service;
ALTER TABLE chapter CHANGE service_id course_id BIGINT;
ALTER TABLE chapter ADD CONSTRAINT fk_chapter_01 FOREIGN KEY (course_id) references course(id) ON DELETE cascade ON UPDATE cascade;

# --- !Downs

RENAME TABLE course TO service;
RENAME TABLE course_admin TO service_admin;

ALTER TABLE service_admin DROP FOREIGN KEY fk_service_admin_01;
ALTER TABLE service_admin CHANGE course_id service_id BIGINT;
ALTER TABLE service_admin ADD CONSTRAINT fk_service_admin_01 FOREIGN KEY (service_id) references service(id) ON DELETE cascade ON UPDATE restrict;

RENAME TABLE user_course TO user_service;

ALTER TABLE user_service DROP FOREIGN KEY fk_user_service_02;
ALTER TABLE user_service CHANGE course_id service_id BIGINT;
ALTER TABLE user_service ADD CONSTRAINT fk_user_service_02 FOREIGN KEY (service_id) references service(id) ON DELETE cascade ON UPDATE restrict;

ALTER TABLE task DROP FOREIGN KEY fk_task_01;
ALTER TABLE task CHANGE course_id service_id BIGINT;
ALTER TABLE Task ADD CONSTRAINT fk_task_01 FOREIGN KEY (service_id) references service(id) ON DELETE restrict ON UPDATE restrict;

ALTER TABLE question DROP FOREIGN KEY fk_question_01;
ALTER TABLE question CHANGE course_id service_id BIGINT;
ALTER TABLE question ADD CONSTRAINT fk_question_service FOREIGN KEY (service_id) references service(id) ON DELETE restrict ON UPDATE restrict;

ALTER TABLE chapter DROP FOREIGN KEY fk_chapter_01;
ALTER TABLE chapter CHANGE course_id service_id BIGINT;
ALTER TABLE chapter ADD CONSTRAINT fk_chaper_service FOREIGN KEY (service_id) references service(id) ON DELETE restrict ON UPDATE restrict;

