# Changing some fields due to changes to Securesocial 2.0.12

# --- !Ups

ALTER TABLE SocialUserAdapter DROP COLUMN onlySocialAccount;
ALTER TABLE SocialUserAdapter ADD COLUMN first_name VARCHAR(255);
ALTER TABLE SocialUserAdapter ADD COLUMN last_name VARCHAR(255);
ALTER TABLE SocialUserAdapter CHANGE displayName full_name VARCHAR(255);
ALTER TABLE SocialUserAdapter DROP COLUMN is_email_verified;

# --- !Downs

ALTER TABLE SocialUserAdapter DROP COLUMN first_name;
ALTER TABLE SocialUserAdapter DROP COLUMN last_name ;
ALTER TABLE SocialUserAdapter CHANGE full_name displayName VARCHAR(255);
ALTER TABLE SocialUserAdapter ADD COLUMN is_email_verified BOOLEAN;
ALTER TABLE SocialUserAdapter ADD COLUMN onlySocialAccount BOOLEAN;
