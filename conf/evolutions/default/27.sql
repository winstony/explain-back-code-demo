# --- add school table

# --- !Ups

CREATE TABLE school (
  id                        BIGINT AUTO_INCREMENT NOT NULL,	
  creator_id                BIGINT,
  name                      VARCHAR(255),
  siteUrl                   VARCHAR(255),
  logoUrl                   VARCHAR(255),
  CONSTRAINT pk_school PRIMARY KEY (id)
);

ALTER TABLE school ADD CONSTRAINT fk_school_creator FOREIGN KEY (creator_id) references partner (id) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE service ADD COLUMN school_id BIGINT;
ALTER TABLE service ADD CONSTRAINT fk_service_school FOREIGN KEY (school_id) REFERENCES school (id) ON DELETE CASCADE ON UPDATE CASCADE;

# --- !Downs

ALTER TABLE service DROP FOREIGN KEY fk_service_school;
ALTER TABLE service DROP COLUMN school_id;

DROP TABLE IF EXISTS school;
