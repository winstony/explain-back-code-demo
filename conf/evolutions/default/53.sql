# --- add columns "activated" and "status" to tables: tags, question, testrule, task, topic

# --- !Ups


ALTER TABLE tags     ADD COLUMN activated BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE testrule ADD COLUMN activated BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE task     ADD COLUMN activated BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE topic    ADD COLUMN activated BOOLEAN NOT NULL DEFAULT false;

ALTER TABLE tags     ADD COLUMN status VARCHAR(255) NOT NULL DEFAULT 'PENDING';
ALTER TABLE question ADD COLUMN status VARCHAR(255) NOT NULL DEFAULT 'PENDING';
ALTER TABLE testrule ADD COLUMN status VARCHAR(255) NOT NULL DEFAULT 'PENDING';
ALTER TABLE task     ADD COLUMN status VARCHAR(255) NOT NULL DEFAULT 'PENDING';
ALTER TABLE topic    ADD COLUMN status VARCHAR(255) NOT NULL DEFAULT 'PENDING';

# --- !Downs

ALTER TABLE tags     DROP COLUMN activated;
ALTER TABLE testrule DROP COLUMN activated;
ALTER TABLE task     DROP COLUMN activated;
ALTER TABLE topic    DROP COLUMN activated;

ALTER TABLE tags     DROP COLUMN status;
ALTER TABLE question DROP COLUMN status;
ALTER TABLE testrule DROP COLUMN status;
ALTER TABLE task     DROP COLUMN status;
ALTER TABLE topic    DROP COLUMN status;


