# Initial db model generation
# --- !Ups

create table AnswerRecord (
  id                        bigint auto_increment not null,
  question_id               bigint,
  answer                    varchar(255),
  author_id                 bigint,
  correct                   boolean,
  postedAt                  timestamp,
  skipped                   boolean,
  comment                   varchar(255),
  constraint pk_AnswerRecord primary key (id))
;

create table chapter (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  constraint pk_chapter primary key (id))
;

create table question (
  id                        bigint auto_increment not null,
  ready                     boolean,
  author_id                 bigint,
  correctAnswer             varchar(255),
  QuestionWithUnderlines    text,
  wrongAnswers              varchar(255),
  otherCorrectAnswers       varchar(255),
  hint                      varchar(255),
  explanation               varchar(255),
  postedAt                  timestamp,
  constraint pk_question primary key (id))
;

create table security_role (
  id                        bigint auto_increment not null,
  role_name                 varchar(255),
  constraint pk_security_role primary key (id))
;

create table SocialUserAdapter (
  id                        bigint auto_increment not null,
  user_id                   bigint,
  displayName               varchar(255),
  email                     varchar(255),
  accountId                 varchar(255),
  provider                  varchar(255),
  onlySocialAccount         boolean,
  avatar_url                varchar(255),
  auth_method               varchar(17),
  is_email_verified         boolean,
  password                  varchar(255),
  salt                      varchar(255),
  constraint ck_SocialUserAdapter_auth_method check (auth_method in ('OAUTH1','OAUTH2','OPENID','USERNAME_PASSWORD')),
  constraint pk_SocialUserAdapter primary key (id))
;

create table tag (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  constraint pk_tag primary key (id))
;

create table TestRule (
  id                        bigint auto_increment not null,
  rule                      text,
  chapter_id                bigint,
  constraint pk_TestRule primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  email                     varchar(255),
  password                  varchar(255),
  fullname                  varchar(255),
  isAdmin                   boolean,
  constraint pk_user primary key (id))
;

create table user_permission (
  id                        bigint auto_increment not null,
  value                     varchar(255),
  constraint pk_user_permission primary key (id))
;


create table Question_Tag (
  question_id                    bigint not null,
  tags_id                        bigint not null,
  constraint pk_Question_Tag primary key (question_id, tags_id))
;

create table Question_TestRule (
  question_id                    bigint not null,
  rules_id                       bigint not null,
  constraint pk_Question_TestRule primary key (question_id, rules_id))
;

create table Tag_Tag (
  tag_id                         bigint not null,
  easierTags_id                  bigint not null,
  constraint pk_Tag_Tag primary key (tag_id, easierTags_id))
;

create table TestRule_Tag (
  testRule_id                    bigint not null,
  tags_id                        bigint not null,
  constraint pk_TestRule_Tag primary key (testRule_id, tags_id))
;

create table user_security_role (
  user_id                        bigint not null,
  security_role_id               bigint not null,
  constraint pk_user_security_role primary key (user_id, security_role_id))
;

create table user_user_permission (
  user_id                        bigint not null,
  user_permission_id             bigint not null,
  constraint pk_user_user_permission primary key (user_id, user_permission_id))
;
alter table AnswerRecord add constraint fk_AnswerRecord_question_1 foreign key (question_id) references question (id) on delete restrict on update restrict;
create index ix_AnswerRecord_question_1 on AnswerRecord (question_id);
alter table AnswerRecord add constraint fk_AnswerRecord_author_2 foreign key (author_id) references user (id) on delete restrict on update restrict;
create index ix_AnswerRecord_author_2 on AnswerRecord (author_id);
alter table question add constraint fk_question_author_3 foreign key (author_id) references user (id) on delete restrict on update restrict;
create index ix_question_author_3 on question (author_id);
alter table SocialUserAdapter add constraint fk_SocialUserAdapter_user_4 foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_SocialUserAdapter_user_4 on SocialUserAdapter (user_id);
alter table TestRule add constraint fk_TestRule_chapter_5 foreign key (chapter_id) references chapter (id) on delete restrict on update restrict;
create index ix_TestRule_chapter_5 on TestRule (chapter_id);



alter table Question_Tag add constraint fk_Question_Tag_question_01 foreign key (question_id) references question (id) on delete restrict on update restrict;

alter table Question_Tag add constraint fk_Question_Tag_tag_02 foreign key (tags_id) references tag (id) on delete restrict on update restrict;

alter table Question_TestRule add constraint fk_Question_TestRule_question_01 foreign key (question_id) references question (id) on delete restrict on update restrict;

alter table Question_TestRule add constraint fk_Question_TestRule_TestRule_02 foreign key (rules_id) references TestRule (id) on delete restrict on update restrict;

alter table Tag_Tag add constraint fk_Tag_Tag_tag_01 foreign key (tag_id) references tag (id) on delete restrict on update restrict;

alter table Tag_Tag add constraint fk_Tag_Tag_tag_02 foreign key (easierTags_id) references tag (id) on delete restrict on update restrict;

alter table TestRule_Tag add constraint fk_TestRule_Tag_TestRule_01 foreign key (testRule_id) references TestRule (id) on delete restrict on update restrict;

alter table TestRule_Tag add constraint fk_TestRule_Tag_tag_02 foreign key (tags_id) references tag (id) on delete restrict on update restrict;

alter table user_security_role add constraint fk_user_security_role_user_01 foreign key (user_id) references user (id) on delete restrict on update restrict;

alter table user_security_role add constraint fk_user_security_role_securit_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

alter table user_user_permission add constraint fk_user_user_permission_user_01 foreign key (user_id) references user (id) on delete restrict on update restrict;

alter table user_user_permission add constraint fk_user_user_permission_user__02 foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table if exists AnswerRecord;

drop table if exists chapter;

drop table if exists question;

drop table if exists Question_Tag;

drop table if exists Question_TestRule;

drop table if exists security_role;

drop table if exists SocialUserAdapter;

drop table if exists tag;

drop table if exists Tag_Tag;

drop table if exists TestRule;

drop table if exists TestRule_Tag;

drop table if exists user;

drop table if exists user_security_role;

drop table if exists user_user_permission;

drop table if exists user_permission;

SET FOREIGN_KEY_CHECKS=1;
