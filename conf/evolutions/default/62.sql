# add payment schema

# --- !Ups

CREATE TABLE click (
  id             bigint(20) NOT NULL AUTO_INCREMENT,
  created_at     timestamp NOT NULL,
  ip             varchar(255) NOT NULL,
  user_id        bigint(20) NOT NULL,
  ps_product_id  bigint(20),
  recurring      tinyint(1) DEFAULT 0 NOT NULL,
  paid           tinyint(1) DEFAULT 0 NOT NULL,
  transaction_id bigint(20),
  PRIMARY KEY (id));

CREATE TABLE product (
  id        bigint(20) NOT NULL AUTO_INCREMENT,
  name      varchar(255) NOT NULL,
  currency  varchar(3) NOT NULL,
  amount    decimal(5, 2) NOT NULL,
  activated tinyint(1) DEFAULT 0 NOT NULL,
  school_id bigint(20) NOT NULL,
  PRIMARY KEY (id));

CREATE TABLE country (
  id       bigint(20) NOT NULL AUTO_INCREMENT,
  name     varchar(255) NOT NULL,
  code     varchar(3) NOT NULL,
  currency varchar(3) NOT NULL,
  PRIMARY KEY (id));

CREATE TABLE payment_system (
  id          bigint(20) NOT NULL AUTO_INCREMENT,
  fixed_fee   decimal(5, 2) NOT NULL,
  percent_fee decimal(5, 2) NOT NULL,
  name        varchar(255) NOT NULL,
  PRIMARY KEY (id));

CREATE TABLE ps_product (
  id                bigint(20) NOT NULL AUTO_INCREMENT,
  payment_system_id bigint(20) NOT NULL,
  product_id        bigint(20) NOT NULL,
  base_amount       decimal(5, 2) NOT NULL,
  base_currency     varchar(3) NOT NULL,
  fixed_fee         decimal(5, 2) NOT NULL,
  percent_fee       decimal(5, 2) NOT NULL,
  raw_amount        decimal(5, 2) NOT NULL,
  country_id        bigint(20) NOT NULL,
  PRIMARY KEY (id));

CREATE TABLE user_product (
  id         bigint(20) NOT NULL AUTO_INCREMENT,
  user_id    bigint(20) NOT NULL,
  product_id bigint(20) NOT NULL,
  time_start timestamp NOT NULL,
  time_end   timestamp NOT NULL,
  click_id   bigint(20) NOT NULL,
  PRIMARY KEY (id));

ALTER TABLE click ADD INDEX FKclick780211 (user_id), ADD CONSTRAINT FKclick780211 FOREIGN KEY (user_id) REFERENCES `user` (id);
ALTER TABLE click ADD INDEX FKclick563319 (ps_product_id), ADD CONSTRAINT FKclick563319 FOREIGN KEY (ps_product_id) REFERENCES ps_product (id);
ALTER TABLE ps_product ADD INDEX FKps_product907714 (payment_system_id), ADD CONSTRAINT FKps_product907714 FOREIGN KEY (payment_system_id) REFERENCES payment_system (id);
ALTER TABLE ps_product ADD INDEX FKps_product780690 (product_id), ADD CONSTRAINT FKps_product780690 FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE ps_product ADD INDEX FKps_product376364 (country_id), ADD CONSTRAINT FKps_product376364 FOREIGN KEY (country_id) REFERENCES country (id);
ALTER TABLE user_product ADD INDEX FKuser_produ648096 (user_id), ADD CONSTRAINT FKuser_produ648096 FOREIGN KEY (user_id) REFERENCES `user` (id);
ALTER TABLE user_product ADD INDEX FKuser_produ253619 (product_id), ADD CONSTRAINT FKuser_produ253619 FOREIGN KEY (product_id) REFERENCES product (id);
ALTER TABLE user_product ADD INDEX FKuser_produ585869 (click_id), ADD CONSTRAINT FKuser_produ585869 FOREIGN KEY (click_id) REFERENCES click (id);
ALTER TABLE product ADD INDEX FKproduct990389 (school_id), ADD CONSTRAINT FKproduct990389 FOREIGN KEY (school_id) REFERENCES school (id);

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS click;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS country;
DROP TABLE IF EXISTS payment_system;
DROP TABLE IF EXISTS ps_product;
DROP TABLE IF EXISTS user_product;
SET FOREIGN_KEY_CHECKS=1;
