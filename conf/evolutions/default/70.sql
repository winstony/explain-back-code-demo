# --- drop "recurrence_rate", "duration" from Click, add "subscription_length", "subscription_type" to Product

# --- !Ups

ALTER TABLE click DROP COLUMN recurrence_rate;
ALTER TABLE click DROP COLUMN duration;

ALTER TABLE product ADD subscription_length INT NOT NULL DEFAULT 1;
ALTER TABLE product ADD subscription_type VARCHAR(255) NOT NULL DEFAULT 'Month';

# --- !Downs

ALTER TABLE click ADD recurrence_rate VARCHAR(255);
ALTER TABLE click ADD duration VARCHAR(255);

ALTER TABLE product DROP COLUMN subscription_length;
ALTER TABLE product DROP COLUMN subscription_type;
