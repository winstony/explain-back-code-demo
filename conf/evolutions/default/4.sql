# move question answer options to choices tables

# --- !Ups

create table choice_text (
  id                        bigint auto_increment not null,
  text                      varchar(255) not null,
  constraint pk_choice_text primary key (id),
  CONSTRAINT u_choice_text_t UNIQUE (text))
;

create table choice (
  id                        bigint auto_increment not null,
  choice_text_id            bigint not null,
  question_id               bigint not null,
  correct                   boolean not null,
  constraint pk_choice      primary key (id),
  CONSTRAINT u_choice_cti_qi UNIQUE (choice_text_id, question_id))
;

alter table choice ADD constraint fk_choice_choice_text foreign key (choice_text_id) references choice_text (id) on delete RESTRICT on update CASCADE;
alter table choice ADD constraint fk_choice_question    foreign key (question_id)    references question    (id) on delete CASCADE  on update CASCADE; 

alter table AnswerRecord add column choice_id bigint;
alter table AnswerRecord ADD constraint fk_AnswerRecord_choice foreign key (choice_id) references choice (id) on delete RESTRICT on update CASCADE;

# --- !Downs

alter table AnswerRecord DROP FOREIGN KEY fk_AnswerRecord_choice;
alter table AnswerRecord DROP column choice_id;

alter table choice DROP FOREIGN KEY fk_choice_question;
alter table choice DROP FOREIGN KEY fk_choice_choice_text;

DROP TABLE IF EXISTS choice;
DROP TABLE IF EXISTS choice_text;
