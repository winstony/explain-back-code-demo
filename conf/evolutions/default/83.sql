# --- create table sale

# --- !Ups


CREATE TABLE sale (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  ps_product_id BIGINT(20) NOT NULL,
  discount_fee DECIMAL(5,2) NULL,
  dicsount_fixed DECIMAL(5,2) NULL,
  date_start TIMESTAMP NULL,
  date_end TIMESTAMP NULL,
  date_created TIMESTAMP NULL,
  author_id BIGINT(20) NULL,
  creator_id BIGINT(20) NULL,
  status VARCHAR(20) NOT NULL DEFAULT 'PENDING',
  ready BIT(1) NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_ps_product_id_idx (ps_product_id ASC),
  INDEX fk_author_id_idx (author_id ASC),
  INDEX fk_creator_id_idx (creator_id ASC),
  CONSTRAINT fk_ps_product_id
    FOREIGN KEY (ps_product_id)
    REFERENCES ps_product (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_author_id
    FOREIGN KEY (author_id)
    REFERENCES user (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_creator_id
    FOREIGN KEY (creator_id)
    REFERENCES partner (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

# --- !Downs

DROP TABLE IF EXISTS sale;
