# --- create table topic

# --- !Ups

CREATE TABLE topic (
  id                        BIGINT auto_increment NOT NULL,
  name                      VARCHAR(255),
  CONSTRAINT pk_topic PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE IF EXISTS topic;