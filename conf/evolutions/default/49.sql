# --- Drop columns: "wrongAnswers", "correctAnswer", "otherCorrectAnswers" in "question" table
# --- Rename "QuestionWithUnderlines" to "text" in "question" table

# --- !Ups

ALTER TABLE question DROP COLUMN wrongAnswers;
ALTER TABLE question DROP COLUMN correctAnswer;
ALTER TABLE question DROP COLUMN otherCorrectAnswers;
ALTER TABLE question CHANGE QuestionWithUnderlines `text` VARCHAR(3000);

# --- !Downs

ALTER TABLE question ADD COLUMN wrongAnswers VARCHAR(255);
ALTER TABLE question ADD COLUMN correctAnswer VARCHAR(255);
ALTER TABLE question ADD COLUMN otherCorrectAnswers VARCHAR(255);
ALTER TABLE question CHANGE `text` QuestionWithUnderlines text;