# --- add posted_at and last_edited_at to service

# --- !Ups

ALTER TABLE service ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';
ALTER TABLE service ADD COLUMN last_edited_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE service DROP COLUMN posted_at;
ALTER TABLE service DROP COLUMN last_edited_at;