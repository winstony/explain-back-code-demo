# move columns 'code_style', 'time_zone', 'email_notification', 'interface_language' to 'user_settings'; move 'first_name', 'last_name', 'date_of_birth' to 'user'

# --- !Ups

ALTER TABLE user_settings DROP COLUMN date_of_birth;
ALTER TABLE user_settings DROP COLUMN first_name;
ALTER TABLE user_settings DROP COLUMN last_name;

ALTER TABLE user_settings ADD COLUMN code_style VARCHAR(100);
ALTER TABLE user_settings ADD COLUMN email_notification BOOLEAN;
ALTER TABLE user_settings ADD COLUMN interface_language VARCHAR(2);
ALTER TABLE user_settings ADD COLUMN time_zone VARCHAR(100);

ALTER TABLE user DROP COLUMN code_style;
ALTER TABLE user DROP COLUMN email_notification;
ALTER TABLE user DROP COLUMN interface_language;
ALTER TABLE user DROP COLUMN time_zone;

ALTER TABLE user ADD COLUMN date_of_birth TIMESTAMP NULL;
ALTER TABLE user ADD COLUMN first_name VARCHAR(150);
ALTER TABLE user ADD COLUMN last_name VARCHAR(150);

# --- !Downs

ALTER TABLE user_settings ADD COLUMN date_of_birth TIMESTAMP NULL;
ALTER TABLE user_settings ADD COLUMN first_name VARCHAR(150);
ALTER TABLE user_settings ADD COLUMN last_name VARCHAR(150);

ALTER TABLE user_settings DROP COLUMN code_style;
ALTER TABLE user_settings DROP COLUMN email_notification;
ALTER TABLE user_settings DROP COLUMN interface_language;
ALTER TABLE user_settings DROP COLUMN time_zone;

ALTER TABLE user ADD COLUMN code_style VARCHAR(100);
ALTER TABLE user ADD COLUMN email_notification BOOLEAN;
ALTER TABLE user ADD COLUMN interface_language VARCHAR(2);
ALTER TABLE user ADD COLUMN time_zone VARCHAR(6);

ALTER TABLE user DROP COLUMN date_of_birth;
ALTER TABLE user DROP COLUMN first_name;
ALTER TABLE user DROP COLUMN last_name;

