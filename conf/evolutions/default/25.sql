# --- add choice_rule table

# --- !Ups

CREATE TABLE choice_rule (
  id                 				BIGINT AUTO_INCREMENT NOT NULL,	
  choice_id                       	BIGINT NOT NULL,
  testrule_id                       BIGINT NOT NULL,
  CONSTRAINT pk_choice_rule PRIMARY KEY (id)
);


ALTER TABLE choice_rule ADD CONSTRAINT fk_choice_rule_01 FOREIGN KEY (choice_id) references choice(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE choice_rule ADD CONSTRAINT fk_choice_rule_02 FOREIGN KEY (testrule_id) references testrule(id) ON DELETE CASCADE ON UPDATE CASCADE;


# --- !Downs

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS choice_rule;

SET FOREIGN_KEY_CHECKS = 1;