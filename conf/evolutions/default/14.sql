# --- create order at question_task

# --- !Ups

ALTER TABLE question_task ADD COLUMN `order_number` bigint;

# --- !Downs

ALTER TABLE question_task DROP COLUMN `order_number`;