# add payment schema

# --- !Ups

CREATE TABLE country_state (
  id       bigint(20) NOT NULL AUTO_INCREMENT,
  name     varchar(255) NOT NULL,
  code     varchar(3) NOT NULL,
  country_id bigint(20) NOT NULL,
  PRIMARY KEY (id));

ALTER TABLE country_state ADD INDEX FKstate_country (country_id), ADD CONSTRAINT FKcountry_state_country FOREIGN KEY (country_id) REFERENCES country (id);

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS country_state;
SET FOREIGN_KEY_CHECKS=1;