# drop contraint before adding countries

# --- !Ups

ALTER TABLE ps_product DROP FOREIGN KEY FKps_product376364;
delete from country where id between 1 and 250;

# --- !Downs

ALTER TABLE ps_product ADD CONSTRAINT FKps_product376364 FOREIGN KEY (country_id) REFERENCES country (id);