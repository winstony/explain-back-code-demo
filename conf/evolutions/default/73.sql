# --- modify table "user_settings": 

# --- !Ups
ALTER TABLE user
ADD (public BOOLEAN NULL, description VARCHAR(500) NULL, linkedin VARCHAR(300) NULL, code_style VARCHAR(100) NULL, email_notification BOOLEAN NULL, interface_language VARCHAR(2) NULL, country VARCHAR(100) NULL, city VARCHAR(100) NULL, time_zone VARCHAR(6) NULL, translator BOOLEAN  NOT NULL DEFAULT FALSE);

# --- !Downs
ALTER TABLE user 
DROP public, DROP description, DROP linkedin, DROP code_style, DROP email_notification, DROP interface_language, DROP country, DROP city, DROP time_zone, DROP translator;