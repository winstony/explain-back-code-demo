# --- add ready to tags

# --- !Ups

ALTER TABLE tags ADD COLUMN ready BOOLEAN NOT NULL DEFAULT 0;

# --- !Downs

ALTER TABLE tags DROP COLUMN ready;
