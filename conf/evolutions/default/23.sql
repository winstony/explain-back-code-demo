# --- add is_public to question

# --- !Ups

ALTER TABLE question ADD COLUMN is_public BOOLEAN;

# --- !Downs

ALTER TABLE question DROP COLUMN is_public;
