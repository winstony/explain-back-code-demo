# --- add ready to testrule

# --- !Ups

ALTER TABLE testrule ADD COLUMN ready BOOLEAN NOT NULL DEFAULT 0;

# --- !Downs

ALTER TABLE testrule DROP COLUMN ready;