# move to SecureSocial 2.0.12

# --- !Ups

alter table question DROP FOREIGN KEY fk_question_author_3;
alter table question ADD constraint fk_question_author_3 foreign key (author_id) references user (id) on delete set null on update restrict;


# --- !Downs

alter table question DROP FOREIGN KEY  fk_question_author_3;
alter table question ADD constraint fk_question_author_3 foreign key (author_id) references user (id) on delete restrict on update restrict;
