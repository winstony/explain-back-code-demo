# --- add columns "activated", "status", "short_description", "description" to tables: school, service

# --- !Ups

ALTER TABLE school ADD COLUMN activated BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE school ADD COLUMN status VARCHAR(255) NOT NULL DEFAULT 'PENDING';
ALTER TABLE school ADD COLUMN short_description CHAR(140);
ALTER TABLE school ADD COLUMN description VARCHAR(2000);
ALTER TABLE service ADD COLUMN activated BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE service ADD COLUMN status VARCHAR(255) NOT NULL DEFAULT 'PENDING';
ALTER TABLE service ADD COLUMN short_description CHAR(140);
ALTER TABLE service ADD COLUMN description VARCHAR(2000);

# --- !Downs

ALTER TABLE school DROP COLUMN activated;
ALTER TABLE school DROP COLUMN status;
ALTER TABLE school DROP COLUMN short_description;
ALTER TABLE school DROP COLUMN description;
ALTER TABLE service DROP COLUMN activated;
ALTER TABLE service DROP COLUMN status;
ALTER TABLE service DROP COLUMN short_description;
ALTER TABLE service DROP COLUMN description;