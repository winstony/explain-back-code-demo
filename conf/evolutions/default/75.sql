# --- modify table "question": 

# --- !Ups
ALTER TABLE question
ADD (uuid VARCHAR(36) NULL);

# --- !Downs
ALTER TABLE question 
DROP uuid;