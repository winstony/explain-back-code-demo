# --- alter table choice

# --- !Ups
ALTER TABLE choice
ADD (text VARCHAR(255) NULL, language VARCHAR(2) NOT NULL DEFAULT 'en');

  

# --- !Downs

ALTER TABLE choice
DROP text, DROP language;
