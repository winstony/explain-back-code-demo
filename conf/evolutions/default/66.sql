# --- add created_at to payment_log

# --- !Ups

ALTER TABLE payment_log ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE payment_log DROP COLUMN created_at;