# --- add alias columns to chapter

# --- !Ups

ALTER TABLE chapter ADD COLUMN alias VARCHAR(50);

# --- !Downs

ALTER TABLE chapter DROP COLUMN alias;
