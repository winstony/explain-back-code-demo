# --- add author_id, posted_at and last_edited_at to testrule

# --- !Ups

ALTER TABLE testrule ADD COLUMN author_id BIGINT;
ALTER TABLE testrule ADD CONSTRAINT fk_testrule_author FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE SET NULL ON UPDATE RESTRICT;
ALTER TABLE testrule ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';
ALTER TABLE testrule ADD COLUMN last_edited_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE testrule DROP FOREIGN KEY fk_testrule_author;
ALTER TABLE testrule DROP COLUMN author_id;
ALTER TABLE testrule DROP COLUMN posted_at;
ALTER TABLE testrule DROP COLUMN last_edited_at;