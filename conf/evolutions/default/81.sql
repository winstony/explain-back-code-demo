# --- create table course_multilangs, add column 'language', to 'course'

# --- !Ups

CREATE TABLE course_multilangs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  course_id BIGINT NOT NULL,
  language VARCHAR(2) NOT NULL DEFAULT 'en',
  name VARCHAR(255) NOT NULL,
  posted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_edited_at TIMESTAMP,
  author_id BIGINT,
  creator_id BIGINT,
  deleted BOOLEAN NOT NULL DEFAULT 0,
  status VARCHAR(20) NOT NULL DEFAULT 'PENDING',
  activated BOOLEAN NOT NULL DEFAULT 0,
  constraint pk_course_multilangs PRIMARY KEY (id));

ALTER TABLE course_multilangs add constraint fk_course_multilangs_course1 foreign key (course_id) references course(id) on delete cascade on update cascade;
create index ix_course_multilangs_course1 on course_multilangs(course_id);

ALTER TABLE course_multilangs add constraint fk_course_multilangs_author1 foreign key (author_id) references user(id) on delete set null on update set null;
create index ix_course_multilangs_author1 on course_multilangs(author_id);

ALTER TABLE course_multilangs add constraint fk_course_multilangs_creator1 foreign key (creator_id) references partner(id) on delete set null on update set null;
create index ix_course_multilangs_creator1 on course_multilangs(creator_id);


ALTER TABLE course ADD COLUMN language VARCHAR(2) NOT NULL DEFAULT 'en';

# --- !Downs

DROP TABLE IF EXISTS course_multilangs;

ALTER TABLE course DROP COLUMN language;
