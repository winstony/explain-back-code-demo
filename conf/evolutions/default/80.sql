# --- create table chapter_multilangs, add columns 'language', 'activated', 'status' to 'chapter'

# --- !Ups

CREATE TABLE chapter_multilangs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  chapter_id BIGINT NOT NULL,
  language VARCHAR(2) NOT NULL DEFAULT 'en',
  name VARCHAR(255) NOT NULL,
  posted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_edited_at TIMESTAMP,
  author_id BIGINT,
  creator_id BIGINT,
  deleted BOOLEAN NOT NULL DEFAULT 0,
  status VARCHAR(20) NOT NULL DEFAULT 'PENDING',
  activated BOOLEAN NOT NULL DEFAULT 0,
  constraint pk_chapter_multilangs PRIMARY KEY (id));

ALTER TABLE chapter_multilangs add constraint fk_chapter_multilangs_chapter1 foreign key (chapter_id) references chapter(id) on delete cascade on update cascade;
create index ix_chapter_multilangs_chapter1 on chapter_multilangs(chapter_id);

ALTER TABLE chapter_multilangs add constraint fk_chapter_multilangs_author1 foreign key (author_id) references user(id) on delete set null on update set null;
create index ix_chapter_multilangs_author1 on chapter_multilangs(author_id);

ALTER TABLE chapter_multilangs add constraint fk_chapter_multilangs_creator1 foreign key (creator_id) references partner(id) on delete set null on update set null;
create index ix_chapter_multilangs_creator1 on chapter_multilangs(creator_id);


ALTER TABLE chapter ADD COLUMN language VARCHAR(2) NOT NULL DEFAULT 'en';
ALTER TABLE chapter ADD COLUMN activated BOOLEAN NOT NULL DEFAULT 0;
ALTER TABLE chapter ADD COLUMN status VARCHAR(255) NOT NULL DEFAULT 'PENDING';


# --- !Downs

DROP TABLE IF EXISTS chapter_multilangs;

ALTER TABLE chapter DROP COLUMN language;
ALTER TABLE chapter DROP COLUMN activated;
ALTER TABLE chapter DROP COLUMN status;
