# drop contraint before adding countries

# --- !Ups

ALTER TABLE ps_product ADD CONSTRAINT FKps_product376364 FOREIGN KEY (country_id) REFERENCES country (id);

# --- !Downs

ALTER TABLE ps_product DROP FOREIGN KEY FKps_product376364;