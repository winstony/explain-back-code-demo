# --- add topic to tags, rules and services

# --- !Ups

ALTER TABLE tag ADD COLUMN topic_id BIGINT;
ALTER TABLE service ADD COLUMN topic_id BIGINT;
ALTER TABLE testrule ADD COLUMN topic_id BIGINT;

ALTER TABLE tag ADD CONSTRAINT fk_tag_topic FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE service ADD CONSTRAINT fk_service_topic FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE testrule ADD CONSTRAINT fk_testrule_topic FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE CASCADE ON UPDATE CASCADE;

# --- !Downs

ALTER TABLE tag DROP FOREIGN KEY fk_tag_topic;
ALTER TABLE service DROP FOREIGN KEY fk_service_topic;
ALTER TABLE testrule DROP FOREIGN KEY fk_testrule_topic;

ALTER TABLE tag DROP COLUMN topic_id;
ALTER TABLE service DROP COLUMN topic_id;
ALTER TABLE testrule DROP COLUMN topic_id;

