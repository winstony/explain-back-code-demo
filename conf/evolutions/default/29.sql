# --- add author_id, posted_at and last_edited_at to chapter

# --- !Ups

ALTER TABLE chapter ADD COLUMN author_id BIGINT;
ALTER TABLE chapter ADD CONSTRAINT fk_chapter_author FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE SET NULL ON UPDATE RESTRICT;
ALTER TABLE chapter ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';
ALTER TABLE chapter ADD COLUMN last_edited_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE chapter DROP FOREIGN KEY fk_chapter_author;
ALTER TABLE chapter DROP COLUMN author_id;
ALTER TABLE chapter DROP COLUMN posted_at;
ALTER TABLE chapter DROP COLUMN last_edited_at;