# add partner to course admin table

# --- !Ups

ALTER TABLE course_admin ADD COLUMN partner_id BIGINT;
ALTER TABLE course_admin ADD CONSTRAINT `fk_course_admin_partner` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

# --- !Downs

ALTER TABLE course_admin DROP FOREIGN KEY `fk_course_admin_partner`;
ALTER TABLE course_admin DROP partner_id;