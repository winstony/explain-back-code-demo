# --- create order at choice_rule

# --- !Ups

ALTER TABLE choice_rule ADD COLUMN `order_number` integer;

# --- !Downs

ALTER TABLE choice_rule DROP COLUMN `order_number`;
