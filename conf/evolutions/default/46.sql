# --- delete table question_service

# --- !Ups

DROP TABLE IF EXISTS question_service;

# --- !Downs

CREATE TABLE question_service (
  question_id                       bigint NOT NULL,
  service_id                        bigint NOT NULL)
;

ALTER TABLE question_service ADD CONSTRAINT pk_question_service PRIMARY KEY (question_id, service_id);
ALTER TABLE question_service ADD CONSTRAINT fk_question_service_01 FOREIGN KEY (question_id) references question(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_question_service_01 on question_service(question_id);
ALTER TABLE question_service ADD CONSTRAINT fk_question_service_02 FOREIGN KEY (service_id) references service(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_question_service_02 on question_service(service_id);