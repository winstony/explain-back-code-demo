# --- add posted_at and last_edited_at to school

# --- !Ups

ALTER TABLE school ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';
ALTER TABLE school ADD COLUMN last_edited_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE school DROP COLUMN posted_at;
ALTER TABLE school DROP COLUMN last_edited_at;