# --- !Ups

ALTER TABLE AnswerRecord DROP FOREIGN KEY fk_AnswerRecord_choice;

# --- !Downs

alter table AnswerRecord ADD constraint fk_AnswerRecord_choice foreign key (choice_id) references choice (id) on delete RESTRICT on update CASCADE;
