# --- add posted_at to question

# --- !Ups

ALTER TABLE question ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE question DROP COLUMN posted_at;