# --- modify table "user": 

# --- !Ups
ALTER TABLE user_settings
ADD (date_of_birth TIMESTAMP NULL, first_name VARCHAR(150) NULL, last_name VARCHAR(150) NULL);

# --- !Downs
ALTER TABLE user 
DROP date_of_birth, DROP first_name, DROP last_name;