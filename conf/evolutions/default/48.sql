# --- Add column "creator_id" to tables: tags, question, testrule, task, chapter, topic

# --- !Ups

ALTER TABLE tags ADD COLUMN creator_id BIGINT;
ALTER TABLE tags ADD CONSTRAINT fk_tags_partner FOREIGN KEY (creator_id) REFERENCES partner (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE question ADD COLUMN creator_id BIGINT;
ALTER TABLE question ADD CONSTRAINT fk_question_partner FOREIGN KEY (creator_id) REFERENCES partner (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE testrule ADD COLUMN creator_id BIGINT;
ALTER TABLE testrule ADD CONSTRAINT fk_testrule_partner FOREIGN KEY (creator_id) REFERENCES partner (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE task ADD COLUMN creator_id BIGINT;
ALTER TABLE task ADD CONSTRAINT fk_task_partner FOREIGN KEY (creator_id) REFERENCES partner (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE chapter ADD COLUMN creator_id BIGINT;
ALTER TABLE chapter ADD CONSTRAINT fk_chapter_partner FOREIGN KEY (creator_id) REFERENCES partner (id) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE topic ADD COLUMN creator_id BIGINT;
ALTER TABLE topic ADD CONSTRAINT fk_topic_partner FOREIGN KEY (creator_id) REFERENCES partner (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE service DROP FOREIGN KEY fk_service_01;
ALTER TABLE service CHANGE partner_id creator_id BIGINT;
ALTER TABLE service ADD CONSTRAINT fk_service_01 FOREIGN KEY (creator_id) references partner(id) ON DELETE restrict ON UPDATE restrict;

# --- !Downs

ALTER TABLE tags DROP FOREIGN KEY fk_tags_partner;
ALTER TABLE tags DROP COLUMN creator_id;
ALTER TABLE question DROP FOREIGN KEY fk_question_partner;
ALTER TABLE question DROP COLUMN creator_id;
ALTER TABLE testrule DROP FOREIGN KEY fk_testrule_partner;
ALTER TABLE testrule DROP COLUMN creator_id;
ALTER TABLE task DROP FOREIGN KEY fk_task_partner;
ALTER TABLE task DROP COLUMN creator_id;
ALTER TABLE chapter DROP FOREIGN KEY fk_chapter_partner;
ALTER TABLE chapter DROP COLUMN creator_id;
ALTER TABLE topic DROP FOREIGN KEY fk_topic_partner;
ALTER TABLE topic DROP COLUMN creator_id;

ALTER TABLE service DROP FOREIGN KEY fk_service_01;
ALTER TABLE service CHANGE creator_id partner_id BIGINT;
ALTER TABLE service ADD CONSTRAINT fk_service_01 FOREIGN KEY (partner_id) references partner(id) ON DELETE restrict ON UPDATE restrict;
