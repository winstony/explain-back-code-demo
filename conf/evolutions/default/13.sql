# --- add name column to task table

# --- !Ups

ALTER TABLE task ADD COLUMN name VARCHAR(255) NOT NULL;

# --- !Downs

ALTER TABLE task DROP COLUMN name;