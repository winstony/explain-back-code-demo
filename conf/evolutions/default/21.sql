# --- add service to chapter

# --- !Ups

ALTER TABLE chapter ADD COLUMN service_id BIGINT;

ALTER TABLE chapter ADD CONSTRAINT fk_chaper_service FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE RESTRICT ON UPDATE CASCADE;

# --- !Downs

ALTER TABLE chapter DROP FOREIGN KEY fk_chaper_service;

ALTER TABLE chapter DROP COLUMN service_id;
