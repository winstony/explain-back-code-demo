# --- create table translate_log: 

# --- !Ups
CREATE TABLE translate_log (
  id			bigint(20) NOT NULL AUTO_INCREMENT,
  user_id		bigint(20) NOT NULL,
  question_id	bigint(20) NOT NULL,
  action		varchar(10) NOT NULL,
  time			TIMESTAMP NOT NULL,
  PRIMARY KEY (id));

ALTER TABLE translate_log ADD CONSTRAINT fk_UserTL FOREIGN KEY (user_id) REFERENCES user(id);
ALTER TABLE translate_log ADD CONSTRAINT fk_QuestionTL FOREIGN KEY (question_id) REFERENCES question(id);

# --- !Downs
DROP TABLE IF EXISTS translate_log;