# --- add chapter_question and chapter_rule tables

# --- !Ups

CREATE TABLE chapter_question (
  question_id                       BIGINT NOT NULL,
  chapter_id                        BIGINT NOT NULL
);

CREATE TABLE chapter_rule (
  rule_id                           BIGINT NOT NULL,
  chapter_id                        BIGINT NOT NULL
);

ALTER TABLE chapter_question ADD CONSTRAINT pk_chapter_question PRIMARY KEY (chapter_id, question_id);
ALTER TABLE chapter_rule ADD CONSTRAINT pk_chapter_rule PRIMARY KEY (chapter_id, rule_id);

ALTER TABLE chapter_question ADD CONSTRAINT fk_chapter_question_01 FOREIGN KEY (question_id) references question(id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX ix_chapter_question_01 on chapter_question(question_id);
ALTER TABLE chapter_question ADD CONSTRAINT fk_chapter_question_02 FOREIGN KEY (chapter_id) references chapter(id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX ix_chapter_question_02 on chapter_question(chapter_id);

ALTER TABLE chapter_rule ADD CONSTRAINT fk_chapter_rule_01 FOREIGN KEY (rule_id) references testrule(id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX ix_chapter_rule_01 on chapter_rule(rule_id);
ALTER TABLE chapter_rule ADD CONSTRAINT fk_chapter_rule_02 FOREIGN KEY (chapter_id) references chapter(id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX ix_chapter_rule_02 on chapter_rule(chapter_id);

# --- !Downs

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS chapter_question;
DROP TABLE IF EXISTS chapter_rule;

SET FOREIGN_KEY_CHECKS = 1;