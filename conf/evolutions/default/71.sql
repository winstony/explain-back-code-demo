# --- add "description" to Product

# --- !Ups

ALTER TABLE product ADD description VARCHAR(2000);

# --- !Downs

ALTER TABLE product DROP COLUMN description;
