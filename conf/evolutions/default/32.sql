# --- add posted_at and last_edited_at to user

# --- !Ups

ALTER TABLE user ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';
ALTER TABLE user ADD COLUMN last_edited_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE user DROP COLUMN posted_at;
ALTER TABLE user DROP COLUMN last_edited_at;