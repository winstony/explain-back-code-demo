# Next db model generation
# --- !Ups

CREATE TABLE question_question (
  parent_id                          bigint NOT NULL,
  child_id                           bigint NOT NULL)
;

CREATE TABLE question_task (
  question_id                    bigint NOT NULL,
  task_id                        bigint NOT NULL)
;

CREATE TABLE task (
  id                 bigint AUTO_INCREMENT NOT NULL,
  type               varchar(255),
  service_id         bigint,
  CONSTRAINT pk_task PRIMARY KEY (id))
;

CREATE TABLE question_service (
  question_id                       bigint NOT NULL,
  service_id                        bigint NOT NULL)
;

CREATE TABLE service (
  id                     bigint AUTO_INCREMENT NOT NULL,
  name                   varchar(255),
  apikey                 varchar(255),
  partner_id             bigint,
  logo                   varchar(255),
  secretkey              varchar(255),
  CONSTRAINT pk_service PRIMARY KEY (id))
;

CREATE TABLE user_service (
  user_id                       bigint NOT NULL,
  service_id                    bigint NOT NULL)
;

CREATE TABLE partner (
  id                     bigint AUTO_INCREMENT NOT NULL,
  user_id                bigint,
  CONSTRAINT pk_partner PRIMARY KEY (id))
;

CREATE TABLE service_admin (
  id                            bigint AUTO_INCREMENT NOT NULL,
  user_id                       bigint,
  role_id                       bigint,
  service_id                    bigint,
  CONSTRAINT pk_service_admin PRIMARY KEY (id))
;

CREATE TABLE partner_admin_permission (
  partner_admin_id       bigint NOT NULL,
  permission_id          bigint NOT NULL)
;

ALTER TABLE question_question ADD CONSTRAINT pk_question_question PRIMARY KEY (parent_id, child_id);
ALTER TABLE question_task ADD CONSTRAINT pk_question_task PRIMARY KEY (question_id, task_id);
ALTER TABLE question_service ADD CONSTRAINT pk_question_service PRIMARY KEY (question_id, service_id);
ALTER TABLE user_service ADD CONSTRAINT pk_user_service PRIMARY KEY (user_id, service_id);

ALTER TABLE question_question ADD CONSTRAINT fk_question_question_01 FOREIGN KEY (parent_id) references question(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_question_question_01 on question_question(parent_id);
ALTER TABLE question_question ADD CONSTRAINT fk_question_question_02 FOREIGN KEY (child_id) references question(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_question_question_02 on question_question(child_id);

ALTER TABLE question_task ADD CONSTRAINT fk_question_task_01 FOREIGN KEY (question_id) references question(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_question_task_01 on question_task(question_id);
ALTER TABLE question_task ADD CONSTRAINT fk_question_task_02 FOREIGN KEY (task_id) references task(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_question_task_02 on question_task(task_id);

ALTER TABLE Task ADD CONSTRAINT fk_task_01 FOREIGN KEY (service_id) references service(id) ON DELETE restrict ON UPDATE restrict;
CREATE INDEX ix_task_01 on Task(service_id);


ALTER TABLE question_service ADD CONSTRAINT fk_question_service_01 FOREIGN KEY (question_id) references question(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_question_service_01 on question_service(question_id);
ALTER TABLE question_service ADD CONSTRAINT fk_question_service_02 FOREIGN KEY (service_id) references service(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_question_service_02 on question_service(service_id);

ALTER TABLE service ADD CONSTRAINT fk_service_01 FOREIGN KEY (partner_id) references partner(id) ON DELETE restrict ON UPDATE restrict;
CREATE INDEX ix_service_01 on service(partner_id);

ALTER TABLE user_service ADD CONSTRAINT fk_user_service_01 FOREIGN KEY (user_id) references user(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_user_service_01 on user_service(user_id);
ALTER TABLE user_service ADD CONSTRAINT fk_user_service_02 FOREIGN KEY (service_id) references service(id) ON DELETE cascade ON UPDATE restrict;
CREATE INDEX ix_user_service_02 on user_service(service_id);

ALTER TABLE partner ADD CONSTRAINT fk_partner_01 FOREIGN KEY (user_id) references user(id) ON DELETE restrict ON UPDATE restrict;
CREATE INDEX ix_partner_01 on partner(user_id);

ALTER TABLE service_admin ADD CONSTRAINT fk_service_admin_01 FOREIGN KEY (service_id) references service(id) ON DELETE restrict ON UPDATE restrict;
CREATE INDEX ix_service_admin_01 on user_service(service_id);
ALTER TABLE service_admin ADD CONSTRAINT fk_service_admin_02 FOREIGN KEY (user_id) references user(id) ON DELETE restrict ON UPDATE restrict;
CREATE INDEX ix_service_admin_02 on service_admin(user_id);
ALTER TABLE service_admin ADD CONSTRAINT fk_service_admin_03 FOREIGN KEY (role_id) references security_role(id) ON DELETE restrict ON UPDATE restrict;
CREATE INDEX ix_service_admin_03 on service_admin(role_id);

ALTER TABLE partner_admin_permission ADD CONSTRAINT fk_partner_admin_permission_01 FOREIGN KEY (partner_admin_id) references service_admin(id) ON DELETE restrict ON UPDATE restrict;
CREATE INDEX ix_partner_admin_permission_01 on partner_admin_permission(partner_admin_id);
ALTER TABLE partner_admin_permission ADD CONSTRAINT fk_partner_admin_permission_02 FOREIGN KEY (permission_id) references user_permission(id) ON DELETE restrict ON UPDATE restrict;
CREATE INDEX ix_partner_admin_permission_02 on partner_admin_permission(permission_id);

# --- !Downs

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS question_question;

DROP TABLE IF EXISTS question_task;

DROP TABLE IF EXISTS task;

DROP TABLE IF EXISTS question_service;

DROP TABLE IF EXISTS service;

DROP TABLE IF EXISTS user_service;

DROP TABLE IF EXISTS partner;

DROP TABLE IF EXISTS service_admin;

DROP TABLE IF EXISTS partner_admin_permission;

SET FOREIGN_KEY_CHECKS = 1;
