# --- add author_id, posted_at and last_edited_at to topic

# --- !Ups

ALTER TABLE topic ADD COLUMN author_id BIGINT;
ALTER TABLE topic ADD CONSTRAINT fk_topic_author FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE SET NULL ON UPDATE RESTRICT;
ALTER TABLE topic ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';
ALTER TABLE topic ADD COLUMN last_edited_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE topic DROP FOREIGN KEY fk_topic_author;
ALTER TABLE topic DROP COLUMN author_id;
ALTER TABLE topic DROP COLUMN posted_at;
ALTER TABLE topic DROP COLUMN last_edited_at;