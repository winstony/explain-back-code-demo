# --- create table question_multilangs

# --- !Ups

CREATE TABLE question_multilangs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  question_id BIGINT NOT NULL,
  language VARCHAR(2) NOT NULL DEFAULT 'en',
  explanation VARCHAR(255) NULL,
  text VARCHAR(3000) NULL,
  crated_at TIMESTAMP NULL,
  last_edites_at TIMESTAMP NULL,
  author_id BIGINT(20) NULL DEFAULT NULL,
  posted_at TIMESTAMP NULL,
  creator_id BIGINT(20) NULL DEFAULT NULL,
  deleted TINYINT(1) NULL DEFAULT 0,
  status VARCHAR(20) NOT NULL DEFAULT 'PENDING',
  ready BIT(1) NOT NULL,
  constraint pk_question_multilangs PRIMARY KEY (id));

ALTER TABLE question_multilangs add constraint fk_question_multilangs_question1 foreign key (question_id) references question(id) on delete cascade on update cascade;
create index ix_question_multilangs_question1 on question_multilangs(question_id);

ALTER TABLE question_multilangs add constraint fk_question_multilangs_author1 foreign key (author_id) references user(id) on delete set null on update set null;
create index ix_question_multilangs_author1 on question_multilangs(author_id);

ALTER TABLE question_multilangs add constraint fk_question_multilangs_creator1 foreign key (creator_id) references partner(id) on delete set null on update set null;
create index ix_question_multilangs_creator1 on question_multilangs(creator_id);
  

# --- !Downs

DROP TABLE IF EXISTS question_multilangs;
