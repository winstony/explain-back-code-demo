# --- add column explanation to choice

# --- !Ups

ALTER TABLE choice ADD COLUMN explanation VARCHAR(2000);

# --- !Downs

ALTER TABLE choice DROP COLUMN explanation;
