# --- add posted_at and last_edited_at to tag

# --- !Ups

ALTER TABLE tag ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';
ALTER TABLE tag ADD COLUMN last_edited_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE tag DROP COLUMN posted_at;
ALTER TABLE tag DROP COLUMN last_edited_at;
