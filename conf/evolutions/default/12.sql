# --- !Ups

ALTER TABLE chapter ADD easier_id BIGINT(19);

# --- !Downs

ALTER TABLE chapter DROP easier_id;