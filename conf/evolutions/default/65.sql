# --- add 'refunded', 'recurrence_rate', 'duration', 'parent_click_id', 'invoice_status' into click; create table 'payment_log'; drop 'paid' from click

# --- !Ups

ALTER TABLE click ADD refunded BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE click ADD recurrence_rate VARCHAR(255);
ALTER TABLE click ADD duration VARCHAR(255);
ALTER TABLE click ADD parent_click_id bigint(20);
ALTER TABLE click ADD INDEX FKclick370575 (parent_click_id), ADD CONSTRAINT FKclick370575 FOREIGN KEY (parent_click_id) REFERENCES click (id);


ALTER TABLE click DROP COLUMN paid;
ALTER TABLE click ADD invoice_status VARCHAR(255);

CREATE TABLE payment_log (
  id              bigint(20) NOT NULL AUTO_INCREMENT,
  click_id        bigint(20) NOT NULL,
  transaction_id  bigint(20) NOT NULL,
  order_number_id bigint(20) NOT NULL,
  response        varchar(10000),
  PRIMARY KEY (id));

ALTER TABLE payment_log ADD INDEX FKpayment_lo853050 (click_id), ADD CONSTRAINT FKpayment_lo853050 FOREIGN KEY (click_id) REFERENCES click (id);

# --- !Downs

ALTER TABLE click DROP COLUMN refunded;
ALTER TABLE click DROP COLUMN recurrence_rate;
ALTER TABLE click DROP COLUMN duration;
ALTER TABLE click DROP FOREIGN KEY FKclick370575;
ALTER TABLE click DROP COLUMN parent_click_id;

ALTER TABLE click ADD paid tinyint(1) DEFAULT 0 NOT NULL;
ALTER TABLE click DROP COLUMN invoice_status;

ALTER TABLE payment_log DROP FOREIGN KEY FKpayment_lo853050;
DROP TABLE IF EXISTS payment_log;


