# --- add securesocial columns to partner table

# --- !Ups

ALTER TABLE partner ADD COLUMN `provider_id` VARCHAR(255) NOT NULL;
ALTER TABLE partner ADD COLUMN `provider_user_id` VARCHAR(255) NOT NULL;
ALTER TABLE partner ADD COLUMN `auth_method` VARCHAR(255) NOT NULL;

ALTER TABLE partner ADD COLUMN `first_name` VARCHAR(255);
ALTER TABLE partner ADD COLUMN `last_name` VARCHAR(255);
ALTER TABLE partner ADD COLUMN `full_name` VARCHAR(255);
ALTER TABLE partner ADD COLUMN `email` VARCHAR(255);
ALTER TABLE partner ADD COLUMN `avatar_url` VARCHAR(255);
ALTER TABLE partner ADD COLUMN `password` VARCHAR(255);
ALTER TABLE partner ADD COLUMN `password_hasher` VARCHAR(255);
ALTER TABLE partner ADD COLUMN `password_salt` VARCHAR(255);

# --- !Downs

ALTER TABLE partner DROP COLUMN `provider_id`;
ALTER TABLE partner DROP COLUMN `provider_user_id`;
ALTER TABLE partner DROP COLUMN `auth_method`;

ALTER TABLE partner DROP COLUMN `first_name`;
ALTER TABLE partner DROP COLUMN `last_name`;
ALTER TABLE partner DROP COLUMN `full_name`;
ALTER TABLE partner DROP COLUMN `email`;
ALTER TABLE partner DROP COLUMN `avatar_url`;
ALTER TABLE partner DROP COLUMN `password`;
ALTER TABLE partner DROP COLUMN `password_hasher`;
ALTER TABLE partner DROP COLUMN `password_salt`;
