# add chapter_id to questions

# --- !Ups

ALTER TABLE QUESTION ADD chapter_id bigint(19) default null;

# --- !Downs

ALTER TABLE QUESTION DROP chapter_id;