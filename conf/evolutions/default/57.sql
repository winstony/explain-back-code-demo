# --- create table choice_multilangs

# --- !Ups

CREATE TABLE choice_multilangs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  choice_id BIGINT(20) NOT NULL,
  text VARCHAR(255) NOT NULL,
  explanation VARCHAR(2000) DEFAULT NULL,
  language VARCHAR(2) NOT NULL DEFAULT 'en',
constraint pk_choice_multilangs PRIMARY KEY (id));

ALTER TABLE choice_multilangs add constraint fk_choice_multilangs_choice1 foreign key (choice_id) references choice(id) on delete cascade on update cascade;
create index ix_choice_multilangs_choice1 on choice_multilangs(choice_id);
  

# --- !Downs

DROP TABLE IF EXISTS choice_multilangs;
