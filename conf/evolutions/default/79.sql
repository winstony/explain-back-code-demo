# --- create table testrule_multilangs, add column 'language' to 'testrule'

# --- !Ups

CREATE TABLE testrule_multilangs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  testrule_id BIGINT NOT NULL,
  language VARCHAR(2) NOT NULL DEFAULT 'en',
  rule TEXT NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_edited_at TIMESTAMP,
  author_id BIGINT,
  creator_id BIGINT,
  deleted BOOLEAN NOT NULL DEFAULT 0,
  status VARCHAR(20) NOT NULL DEFAULT 'PENDING',
  ready BOOLEAN NOT NULL DEFAULT 0,
  constraint pk_testrule_multilangs PRIMARY KEY (id));

ALTER TABLE testrule_multilangs add constraint fk_testrule_multilangs_testrule1 foreign key (testrule_id) references testrule(id) on delete cascade on update cascade;
create index ix_testrule_multilangs_testrule1 on testrule_multilangs(testrule_id);

ALTER TABLE testrule_multilangs add constraint fk_testrule_multilangs_author1 foreign key (author_id) references user(id) on delete set null on update set null;
create index ix_testrule_multilangs_author1 on testrule_multilangs(author_id);

ALTER TABLE testrule_multilangs add constraint fk_testrule_multilangs_creator1 foreign key (creator_id) references partner(id) on delete set null on update set null;
create index ix_testrule_multilangs_creator1 on testrule_multilangs(creator_id);


ALTER TABLE testrule ADD COLUMN language VARCHAR(2) NOT NULL DEFAULT 'en';

# --- !Downs

DROP TABLE IF EXISTS testrule_multilangs;

ALTER TABLE testrule DROP COLUMN language;
