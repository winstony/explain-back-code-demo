# --- add "is free" to Question

# --- !Ups

ALTER TABLE question ADD is_free BOOLEAN NOT NULL DEFAULT TRUE;

# --- !Downs

ALTER TABLE question DROP COLUMN is_free;
