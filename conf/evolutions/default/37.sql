# --- add author_id to tag

# --- !Ups

ALTER TABLE tag ADD COLUMN author_id BIGINT;
ALTER TABLE tag ADD CONSTRAINT fk_tag_author FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE SET NULL ON UPDATE RESTRICT;

# --- !Downs

ALTER TABLE tag DROP FOREIGN KEY fk_tag_author;
ALTER TABLE tag DROP COLUMN author_id;
