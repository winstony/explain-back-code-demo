# add timestamp edited to question

# --- !Ups

ALTER TABLE Question CHANGE postedAt last_edited_at timestamp NULL;

# --- !Downs

ALTER TABLE Question CHANGE last_edited_at postedAt timestamp;