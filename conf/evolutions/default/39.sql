# --- add "deleted" to tables: tag, question, task, chapter, topic, service, partner, school, testrule, user

# --- !Ups

ALTER TABLE tag ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE question ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE task ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE chapter ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE topic ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE service ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE partner ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE school ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE testrule ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;
ALTER TABLE user ADD COLUMN deleted BOOLEAN NOT NULL DEFAULT false;

# --- !Downs

ALTER TABLE tag DROP COLUMN deleted;
ALTER TABLE question DROP COLUMN deleted;
ALTER TABLE task DROP COLUMN deleted;
ALTER TABLE chapter DROP COLUMN deleted;
ALTER TABLE topic DROP COLUMN deleted;
ALTER TABLE service DROP COLUMN deleted;
ALTER TABLE partner DROP COLUMN deleted;
ALTER TABLE school DROP COLUMN deleted;
ALTER TABLE testrule DROP COLUMN deleted;
ALTER TABLE user DROP COLUMN deleted;
