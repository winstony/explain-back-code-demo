# --- add school_id to social user adapter

# --- !Ups

ALTER TABLE socialuseradapter ADD COLUMN school_id BIGINT;
ALTER TABLE socialuseradapter ADD FOREIGN KEY fk_social_user_school_id (school_id) references school (id) on delete restrict on update restrict;

ALTER TABLE user ADD COLUMN school_id BIGINT;
ALTER TABLE user ADD FOREIGN KEY fk_user_school_id (school_id) references school (id) on delete restrict on update restrict;

# --- !Downs

ALTER TABLE user DROP FOREIGN KEY fk_user_school_id;
ALTER TABLE user DROP COLUMN school_id;

ALTER TABLE socialuseradapter DROP FOREIGN KEY fk_social_user_school_id;
ALTER TABLE socialuseradapter DROP COLUMN school_id;
