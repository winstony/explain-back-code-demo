# --- add column skypename to user table

# --- !Ups

ALTER TABLE user ADD COLUMN skypename VARCHAR(128);

# --- !Downs

ALTER TABLE user DROP COLUMN skypename;