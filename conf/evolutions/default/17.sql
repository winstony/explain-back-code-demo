# --- Make choice_text case sensitive, only affects mySql

# --- !Ups

ALTER TABLE choice_text CHANGE text text VARCHAR(255) BINARY NOT NULL;

# --- !Downs

ALTER TABLE choice_text CHANGE text text VARCHAR(255) NOT NULL;
