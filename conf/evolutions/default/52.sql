# --- Add column "topic_id" to "question" table

# --- !Ups

ALTER TABLE question ADD COLUMN topic_id BIGINT;
ALTER TABLE question ADD CONSTRAINT fk_question_topic FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE RESTRICT ON UPDATE RESTRICT;

# --- !Downs

ALTER TABLE question DROP FOREIGN KEY fk_question_topic;
ALTER TABLE question DROP COLUMN topic_id;
