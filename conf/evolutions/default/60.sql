# --- changes to import quizful and support UTF-8

# --- !Ups

ALTER TABLE choice DROP FOREIGN KEY fk_choice_question;
ALTER TABLE choice DROP INDEX fk_choice_question;
ALTER TABLE choice DROP FOREIGN KEY fk_choice_choice_text;
ALTER TABLE choice DROP INDEX u_choice_cti_qi;
ALTER TABLE choice ADD CONSTRAINT `fk_choice_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE choice ADD CONSTRAINT `fk_choice_choice_text` FOREIGN KEY (`choice_text_id`) REFERENCES `choice_text` (`id`) ON UPDATE CASCADE;

ALTER TABLE question MODIFY explanation VARCHAR(3000);
ALTER TABLE question_multilangs MODIFY explanation VARCHAR(3000);

ALTER TABLE choice MODIFY `text` VARCHAR(512);
ALTER TABLE choice_multilangs MODIFY `text` VARCHAR(512) NOT NULL;

ALTER TABLE `choice_multilangs` CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE `question_multilangs` CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;


# --- !Downs

ALTER TABLE choice ADD UNIQUE `u_choice_cti_qi` (`choice_text_id`,`question_id`);