# --- add service_id to question

# --- !Ups

ALTER TABLE question ADD COLUMN service_id BIGINT;

ALTER TABLE question ADD CONSTRAINT fk_question_service FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE RESTRICT ON UPDATE CASCADE;

# --- !Downs

ALTER TABLE question DROP FOREIGN KEY fk_question_service;

ALTER TABLE question DROP COLUMN service_id;
