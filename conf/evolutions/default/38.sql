# --- add subscribe to user

# --- !Ups

ALTER TABLE user ADD COLUMN subscribed BOOLEAN NOT NULL DEFAULT 0;

# --- !Downs

ALTER TABLE user DROP COLUMN subscribed;
