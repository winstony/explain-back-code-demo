# --- rename tag to tags

# --- !Ups

ALTER TABLE tag RENAME TO tags;

# --- !Downs

ALTER TABLE tags RENAME TO tag;