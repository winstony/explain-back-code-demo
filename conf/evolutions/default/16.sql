# --- create level field for chapters

# --- !Ups

ALTER TABLE chapter ADD COLUMN level INTEGER;

# --- !Downs

ALTER TABLE chapter DROP COLUMN level;
