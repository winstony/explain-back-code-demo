# --- alter table user_settings:

# --- !Ups
ALTER TABLE user_settings DROP FOREIGN KEY fk_user_settings;
ALTER TABLE user_settings DROP PRIMARY KEY;

# --- !Downs
ALTER TABLE user_settings ADD PRIMARY KEY (user_id);
ALTER TABLE user_settings ADD CONSTRAINT fk_user_settings FOREIGN KEY (user_id) REFERENCES user(id);
