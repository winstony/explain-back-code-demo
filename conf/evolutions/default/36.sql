# --- add author_id, posted_at and last_edited_at to task

# --- !Ups

ALTER TABLE task ADD COLUMN author_id BIGINT;
ALTER TABLE task ADD CONSTRAINT fk_task_author FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE SET NULL ON UPDATE RESTRICT;
ALTER TABLE task ADD COLUMN posted_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';
ALTER TABLE task ADD COLUMN last_edited_at TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01';


# --- !Downs

ALTER TABLE task DROP FOREIGN KEY fk_task_author;
ALTER TABLE task DROP COLUMN author_id;
ALTER TABLE task DROP COLUMN posted_at;
ALTER TABLE task DROP COLUMN last_edited_at;