# --- create table tags_multilangs, add column 'language', to 'tags'

# --- !Ups

CREATE TABLE tags_multilangs (
  id BIGINT AUTO_INCREMENT NOT NULL,
  tags_id BIGINT NOT NULL,
  language VARCHAR(2) NOT NULL DEFAULT 'en',
  name VARCHAR(255) NOT NULL,
  posted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_edited_at TIMESTAMP,
  author_id BIGINT,
  creator_id BIGINT,
  deleted BOOLEAN NOT NULL DEFAULT 0,
  status VARCHAR(20) NOT NULL DEFAULT 'PENDING',
  activated BOOLEAN NOT NULL DEFAULT 0,
  constraint pk_tags_multilangs PRIMARY KEY (id));

ALTER TABLE tags_multilangs add constraint fk_tags_multilangs_tags1 foreign key (tags_id) references tags(id) on delete cascade on update cascade;
create index ix_tags_multilangs_tags1 on tags_multilangs(tags_id);

ALTER TABLE tags_multilangs add constraint fk_tags_multilangs_author1 foreign key (author_id) references user(id) on delete set null on update set null;
create index ix_tags_multilangs_author1 on tags_multilangs(author_id);

ALTER TABLE tags_multilangs add constraint fk_tags_multilangs_creator1 foreign key (creator_id) references partner(id) on delete set null on update set null;
create index ix_tags_multilangs_creator1 on tags_multilangs(creator_id);


ALTER TABLE tags ADD COLUMN language VARCHAR(2) NOT NULL DEFAULT 'en';

# --- !Downs

DROP TABLE IF EXISTS tags_multilangs;

ALTER TABLE tags DROP COLUMN language;
