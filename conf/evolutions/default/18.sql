# --- create result message field for task

# --- !Ups

ALTER TABLE task ADD COLUMN result_msg VARCHAR(255) DEFAULT NULL;

# --- !Downs

ALTER TABLE task DROP COLUMN result_msg;