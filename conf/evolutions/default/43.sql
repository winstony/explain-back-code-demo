# --- create user_settings table

# --- !Ups

CREATE TABLE user_settings (
  user_id   bigint NOT NULL,
  settings  varchar(15000),
  CONSTRAINT pk_user_settings PRIMARY KEY (user_id))
;
ALTER TABLE user_settings ADD CONSTRAINT fk_user_settings FOREIGN KEY (user_id) references user(id) ON DELETE cascade ON UPDATE restrict;

# --- !Downs

DROP TABLE if EXISTS user_settings;