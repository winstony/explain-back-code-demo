package controllers.api;

import java.util.List;
import java.util.Optional;

import controllers.api.exceptions.ApiBadRequestException;
import models.partners.Course;
import models.partners.School;
import models.secure.SchoolUserTo;
import play.libs.Json;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;
import scala.concurrent.Future;
import securesocial.core.java.SecuredAction;
import securesocial.core.java.UserAwareAction;
import service.CommentsService;
import to.api.Comment;
import utils.ControllerUtils;
import utils.ServiceUtils;
import dao.CourseDao;
import dao.QuestionDao;
import dao.couchbase.CouchbaseCommentDao;
import static utils.ControllerUtils.*;

public class CommentsController extends Controller {

    private static final String JSON = "application/json";

    @SecuredAction
    public static Promise<Result> postCommentV1() {
        Comment comment = Json.fromJson(request().body().asJson(), Comment.class);
        if (!comment.validate().isEmpty()) {
            return Promise.pure(apiBadRequest(comment.validate()));
        }
        String qUuid = Optional.ofNullable(QuestionDao.findById(comment.questionId)).map(q -> q.uuid).orElse(null);
        return postComment(qUuid, comment);
    }

    @SecuredAction
    public static Promise<Result> postComment(String questionUuid) {
        Comment comment = Json.fromJson(request().body().asJson(), Comment.class);
        if (!comment.validate().isEmpty()) {
            return Promise.pure(apiBadRequest(comment.validate()));
        }
        return postComment(questionUuid, comment);
    }

    private static Promise<Result> postComment(String qUuid, Comment comment) {
        SchoolUserTo user = ControllerUtils.getCurrentUser();
        return new CommentsService().addQuestionComment(user, comment, qUuid).map(c -> ok(Json.toJson(c)).as(JSON));
    }

    @Deprecated // "Use getQuestionComments(String uuid)"
    @SecuredAction
    public static Promise<Result> getQuestionCommentsV1(long questionId) {
        CouchbaseCommentDao commentDao = new CouchbaseCommentDao();
        Future<List<Comment>> comments = commentDao.getComments(questionId, 25, null);
        Promise<Result> res = Promise.wrap(comments).map(c -> ok(Json.toJson(c)).as(JSON));
        return res.fallbackTo(Promise.pure(apiInternalServerError("Could not get comments")));
    }

    @UserAwareAction
    public static Promise<Result> getQuestionComments(String qUuid, Integer limit, String startKey) {
        if (limit < 1 || limit > 100) {
            throw new ApiBadRequestException("Limit must be in range from 1 to 100");
        }
        Optional<Promise<Result>> maybe = new CommentsService().getComments(qUuid, limit, startKey)
                .map(promise -> promise.map(comments -> ok(Json.toJson(comments)).as(JSON)));
        return maybe.orElse(Promise.promise(() -> apiNotFound("Question with uuid " + qUuid + " not found")))
                .fallbackTo(Promise.pure(apiInternalServerError("Could not get comments")));
    }

    @SecuredAction
    public static Promise<Result> getUserComments(Integer limit, String startKey) {
        if (limit < 1 || limit > 100) {
            throw new ApiBadRequestException("Limit must be in range from 1 to 100");
        }
        SchoolUserTo user = ControllerUtils.getCurrentUser();
        Promise<List<Comment>> comments = new CommentsService().getUserComments(user.id, limit, startKey);
        Promise<Result> res = comments.map(cl -> ok(Json.toJson(cl)).as(JSON));
        return res.fallbackTo(Promise.pure(apiInternalServerError("Could not get comments")));
    }

    @SecuredAction
    public static Promise<Result> updateQuestionComment(String questionUuid, String commentUuid) {
        Comment comment = Json.fromJson(request().body().asJson(), Comment.class);
        if (!comment.validate().isEmpty()) {
            return Promise.pure(apiBadRequest(comment.validate()));
        }
        Long userId = ControllerUtils.getCurrentUser().id;
        return new CommentsService().updateQuestionComment(userId, commentUuid, comment).map(maybeUpdated -> {
            Optional<Result> map = maybeUpdated.map(c -> ok(Json.toJson(c)).as(JSON));
            return map.orElse(apiNotFound("Comment not found or no access"));
        });
    }

    @SecuredAction
    public static Promise<Result> deleteQuestionComment(String questionUuid, String commentUuid) {
        Long userId = ControllerUtils.getCurrentUser().id;
        return new CommentsService().deleteComment(userId, commentUuid).map(deleted -> {
            Optional<Result> result = deleted.map(c -> ok(Json.toJson(c)).as(JSON));
            return result.orElse(apiNotFound("Comment not found or no access"));
        });
    }

    @UserAwareAction
    public static Promise<Result> latestComments(String courseAlias, Integer limit, String startKey) {
        if (limit < 1 || limit > 100) {
            throw new ApiBadRequestException("Limit must be in range from 1 to 100");
        }
        School school = ServiceUtils.getRequestSchool();
        if (courseAlias == null) {
            return latestSchoolComments(school, limit, startKey);
        }
        return latestCourseComments(school, courseAlias, limit, startKey);
    }

    private static Promise<Result> latestSchoolComments(School school, int limit, String startKey) {
        return new CommentsService().getSchoolComments(school.id, limit, startKey)
                .map(comments -> ok(Json.toJson(comments)).as(JSON));
    }

    private static Promise<Result> latestCourseComments(School school, String courseAlias, int limit, String startKey) {
        Course course = CourseDao.findByAlias(school.id, courseAlias);
        if (course == null) {
            throw new ApiBadRequestException("Course " + courseAlias + " not found");
        }
        return new CommentsService().getCourseComments(school.id, course.id, limit, startKey)
                .map(comments -> ok(Json.toJson(comments)).as(JSON));
    }
}
