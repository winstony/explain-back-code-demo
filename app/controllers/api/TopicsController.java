package controllers.api;


import dao.TopicDao;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.UserAwareAction;
import to.api.Topic;
import to.api.convert.ApiConverter;
import utils.ServiceUtils;

import java.util.List;
import java.util.stream.Collectors;

public class TopicsController extends Controller {
    private static final Logger.ALogger log = Logger.of(TopicsController.class);

    @UserAwareAction
    public static Result getTopics() {
        Long schoolId = ServiceUtils.getRequestSchool().id;
        List<Topic> tos = TopicDao.getSchoolTopics(schoolId).stream()
                .map(ApiConverter::to).collect(Collectors.toList());
        return ok(Json.toJson(tos));
    }
}
