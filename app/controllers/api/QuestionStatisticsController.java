package controllers.api;

import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.QuestionStatisticsService;

public class QuestionStatisticsController extends Controller {

    private static QuestionStatisticsService questionStatistics = new QuestionStatisticsService();

    public static Promise<Result> get(String questionUuid) {
        return questionStatistics.get(questionUuid).map(c -> ok(Json.toJson(c)));
    }
}
