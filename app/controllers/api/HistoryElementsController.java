package controllers.api;

import static utils.ControllerUtils.apiBadRequest;
import static utils.ControllerUtils.apiInternalServerError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import models.core.User;
import models.secure.SchoolUserTo;
import dao.couchbase.CouchbaseHistoryElementDao;
import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import play.libs.F.Promise;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import scala.concurrent.Future;
import securesocial.core.java.SecuredAction;
import service.HistoryElementsService;
import service.ChapterStatsFactory;
import to.api.HistoryElement;
import to.api.Stats;
import utils.ControllerUtils;

public class HistoryElementsController extends Controller {
    private static final ALogger log = Logger.of(HistoryElementsController.class);
    private static HistoryElementsService historyElementsService = new HistoryElementsService();

    @SecuredAction
    @BodyParser.Of(BodyParser.Json.class)
    public static Promise<Result> saveHistoryElement() {
        HistoryElement record = Json.fromJson(request().body().asJson(), HistoryElement.class);
        if (!record.validate().isEmpty()) {
            apiBadRequest(record.validate());
        }
        SchoolUserTo user = ControllerUtils.getCurrentUser();
        return historyElementsService.saveHistoryElement(user, record).map(r -> ok(Json.toJson(r)));
    }

    @SecuredAction
    public static Promise<Result> getHistoryElements(long courseId, Long chapterId) {
        CouchbaseHistoryElementDao historyDao = new CouchbaseHistoryElementDao();
        Long userId = ControllerUtils.getCurrentUser().id;
        Future<List<HistoryElement>> history = null;
        if (chapterId == null) {
            history = historyDao.getCourseAnswers(userId, courseId);
        } else {
            history = historyDao.getChapterAnswers(userId, courseId, chapterId);
        }
        return Promise.wrap(history).map(answers -> ok(Json.toJson(answers)).as("application/json"));
    }

    @SecuredAction
    @BodyParser.Of(BodyParser.Json.class)
    public static Promise<Result> saveHistoryElementsList() {
        HistoryElement[] records = Json.fromJson(request().body().asJson(), HistoryElement[].class);
        for (HistoryElement record : records) {
            if (!record.validate().isEmpty()) {
                apiBadRequest(record.validate());
            }
            if (record.couchbaseId == null) {
                apiBadRequest(Collections.singletonList("Mobile historyElements must already have 'id'"));
            }
            if (record.answeredAt == null) {
                apiBadRequest(Collections.singletonList("Mobile historyElements must already have 'answeredAt'"));
            }
        }
        SchoolUserTo user = ControllerUtils.getCurrentUser();
        Promise<List<HistoryElement>> sequence = historyElementsService.saveHistoryElementsList(user, records);
        return sequence.map(list -> ok(Json.toJson(list)));
    }
}
