package controllers.api;

import static utils.ControllerUtils.apiNotFound;

import java.util.List;
import java.util.Optional;

import models.secure.SchoolUserTo;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecuredAction;
import service.AchievementsService;
import to.api.achieve.Achievement;
import utils.ControllerUtils;

public class AchievementsController extends Controller {
    private static AchievementsService service = new AchievementsService();

    public static Promise<Result> get(String id) {
        Promise<Optional<Achievement>> promise = service.get(id);
        return promise.map(maybe -> {
            return maybe.map(a -> (Result) ok(Json.toJson(a)))
                    .orElseGet(() -> apiNotFound("No achievement with id" + id));
        });
    }

    @SecuredAction
    public static Promise<Result> getAchievements() {
        SchoolUserTo user = ControllerUtils.getCurrentUser();
        Promise<List<Achievement>> promise = service.getList(user.id);
        return promise.map(list -> ok(Json.toJson(list)));
    }

    @SecuredAction
    public static Promise<Result> addImageUrl(String id, String url) {
        return service.addImageUrl(url, id).map(a -> ok(Json.toJson(a)));
    }
}
