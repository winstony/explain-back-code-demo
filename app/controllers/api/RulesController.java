package controllers.api;

import dao.TestRuleDao;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.UserAwareAction;
import to.api.TestRule;
import to.api.convert.ApiConverter;
import utils.ServiceUtils;

import java.util.List;
import java.util.stream.Collectors;

public class RulesController extends Controller {
    private static final Logger.ALogger log = Logger.of(RulesController.class);

    @UserAwareAction
    public static Result getRules(String topicAlias, Integer size, Integer page) {
        Long schoolId = ServiceUtils.getRequestSchool().id;
        List<TestRule> tos = TestRuleDao.getSchoolRules(topicAlias, schoolId, size, page).stream()
                .map(rule -> ApiConverter.to(rule, ServiceUtils.getLang(), true)).collect(Collectors.toList());
        return ok(Json.toJson(tos));
    }
}
