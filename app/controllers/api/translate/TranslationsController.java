package controllers.api.translate;

import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecuredAction;
import service.ProfileService;
import service.translate.TranslationsService;
import to.api.Profile;
import to.api.Topic;
import to.api.translate.QuestionForTranslation;
import utils.ControllerUtils;
import utils.ServiceUtils;

import java.util.List;

public class TranslationsController extends Controller {

    @SecuredAction
    public static Result becomeTranslator() {
        Profile p = new ProfileService().makeTranslator(ControllerUtils.getCurrentUserDb());
        return ok(Json.toJson(p));
    }

    @SecuredAction
    public static Result my() {
        List<QuestionForTranslation> list = new TranslationsService().my(ControllerUtils.getCurrentUser().id);
        return ok(Json.toJson(list));
    }

    @SecuredAction
    public static Result getTranslation(String uuid) {
        QuestionForTranslation qft = new TranslationsService().get(uuid, ControllerUtils.getCurrentUser().id);
        if (qft == null) {
            return ControllerUtils.apiNotFound("No question accessible with id " + uuid);
        }
        return ok(Json.toJson(qft));
    }

    @SecuredAction
    @BodyParser.Of(BodyParser.Json.class)
    public static Result saveTranslation(String uuid) {
        QuestionForTranslation record = Json.fromJson(request().body().asJson(), QuestionForTranslation.class);
        QuestionForTranslation qft = new TranslationsService().save(record, uuid, ControllerUtils.getCurrentUser().id);
        return ok(Json.toJson(qft));
    }

    @SecuredAction
    public static Result openNewTranslation(String topic) {
        QuestionForTranslation qft = new TranslationsService().open(ControllerUtils.getCurrentUser().id, topic);
        if (qft == null) {
            return ControllerUtils.apiNoContent();
        }
        return ok(Json.toJson(qft));
    }

    @SecuredAction
    public static Result cancelTranslation(String uuid) {
        new TranslationsService().cancel(uuid, ControllerUtils.getCurrentUser().id);
        return ok(Json.parse("{}"));
    }

    @SecuredAction
    public static Result getTranslatableTopics() {
        Long schoolId = ServiceUtils.getRequestSchool().id;
        List<Topic> tos = TranslationsService.getSchoolTranslatableTopics(schoolId);
        if (tos == null || tos.isEmpty()) {
            return ControllerUtils.apiNoContent();
        }
        return ok(Json.toJson(tos));
    }

    @SecuredAction
    @BodyParser.Of(BodyParser.Json.class)
    public static Result getTopicsByAliases() {
        String[] topicsAliases = Json.fromJson(request().body().asJson(), String[].class);
        List<Topic> topics = TranslationsService.getTopicsByAliases(topicsAliases);
        return ok(Json.toJson(topics));
    }
}
