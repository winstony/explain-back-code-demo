package controllers.api;

import java.util.Optional;

import models.core.User;
import models.secure.SchoolUserTo;
import play.Logger;
import play.Logger.ALogger;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecuredAction;
import service.NotificationsService;
import utils.ControllerUtils;

public class NotificationsController extends Controller {
    private static final ALogger log = Logger.of(NotificationsController.class);
    private static final NotificationsService notificationService = new NotificationsService();

    @SecuredAction
    public static Promise<Result> getTimelineNotifications(Long before, int limit) {
        before = before == null ? Long.MAX_VALUE : before;
        SchoolUserTo user = ControllerUtils.getCurrentUser();
        return notificationService.findTimelineNotifications(user, before, limit).map(list -> {
            return ok(Json.toJson(list));
        });
    }

    /**
     * Returns latest comment notifications. If <b>before</b> is specified, then return 50 comment notifications
     * appeared earlier this timestamp.
     */
    @SecuredAction
    public static Promise<Result> getCommentNotifications(Long before, int limit) {
        before = before == null ? Long.MAX_VALUE : before;
        SchoolUserTo user = ControllerUtils.getCurrentUser();
        return notificationService.findCommentNotifications(user, before, limit).map(list -> {
            return ok(Json.toJson(list));
        });
    }

    @SecuredAction
    public static void markAllTimelineSeen() {
        // TODO
    }

    @SecuredAction
    public static void markAllTimelineRead() {
        // TODO
    }

    @SecuredAction
    public static void markAllCommentsSeen() {
        // TODO
    }

    @SecuredAction
    public static void markAllCommentsRead() {
        // TODO
    }

    @SecuredAction
    public static void markSeen(String notificationId) {
        // TODO
    }

    @SecuredAction
    public static void markRead(String notificationId) {
        // TODO
    }
}
