package controllers.api.exceptions;

import play.mvc.Result;
import utils.ControllerUtils;

public class ApiForbiddenException extends ApiException {
    public ApiForbiddenException() {
        super("Not authorized");
    }

    public ApiForbiddenException(String message) {
        super(message);
    }

    public Result httpResult() {
        return ControllerUtils.forbidden(getMessage());
    }

}
