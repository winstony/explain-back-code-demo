package controllers.api.exceptions;

import play.mvc.Result;
import utils.ControllerUtils;

public class ApiNotFoundException extends ApiException {
    public ApiNotFoundException(String message) {
        super(message);
    }

    public Result httpResult() {
        return ControllerUtils.apiNotFound(getMessage());
    }
}
