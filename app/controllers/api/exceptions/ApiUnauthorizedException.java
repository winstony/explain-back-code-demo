package controllers.api.exceptions;

import play.mvc.Result;
import utils.ControllerUtils;

public class ApiUnauthorizedException extends ApiException {

    public Result httpResult() {
        return ControllerUtils.unauthorized();
    }
}
