package controllers.api.exceptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import play.mvc.Result;
import utils.ControllerUtils;

public class ApiBadRequestException extends ApiException {
    private List<String> errors;

    public ApiBadRequestException(String error) {
        super(error);
        errors = Collections.singletonList(error);
    }

    public ApiBadRequestException(Collection<String> errors) {
        super(String.valueOf(errors));
        this.errors = new ArrayList<>(errors);
    }

    public Result httpResult() {
        return ControllerUtils.apiBadRequest(errors);
    }

}
