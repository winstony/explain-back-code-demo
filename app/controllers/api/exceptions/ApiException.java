package controllers.api.exceptions;

import play.mvc.Result;
import utils.ControllerUtils;

public class ApiException extends RuntimeException {

    public ApiException() {
    }

    public ApiException(String message) {
        super(message);
    }

    public Result httpResult() {
        return ControllerUtils.apiInternalServerError(getMessage());
    }

}
