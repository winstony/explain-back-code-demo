package controllers.api.exceptions;

import play.mvc.Result;
import utils.ControllerUtils;

public class ApiPaymentRequiredException extends ApiException {
    public Result httpResult() {
        return ControllerUtils.paymentRequired();
    }
}
