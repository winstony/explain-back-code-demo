package controllers.api;

import org.apache.commons.lang3.StringUtils;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecuredAction;
import securesocial.core.java.UserAwareAction;
import service.QuestionsService;
import to.api.Question;
import utils.ControllerUtils;

import java.util.ArrayList;
import java.util.List;

import static utils.ControllerUtils.apiBadRequest;

public class QuestionsController extends Controller {

    @SecuredAction
    public static Promise<Result> chapterQuestions(String courseAlias, String chapterAlias, Integer size) {
        List<String> errors = new ArrayList<>();
        if (size < 1 || size > 100) {
            errors.add("Size must be between 1 and 100");
        }
        if (StringUtils.isBlank(courseAlias)) {
            errors.add("courseAlias must not be blank");
        }
        if (StringUtils.isBlank(chapterAlias)) {
            errors.add("chapterAlias must not be blank");
        }
        if (!errors.isEmpty()) {
            return Promise.pure(apiBadRequest(errors));
        }
        Promise<List<to.api.Question>> questions = QuestionsService.getChapterQuestions(
                courseAlias,
                chapterAlias,
                size);
        return questions.map(q -> ok(Json.toJson(q)).as(ControllerUtils.JSON));
    }

    @UserAwareAction
    public static Result getQuestionByUuid(String uuid) {
        Question q = QuestionsService.getQuestionByUuid(uuid);
        return ok(Json.toJson(q));
    }

    public static Result openQuestionUuidForCourse(String courseAlias) {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isBlank(courseAlias)) {
            errors.add("courseAlias must not be blank");
        }
        if (!errors.isEmpty()) {
            return apiBadRequest(errors);
        }
        String qUuid = QuestionsService.getOpenQuestionUuidForCourse(courseAlias);
        return ok(qUuid);
    }
}
