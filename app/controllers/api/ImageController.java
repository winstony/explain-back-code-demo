package controllers.api;

import java.io.File;
import java.io.IOException;

import org.parse4j.ParseException;
import org.parse4j.ParseFile;

import clients.parse.ParseClient;
import clients.parse.ParseClient.Image;

import com.google.common.io.Files;

import controllers.api.exceptions.ApiBadRequestException;
import controllers.api.exceptions.ApiException;
import play.Play;
import play.libs.Json;
import play.libs.F.Promise;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;

public class ImageController extends Controller {

    public static Result get(String id) {
        Image image = new ParseClient().get(id);
        return ok(image.data).as(image.type);
    }

    public static Result saveImage() {
        return null;
    }

    public static Result upload() {
        MultipartFormData body = request().body().asMultipartFormData();
        FilePart picture = body.getFile("image");
        if (picture != null) {
            File file = picture.getFile();
            try {
                byte[] image = Files.toByteArray(file);
                if (image.length > 10 * (1 << 20)) {
                    throw new ApiBadRequestException("Image size too big, max 10 Mb");
                }
                String imageId = new ParseClient().upload(picture.getFilename(), image, picture.getContentType());
                Call call = controllers.api.routes.ImageController.get(imageId);
                String url = call.absoluteURL(/* secure */false, request().host());
                return ok(Json.newObject().set("url", Json.toJson(url)));
            } catch (IOException e) {
                throw new ApiException(e.getMessage());
            }
        }
        throw new ApiBadRequestException("image is missing");
    }
}
