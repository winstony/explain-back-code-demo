package controllers.api;

import models.core.User;
import models.secure.SchoolUserTo;
import play.Play;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecuredAction;
import securesocial.core.java.UserAwareAction;
import utils.ControllerUtils;

public class PingController extends Controller {

    @UserAwareAction
    public static Result ping() {
        if (!Play.isProd()) {
            SchoolUserTo user = ControllerUtils.getCurrentUser();
            PingUserInfo info = new PingUserInfo();
            if (user != null) {
                info.status = "authenticated";
                info.userId = user.id;
                info.email = user.email;
                info.fullName = user.fullName;
            } else {
                info.status = "not authenticated";
            }
            return ok(Json.toJson(info)).as("application/json");
        }
        return notFound();
    }

    @SecuredAction
    public static Result pingSecure() {
        if (!Play.isProd()) {
            SchoolUserTo user = ControllerUtils.getCurrentUser();
            PingUserInfo info = new PingUserInfo();
            info.status = "authenticated";
            info.userId = user.id;
            info.fullName = user.fullName;
            return ok(Json.toJson(info)).as("application/json");
        }
        return notFound();
    }

    public static class PingUserInfo {
        public String status;
        public String email;
        public Long userId;
        public String accountId;
        public String providerId;
        public String fullName;
    }
}
