package controllers.api;

import dao.ChapterDao;
import dao.CourseDao;
import models.core.Chapter;
import models.partners.Course;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecuredAction;
import service.StatsService;
import to.api.ItemStatus;
import utils.ControllerUtils;
import utils.ServiceUtils;

import java.util.List;

public class StatsController extends Controller {

    @SecuredAction
    public static Promise<Result> getChapterStats(String courseAlias, String chapterAlias) {
        Long schoolId = ServiceUtils.getSchoolId();
        Long userId = ControllerUtils.getCurrentUser().id;
        ChapterDao.FindChapterParams params = new ChapterDao.FindChapterParams();
        params.schoolId = schoolId;
        params.chapterAlias = chapterAlias;
        params.courseAlias = courseAlias;

        Chapter chapter = ChapterDao.find(params);
        Promise<ItemStatus> chapterStats = new StatsService().getChapterStats(
                userId,
                schoolId,
                chapter.courseId,
                chapter.id);
        return chapterStats.map(s -> ok(Json.toJson(s)).as("application/json"));
    }

    @SecuredAction
    public static Promise<Result> getCourseStats(String courseAlias) {
        Long schoolId = ServiceUtils.getSchoolId();
        Long userId = ControllerUtils.getCurrentUser().id;
        CourseDao.GetCourseParams params = new CourseDao.GetCourseParams(schoolId);
        params.courseAlias = courseAlias;
        Course course = CourseDao.getCourse(params);
        Promise<ItemStatus> courseStats = new StatsService().getCourseStats(userId, schoolId, course.id);
        return courseStats.map(s -> ok(Json.toJson(s)).as("application/json"));
    }

    @SecuredAction
    public static Promise<Result> getCourseStatsByChapter(String courseAlias) {
        Long schoolId = ServiceUtils.getSchoolId();
        Long userId = ControllerUtils.getCurrentUser().id;
        CourseDao.GetCourseParams params = new CourseDao.GetCourseParams(schoolId);
        params.courseAlias = courseAlias;
        Course course = CourseDao.getCourse(params);
        Promise<List<ItemStatus>> courseStats = new StatsService().getCourseStatsByChapter(userId, schoolId, course.id);
        return courseStats.map(s -> ok(Json.toJson(s)).as("application/json"));
    }

    @SecuredAction
    public static Promise<Result> getStatsByCourse() {
        Long schoolId = ServiceUtils.getSchoolId();
        Long userId = ControllerUtils.getCurrentUser().id;
        Promise<List<ItemStatus>> coursesStats = new StatsService().getStatsByCourse(userId, schoolId);
        return coursesStats.map(s -> ok(Json.toJson(s)).as("application/json"));
    }

}
