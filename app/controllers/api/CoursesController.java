package controllers.api;

import dao.CourseDao;
import models.secure.SchoolUserTo;
import play.Logger;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.UserAwareAction;
import service.StatsService;
import to.api.Chapter;
import to.api.Course;
import to.api.ItemStatus;
import to.api.convert.ApiConverter;
import utils.ControllerUtils;
import utils.ServiceUtils;

import java.util.List;
import java.util.stream.Collectors;

public class CoursesController extends Controller {
    private static final Logger.ALogger log = Logger.of(CoursesController.class);

    @UserAwareAction
    public static Result getCourses() {
        Long schoolId = ServiceUtils.getRequestSchool().id;
        List<Course> tos = CourseDao.getSchoolCourses(schoolId).stream()
                .map(co -> ApiConverter.to(co)).collect(Collectors.toList());
        return ok(Json.toJson(tos));
    }

    @UserAwareAction
    public static Promise<Result> getCourse(String courseAlias) {
        return getCoursePromise(courseAlias).map(course -> ok(Json.toJson(course)));
    }

    @UserAwareAction
    public static Promise<Result> chapters(String courseAlias) {
        return getCoursePromise(courseAlias).map(c -> ok(Json.toJson(c.chapters)));
    }

    @UserAwareAction
    public static Promise<Result> chapter(String courseAlias, String chapterAlias) {
        return getCoursePromise(courseAlias).map(course -> {
            return course.chapters.stream()
                    .filter(chapter -> chapterAlias.equals(chapter.alias))
                    .findFirst().get();
        }).map(chapter -> ok(Json.toJson(chapter)));
    }

    private static Promise<to.api.Course> getCoursePromise(String courseAlias) {
        Long schoolId = ServiceUtils.getRequestSchool().id;
        Course courseTo = ApiConverter.to(CourseDao.findByAlias(schoolId, courseAlias));
        SchoolUserTo user = ControllerUtils.getCurrentUser();
        if (user == null) {
            return Promise.pure(courseTo);
        }
        Promise<List<ItemStatus>> statsByChapterPromise =
                new StatsService().getCourseStatsByChapter(user.id, schoolId, courseTo.id);
        return statsByChapterPromise.map(statsByChapter -> {
            for (Chapter chapter : courseTo.chapters) {
                ItemStatus chapterStatus = statsByChapter.stream()
                        .filter(s -> s.chapterId == chapter.id)
                        .findFirst().orElse(new ItemStatus());
                chapter.progress = chapterStatus.getProgress();
                chapter.status = chapterStatus;
            }
            courseTo.chapters.sort((c1, c2) -> Integer.compare(c1.level, c2.level));
            if (courseTo.chapters.isEmpty()) {
                return courseTo;
            }
            courseTo.chapters.get(0).open = true;
            for (int index = 0; index < courseTo.chapters.size() - 1; index++) {
                Chapter chapter = courseTo.chapters.get(index);
                courseTo.chapters.get(index + 1).open = chapter.open && chapter.status.getNextOpen();
            }
            return courseTo;
        });
    };

}
