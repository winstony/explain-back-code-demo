package models;

import play.db.ebean.Model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@javax.persistence.MappedSuperclass
public abstract class ModelExtended extends Model {

    private static final long serialVersionUID = -5889434124182296871L;

    /** Default amount of displayed rows */
    public static Integer defaultPageSize = 10;

    protected static Finder<Long, ? extends ModelExtended> find;

    /** Primary key for any Model. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public abstract ModelExtended getById(Long id);

    @Override
    public void delete() {
        if (this instanceof Deletable) {
            ((Deletable) this).setDeleted(true);
            super.update();
        } else {
            super.delete();
        }
    }

    @Override
    public void save() {
        if (this instanceof Deletable) {
            ((Deletable) this).setDeleted(false);
        }
        super.save();
    }
}