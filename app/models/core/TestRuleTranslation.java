package models.core;

import javax.persistence.*;

@Entity
@Table(name = "testrule_multilangs")
public class TestRuleTranslation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "testrule_id")
    private Long testRuleId;

    @ManyToOne
    @JoinColumn(name = "testrule_id", insertable = false, updatable = false)
    public TestRule testRule;

    public String language;

    public String rule;

    public Long getTestRuleId() {
        return testRuleId;
    }

    public void setTestRuleId(Long testRuleId) {
        this.testRuleId = testRuleId;
    }
}
