package models.core;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Page;
import models.Deletable;
import models.ModelExtended;
import models.Status;
import models.partners.Partner;
import play.data.validation.Constraints.MaxLength;
import play.data.validation.Constraints.Required;
import utils.ControllerUtils;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="testrule")
public class TestRule extends ModelExtended implements Deletable {

    private static final int FIRST_LETTERS_TO_DISPLAY = 180;

    public boolean ready;

    @ManyToMany
    @JoinTable(name = "testrule_tag",
               joinColumns = { @JoinColumn(name = "TestRule_id") },
               inverseJoinColumns = { @JoinColumn(name = "tags_id") })
    public List<Tag> tags;

    @ManyToOne
    public Topic topic;

    @Required
    @MaxLength(1000)
    public String rule;

    @ManyToOne
    public Chapter chapter;

    public String language;

    @OneToMany(mappedBy = "testRule")
    public List<TestRuleTranslation> translations;

    @ManyToOne
    public User author;

    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    @ManyToOne
    public Partner creator;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    public boolean activated;

    @Enumerated(EnumType.STRING)
    public Status status;

    public Boolean deleted;


    /**
     * Generic query helper for entity TestRule with id Long
     */
    static Finder<Long, TestRule> find = new Finder<Long, TestRule>(Long.class, TestRule.class);

    public static int count() {
        return find.findRowCount();
    }

    /**
     * Return a page of TestRule
     *
     * @param page Page to display
     * @param pageSize Number of TestRules per page
     * @param sortBy TestRules property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<TestRule> page(int page,
                                      int pageSize,
                                      String sortBy,
                                      String order,
                                      String filter) {
        return find.where()
                   .like("rule", "%" + filter + "%")
                   .orderBy(sortBy + " " + order)
                   .findPagingList(pageSize)
                   .setFetchAhead(false)
                   .getPage(page);
    }

    /**
     * Show the rule itself it is less than 60 characters,
     * else show first 60 characters + three dots.
     */
    @Override
    public String toString() {
        return ((rule == null) ? "[Rule " + String.valueOf(id) + "]" :
             rule.substring(0, Math.min(rule.length(), FIRST_LETTERS_TO_DISPLAY))
                        + (rule.length() > FIRST_LETTERS_TO_DISPLAY ? "..." : ""));
    }

    public static Map<Long, String>  findAllFromChapter(String chapterId) {
        return listMapper(
                find.select("*")
                    .where()
                    .like("chapter", chapterId)
                    .findList()
        );
    }

    public static List<TestRule> filterByTags(List<Long> tagIds) {
        ExpressionList<TestRule> query = find.where().in("tags.id", tagIds);
        if (tagIds != null && !tagIds.isEmpty()) {
            return query.findList();
        }
        return null;
    }


    public static Map<Long, String> getMap() {
        return listMapper(TestRule.find.select("*").where().eq("deleted", false).findList());
    }

    protected static Map<Long, String>  listMapper(List<TestRule> list){
        LinkedHashMap<Long, String> options = new LinkedHashMap<Long,String>();
        for (TestRule rule: list) {
            options.put(rule.id, rule.rule);
        }
        return options;
    }

    public static TestRule findById(Long id) {
        return find.select("*").setId(id).findUnique();
    }

    @Override
    public TestRule getById(Long id) {
        return findById(id);
    }

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.author = ControllerUtils.getCurrentUserDb();
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    public void basicSave() {
        super.save();
    }

    public String getAuthorEmail() {
        return author == null ? null : author.email;
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }

    public Long getId() {
        return id;
    }

}
