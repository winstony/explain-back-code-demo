package models.core;

import com.avaje.ebean.*;
import com.avaje.ebean.Query;
import models.Deletable;
import models.ModelExtended;
import models.Status;
import models.core.aggregators.ValuableTag;
import models.partners.Partner;
import play.Logger;
import play.Logger.ALogger;
import play.data.validation.Constraints.Required;
import utils.ControllerUtils;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "tags")
public class Tag extends ModelExtended implements Comparable<Tag>, Deletable {

    private static final ALogger log = Logger.of(Tag.class);

    @Required
    @Column(name = "name")
    public String name;

    public boolean ready;

    /** A user that has created the tag. */
    @ManyToOne
    public User author;

    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    @ManyToOne
    public Partner creator;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    @ManyToMany
    @JoinTable(name = "Tag_Tag",
               joinColumns = { @JoinColumn(name = "tag_id", referencedColumnName="id") },
               inverseJoinColumns = { @JoinColumn(name = "easierTags_id", referencedColumnName="id") })
    public List<Tag> easierTags = new ArrayList<Tag>();

    @ManyToMany(mappedBy="easierTags")
    public List<Tag> harderTags = new ArrayList<Tag>();

    //    @ManyToMany(cascade = CascadeType.REFRESH)
    //    @JoinTable(name = "Question_Tag",
    //               joinColumns = { @JoinColumn(name = "tags_id") },
    //               inverseJoinColumns = { @JoinColumn(name = "question_id") })
    //    public List<Question> questions;

    @ManyToMany(mappedBy = "tags")
    @JoinTable(name = "testrule_tag",
    joinColumns = { @JoinColumn(name = "tags_id") },
    inverseJoinColumns = { @JoinColumn(name = "TestRule_id") })
    public List<TestRule> rules;

    @ManyToOne
    @JoinColumn(name = "topic_id", referencedColumnName = "id")
    public Topic topic;

    public String language;

    @OneToMany(mappedBy = "tags")
    public List<TagTranslation> translations;

    public boolean activated;

    @Enumerated(EnumType.STRING)
    public Status status;

    public Boolean deleted;

    /** Generic query helper for entity Tag with id Long */
    static final Finder<Long, Tag> find = new Finder<Long, Tag>(Long.class, Tag.class);

    public static List<Tag> findAll() {
        return find
            .fetch("easierTags")
                .fetch("topic")
                .findList();
    }

    public static int count() {
        return find.findRowCount();
    }

    /**
     * Return a page of tag
     *
     * @param page Page to display
     * @param pageSize Number of tags per page
     * @param sortBy Tag property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<Tag> page(int page,
            int pageSize,
            String sortBy,
            String order,
            String filter) {
        return find.fetch("easierTags").fetch("topic").where()
                .like("name", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static Map<Long, String> getMap() {
        LinkedHashMap<Long, String> options = new LinkedHashMap<Long,String>();
        for (Tag tag : Tag.find.select("id, name").where().eq("deleted", false).orderBy("name").findList()) {
            options.put(tag.id, tag.name);
        }
        return options;
    }

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.author = ControllerUtils.getCurrentUserDb();
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    public void basicSave() {
        super.save();
    }

    @Override
    public int compareTo(Tag otherTag) {
        return name.compareTo(otherTag.name);
    }

    public static Tag findByName(String filter) {
        return find.where().eq("name", filter).findUnique();
    }

    @Override
    public String toString() {
        return name;
    }

    public String getAuthorEmail() {
        return author == null ? null : author.email;
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }

    @Override
    public Tag getById(Long id) {
        return findById(id);
    }

    public static Tag findById(Long id) {
        return find.select("*")
                .fetch("easierTags")
                .fetch("harderTags")
                .where().eq("id", id).findUnique();
    }

    public static List<Tag> findByRuleIds(List<Long> ruleIds) {
        Logger.debug("---- findByRuleIds ----" + ruleIds);
        List<Tag> tags = null;
        if (ruleIds != null && !ruleIds.isEmpty()) {
            tags = find.where().in("rules.id", ruleIds).findList();
        }
        return tags != null ? tags : new ArrayList<Tag>();
    }

    public List<Question> getQuestions() {
        Logger.debug("--- getQuestions ---");
        List<Question> list = new ArrayList<Question>();
        Question.getQuestionsByRuleIds(this.getRuleIds());
        Logger.debug("list : " + list);
        return list;
    }

    protected List<Long> getRuleIds() {
        List<Long> ids = new ArrayList<Long>();
        if (rules != null && !rules.isEmpty()) {
            ids = new ArrayList<Long>(rules.size());
            for(TestRule rule: rules) {
                ids.add(rule.id);
            }
        }
        Logger.debug("Tag ---- getRuleIds --- " + ids.size());
        return ids;
    }

    public static List<ValuableTag> getValuableTags() {
        Logger.debug("---Tag : getValuableTagsByIds(...)---");
        List<ValuableTag> result;
        String sql = " select t.id, count(q.id) as questionsCount "
                + "from tags t"
                + " join testrule_tag tt on t.id = tt.tags_id "
                + " join testrule tr on tr.id = tt.testrule_id "
                + " join question_testrule qt on tr.id = qt.rules_id "
                + " join question q on q.id = qt.question_id "
                + "group by t.id";
        RawSql rawSql = RawSqlBuilder.parse(sql)
                // map result columns to bean properties
                .columnMapping("t.id", "tag.id")
                .create();
        Query<ValuableTag> query = Ebean.find(ValuableTag.class);
        Logger.debug("---create query --- ");
        result = query.setRawSql(rawSql).findList();
        return  result != null ? result : new ArrayList<ValuableTag>();
    }
}
