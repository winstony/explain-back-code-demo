package models.core;

import com.avaje.ebean.Page;
import models.Deletable;
import models.ModelExtended;
import models.Status;
import models.partners.Partner;
import play.data.validation.Constraints.Required;
import utils.ControllerUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Entity
public class Topic extends ModelExtended implements Deletable {

    private static final long serialVersionUID = -1128669867535681322L;

    @Column
    public String alias;

    @Required
    @Column(name="name")
    public String title;

    @ManyToOne
    public User author;

    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    @ManyToOne
    public Partner creator;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    public boolean activated;

    @Enumerated(EnumType.STRING)
    public Status status;

    public Boolean deleted;

    static final Finder<Long, Topic> find = new Finder<Long, Topic>(Long.class, Topic.class);

    @Override
    public ModelExtended getById(Long id) {
        return find.byId(id);
    }

    public static int count() {
        return find.findRowCount();
    }

    public static Page<Topic> page(int page, int pageSize, String sortBy, String order, String filter) {
        return find.where()
                .like("title", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static Map<String, String> getMap() {
        LinkedHashMap<String, String> options = new LinkedHashMap<String,String>();
        for (Topic topic: Topic.find.where().eq("deleted", false).orderBy("title").findList()) {
            options.put(topic.id.toString(), topic.title);
        }
        return options;
    }

    @Override
    public String toString() {
        return "Topic:{" + id + " : " + title + "}";
    }

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.author = ControllerUtils.getCurrentUserDb();
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    public String getAuthorEmail() {
        return author == null ? null : author.email;
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }
}
