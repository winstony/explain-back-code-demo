package models.core;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceException;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;

import com.avaje.ebean.Ebean;

import play.Logger;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import scala.Option;

import java.util.ArrayList;

@Entity
@Table(name="choice",
       uniqueConstraints=
       {@UniqueConstraint(columnNames={"question_id","choice_text_id"})})
public class Choice extends Model {

    @Id /** Primary key */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Required
    @ManyToOne
    @JoinColumn(name="question_id")
    public Question question;

    @Required
    @ManyToOne
    @JoinColumn(name="choice_text_id")
    public ChoiceText choiceText;

    @Required
    @Column(name="correct")
    public Boolean correct;

    @Column(name = "explanation")
    public String explanation;

    public String text;

    @OneToMany(mappedBy = "choice")
    public List<ChoiceTranslation> translations;

    static Finder<Long, Choice> find =
            new Finder<Long, Choice>(Long.class, Choice.class);

    public Choice() {

    }

    public Choice(Question question, ChoiceText text, String explanation) {
        this(question, text, explanation, false);
    }

    public Choice(Question question, ChoiceText text, String explanation, boolean correct) {
        this.question = question;
        this.choiceText = text;
        this.explanation = StringUtils.trim(explanation);
        this.correct = correct;
    }

    public static void add(Question question, String answerOptionText, String explanation, boolean correct) {
        ChoiceText choiceText = ChoiceText.getOrCreate(answerOptionText.trim());
        add(question, choiceText, explanation, correct);
    }

    public static void add(Question question, ChoiceText text, String explanation, boolean correct) {
        Choice choice = new Choice(question, text, explanation, correct);
        try {
            choice.save();
        } catch (PersistenceException e) {
            Logger.error("Can't persist Choice!!", e);
        }
    }
    
    public static void delete(Question question, ChoiceText text) {
        Choice choice = Choice.find.select("id")
                                   .where()
                                   .eq("question.id", question.id)
                                   .eq("choiceText.id", text.id)
                                   .findUnique();
        choice.delete();
    }

    public static boolean deleteByQuestionIdAndId(Long questionId, Long choiceId) {
        Choice choice = Choice.getById(choiceId);
        Question question = Question.findById(questionId);
        try {
            if ((choice != null) && (question != null) && (choice.question.id == questionId)) {
                choice.delete();
                return true;    
            } 
        } catch (Exception e) {
            Logger.debug("Deleting exception!");
        }
        return false;
    }


	public static Option<Choice> findChoiceByQuestionChoiceText(Question question, String choiceText) {
		Choice choice = find.where()
				.eq("question.id", question.id)
				.like("choiceText.text", choiceText)
				.findUnique();
		return Option.apply(choice);
	}
    
    public String getText() {
        return text;
    }

	public static void deleteByQuestionId(Long questionId) {
		List<Object> choiceIds = Choice.find.select("*")
				.where().eq("question.id", questionId).findIds();
		for (Object cId : choiceIds) {
			Choice.find.byId((Long) cId).delete();
		}
	}

    public List<TestRule> getRules(){
        List<ChoiceRule> choiceRuleListForThisChoice = ChoiceRule.getListOfChoiceRulesByChoiceId(id);
        List<TestRule> testRules = new ArrayList<TestRule>();
        for (ChoiceRule choiceRule: choiceRuleListForThisChoice) {
            testRules.add(choiceRule.testRule); // Ebean.find(TestRule.class).where().eq("id",choiceRule.testRule.id).findUnique();
        }
        return testRules;
    }

	public static Choice getById(Long id) {
		return find.select("*").fetch("choiceText").setId(id).findUnique();
	}

	public String toString() {
		return this.getText();
	}
}
