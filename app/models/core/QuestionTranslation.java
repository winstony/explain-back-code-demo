package models.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "question_multilangs")
public class QuestionTranslation extends Model {

    @Id
    public Long id;

    @ManyToOne
    @Column(name = "question_id")
    public Question question;

    @Column(name = "question_id")
    public Long questionId;

    public String language;

    public String explanation;

    public String text;

    public Boolean deleted;

    public String status;

    public Boolean ready;

    public Long getQuestionId() {
        return questionId;
    }

}
