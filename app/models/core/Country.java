package models.core;

import com.avaje.ebean.Page;
import models.ModelExtended;
import play.data.validation.Constraints.MaxLength;
import play.data.validation.Constraints.Required;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class Country extends ModelExtended {

    @Required
    @MaxLength(255)
    public String name;

    /** ISO 3166 code of country which consists of 3 letters. */
    @Required
    @MaxLength(3)
    public String code;

    /** Currency - 3-character string value according to ISO 4217. */
    @Required
    @MaxLength(3)
    public String currency;

    @OneToMany(mappedBy = "country")
    public List<PsProduct> psProducts;

    static Finder<Long, Country> find = new Finder<>(Long.class, Country.class);

    @Override
    public Country getById(Long id) {
        return find.byId(id);
    }

    public static Country getByCode(String code) {
        return find.where().eq("code", code).findUnique();
    }

    @Override
    public String toString() {
        return name;
    }

    public static Page<Country> page(int page, int pageSize, String sortBy, String order, String filter) {
        return find.where()
                .like("name", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static int count() {
        return find.findRowCount();
    }

    public static Map<String, String> getMap() {
        LinkedHashMap<String, String> options = new LinkedHashMap<String,String>();
        for (Country country: Country.find.orderBy("name").findList()) {
            options.put(country.id.toString(), country.name);
        }
        return options;
    }
}
