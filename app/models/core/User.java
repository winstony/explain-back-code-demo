package models.core;

import be.objectify.deadbolt.core.models.Subject;
import com.avaje.ebean.Page;
import dao.UserDao;
import models.Deletable;
import models.ModelExtended;
import models.deadbolt.SecurityRole;
import models.deadbolt.UserPermission;
import models.secure.SocialUserAdapter;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.validation.Constraints.Email;
import securesocial.core.BasicProfile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class User extends ModelExtended implements Subject, Deletable, Serializable {

    private static final Logger.ALogger log = Logger.of(User.class);

    @Column(name = "translator")
    public boolean translator = false;

    @Column(name = "school_id")
    public Long schoolId;

    @Email
    public String email;

    public String skypename;

    public String fullname;

    @Column(name="isAdmin")
    public boolean isAdmin;

    @Column(name="subscribed")
    public boolean subscribed;

    @OneToMany(mappedBy = "author")
    public List<AnswerRecord> answers;

    @OneToMany(mappedBy = "author")
    public List<Question> questions;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    public List<SocialUserAdapter> accounts;

    @OneToMany(mappedBy = "user")
    public List<Click> clicks;

    @OneToMany(mappedBy = "user")
    public List<UserProduct> userProducts;

    @OneToOne(mappedBy = "user")
    public UserSettings userSettings;

    @ManyToMany
	@JoinTable(name = "user_security_role",
			joinColumns = { @JoinColumn(name = "user_id") },
			inverseJoinColumns = { @JoinColumn(name = "security_role_id") })
    public List<SecurityRole> roles;

    @ManyToMany
	@JoinTable(name = "user_user_permission",
			joinColumns = { @JoinColumn(name = "user_id") },
			inverseJoinColumns = { @JoinColumn(name = "user_permission_id") })
    public List<UserPermission> permissions;

    public BasicProfile profile;

    public String username;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    @ManyToOne
    public Country country;

    public String city;

    @Column(name="date_of_birth")
    public Date dateOfBirth;

    @Column(name="first_name")
    public String firstName;

    @Column(name="last_name")
    public String lastName;

    public Boolean deleted;

    public User(BasicProfile profile, Long schoolId) {
        log.debug("profile = " + profile);
        this.profile = profile;
        this.schoolId = schoolId;
    }

    public User() {
    }

    private User(UserBuilder builder) {
        this.email = builder.email;
        this.skypename = builder.skypename;
        this.fullname = builder.fullname;
        this.isAdmin = builder.isAdmin;
        this.subscribed = builder.subscribed;
        this.answers = builder.answers;
        this.questions = builder.questions;
        this.accounts = builder.accounts;
        this.roles = builder.roles;
        this.permissions = builder.permissions;
        this.profile = builder.profile;
    }

    public static class UserBuilder {
        private String email;
        private String skypename;
        private String fullname;
        private boolean isAdmin;
        private boolean subscribed;
        private List<AnswerRecord> answers;
        private List<Question> questions;
        private List<SocialUserAdapter> accounts;
        private List<SecurityRole> roles;
        private List<UserPermission> permissions;
        private BasicProfile profile;

        public UserBuilder(final String email) {
            this.email = email;
        }

        public UserBuilder skypename(final String skypename) {
            this.skypename = skypename;
            return this;
        }

        public UserBuilder fullname(final String fullname) {
            this.fullname = fullname;
            return this;
        }

        public UserBuilder isAdmin(final boolean isAdmin) {
            this.isAdmin = isAdmin;
            return this;
        }

        public UserBuilder subscribed(final boolean subscribed) {
            this.subscribed = subscribed;
            return this;
        }

        public UserBuilder answers(final List<AnswerRecord> answers) {
            this.answers = answers;
            return this;
        }

        public UserBuilder questions(final List<Question> questions) {
            this.questions = questions;
            return this;
        }

        public UserBuilder accounts(final List<SocialUserAdapter> accounts) {
            this.accounts = accounts;
            return this;
        }

        public UserBuilder roles(final List<SecurityRole> roles) {
            this.roles = roles;
            return this;
        }

        public UserBuilder permissions(final List<UserPermission> permissions) {
            this.permissions = permissions;
            return this;
        }

        public UserBuilder profile(final BasicProfile profile) {
            this.profile = profile;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    /**
     * Generic query helper for entity User with id Long
     */
    private static Finder<Long, User> find = new Finder<Long, User>(Long.class,
                                                                   User.class);

    public static int count() {
        return find.findRowCount();
    }

    /**
     * Return a page of user
     *
     * @param page Page to display
     * @param pageSize Number of users per page
     * @param sortBy User property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<User> page(int page,
                                  int pageSize,
                                  String sortBy,
                                  String order,
                                  String filter) {
        return find.where()
                   .like("fullname", "%" + filter + "%")
                   .orderBy(sortBy + " " + order)
                   .findPagingList(pageSize)
                   .setFetchAhead(false)
                   .getPage(page);
    }

    public static Map<String, String> getMap() {
        LinkedHashMap<String, String> options = new LinkedHashMap<String,String>();
        for (User user: User.find.where().eq("deleted", false).orderBy("email").findList()) {
            options.put(user.id.toString(), user.email);
        }
        return options;
    }

    @Override
    public List<SecurityRole> getRoles() {
        return roles;
    }

    @Override
    public List<UserPermission> getPermissions() {
        return permissions;
    }

    @Override
    public String getIdentifier()
    {
        return email;
    }

    public void setRoles(List<SecurityRole> roles) {
        this.roles = roles;
    }

    public void setPermissions(List<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public static User findByEmailInSchool(String email, Long schoolId) {
        return User.find.where()
                .eq("email", email)
                .eq("schoolId", schoolId)
                .findUnique();
    }

    public int right() {
        return AnswerRecord.countRight(this);
    }

    public int wrong() {
        return AnswerRecord.countWrong(this);
    }

    public int skipped() {
        return AnswerRecord.countSkipped(this);
    }


    public static User findById(Long id) {
        return User.find.byId(id);
    }

    @Override
    public User getById(Long id) {
        return find.byId(id);
    }

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    public static User subscribeWithEmail(String email, long schoolId) {
        if (StringUtils.isBlank(email)) {
            throw new IllegalArgumentException("email must not be blank");
        }
        User user = null;
        User userWithSameEmail = User.findByEmailInSchool(email, schoolId);
        if (userWithSameEmail != null) {
            user = userWithSameEmail;
        } else {
            user = new User();
            user.email = email;
            user.fullname = "";
            user.subscribed = true;
            user.schoolId = schoolId;
            user.save();
        }
        return user;
    }


    public static User findOrCreate(SocialUserAdapter adapter) {
        User user = null;
        if (StringUtils.isBlank(adapter.email)) {
            throw new IllegalArgumentException("email must not be blank");
        }
        User userWithSameEmail = User.findByEmailInSchool(adapter.email, adapter.schoolId);
        if (userWithSameEmail != null) {
            user = userWithSameEmail;
        } else {
            user = new User();
            user.schoolId = adapter.schoolId;
            user.email = adapter.email;
            user.fullname = adapter.fullName;
            user.username = generateUsername(adapter.email);
            user.lastEditedAt = new Date();
            user.save();
        }
        user.profile = adapter.getBasicProfile();
        return user;
    }

    public static String generateUsername(String email) {
        String username = email.split("@")[0];
        username = username.substring(0, Math.min(13, username.length()))
                    .toLowerCase()
                    .replaceAll("[^a-z0-9]+", "")
                    .concat(String.valueOf(new Random().nextInt(10)))
                    .concat(String.valueOf(new Random().nextInt(10)));

        int count = 0;
        while (UserDao.findByUsername(username) != null && count < 100) {
            username = username.substring(0, username.length() - 2)
                    .concat(String.valueOf(new Random().nextInt(10)))
                    .concat(String.valueOf(new Random().nextInt(10)));
            count++;
        }
        if (count == 100) {
            username = UUID.randomUUID().toString().replace("-", "").substring(0, 15);
        }
        return username;
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }

}
