package models.core;

import play.db.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name = "tags_multilangs")
public class TagTranslation extends Model {
    @Id
    public Long id;

    @ManyToOne
    @Column(name = "tags_id")
    public Tag tags;

    @Column(name = "tags_id")
    public Long tagsId;

    public String language;

    public String name;

    public Boolean deleted;

    public String status;

    public Boolean activated;

    public Long getTagId() {
        return tagsId;
    }
}
