package models.core;

import com.avaje.ebean.Page;
import models.ModelExtended;
import play.data.validation.Constraints.Required;

import javax.persistence.*;

@Entity
@Table(name="choice_text",
    uniqueConstraints=
    {@UniqueConstraint(columnNames={"text"})})
public class ChoiceText extends ModelExtended {

    @Required
    @Column(name="text")
    public String text;

    @OneToMany(mappedBy="choiceText")
    @Column(name = "choice_id")
    public Choice choiceId;

    static Finder<Long, ChoiceText> find = new Finder<Long, ChoiceText>(Long.class, ChoiceText.class);

    public ChoiceText() {}

    public ChoiceText(String text) {
        this.text = text;
    }

    public static int count() {
        return find.findRowCount();
    }

    /**
     * Return a page of ChoiceText
     *
     * @param page Page to display
     * @param pageSize Number of ChoiceTexts per page
     * @param sortBy ChoiceText property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<ChoiceText> page(int page,
                                        int pageSize,
                                        String sortBy,
                                        String order,
                                        String filter) {
        return find.where()
                .like("text", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static ChoiceText getOrCreate(String text) {
        ChoiceText sameTextInDatabase = find.where().eq("text", text).findUnique();
        if (sameTextInDatabase != null)
            return sameTextInDatabase;
        ChoiceText newText = new ChoiceText(text);
        newText.save();
        return newText;
    }
    
    @Override
    public String toString() {
        return text;
    }

    public static ChoiceText findById(Long id) {
        return find.byId(id);
    }   


	@Override
	public ChoiceText getById(Long id) {
		return find.byId(id);
	}
}
