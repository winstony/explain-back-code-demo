package models.core;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.PersistenceException;

import com.avaje.ebean.Ebean;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.Logger;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="choice_rule",
    uniqueConstraints=
    {@UniqueConstraint(columnNames={"choice_id", "testrule_id"})})
public class ChoiceRule extends Model {
    
    /** Primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Required
    @ManyToOne  
    @JoinColumn(name="choice_id")
    public Choice choice;

    @Required
    @ManyToOne
    @JoinColumn(name="testrule_id")
    public TestRule testRule;

    
    @Column(name="order_number")
    public Integer order;

    public ChoiceRule(Choice choice, TestRule testRule) {
        this.choice = choice;
        this.testRule = testRule;
        this.order = 0;
    }

    public ChoiceRule(Choice choice, TestRule testRule, Integer order) {
        this.choice = choice;
        this.testRule = testRule;
        this.order = order;
    }


    public static void add(Choice choice, TestRule testRule, Integer order) {
        ChoiceRule choiceRule = new ChoiceRule(choice, testRule, order);
        try { 
            choiceRule.save();
        } catch (PersistenceException e) {
            Logger.error("Can't persist ChoiceRule!!", e);
        }
    }
    
    public static void delete(Choice choice, TestRule testRule) {
        ChoiceRule choiceRule = ChoiceRule.find.select("id")
                                   .where()
                                   .eq("choice.id", choice.id)
                                   .eq("testRule.id", testRule.id)
                                   .findUnique();
        choiceRule.delete();
    }



    static Finder<Long, ChoiceRule> find = new Finder<Long, ChoiceRule>(Long.class, ChoiceRule.class);

    public static List<ChoiceRule> getListOfChoiceRulesByChoiceId(Long choiceId) {
        return Ebean.find(ChoiceRule.class).where().eq("choice.id",choiceId).findList();
    }

    public static List<ChoiceRule> getListOfChoiceRulesByChoiceIds(List<Long> choiceIdsList) {
        List<ChoiceRule> choiceRules = new ArrayList<>();
        for (Long choiceId: choiceIdsList){
            choiceRules.addAll(ChoiceRule.getListOfChoiceRulesByChoiceId(choiceId));
        }
        return choiceRules;
    }

    public List<TestRule> getListOfRulesFromChoiceRules(List<ChoiceRule> choiceRuleList){
        List<TestRule> testRules = new ArrayList<TestRule>();
        for (ChoiceRule choiceRule: choiceRuleList) {
            testRules.add(choiceRule.testRule);
        }
        return testRules;
    }




    public ChoiceRule getById(Long id) {
        return find.byId(id);
    }
}

