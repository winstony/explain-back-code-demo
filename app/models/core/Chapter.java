package models.core;

import com.avaje.ebean.Page;
import models.Deletable;
import models.ModelExtended;
import models.partners.Partner;
import models.partners.Course;
import play.data.validation.Constraints.Required;
import utils.ControllerUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class Chapter extends ModelExtended implements Deletable {

    @Column
    public String alias;

    @Required
    public String name;

    // TODO make @Required after adding to UI
    public Integer level;

    private static final int ALL_CHAPTERS_CODE = -1;

	public static final String MODEL_NAME = "chapter";

	@Deprecated
    @OneToMany(mappedBy = Chapter.MODEL_NAME)
    public List<TestRule> rules;

	@OneToMany(mappedBy = Chapter.MODEL_NAME, cascade = CascadeType.DETACH)
	public List<Question> questions;

//	@OneToOne... does not work with ebean
	@Column(name="easier_id")
	public Chapter easierChapter;
	
    @ManyToOne
    public Course course;

    @Column(name = "course_id")
    public Long courseId;

    @ManyToOne
    public User author;

    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    @ManyToOne
    public Partner creator;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    public Boolean deleted;

    public Boolean activated;

    @Column(name = "is_free")
    public boolean free;

    @Override
    public String toString() {
        return name;
    }

    /** Generic query helper for entity Chapter with id Long */
    private static Finder<Long, Chapter> find =
                    new Finder<Long, Chapter>(Long.class, Chapter.class);

    public static int count() {
        return find.findRowCount();
    }

    /**
     * Return a page of chapter
     *
     * @param page Page to display
     * @param pageSize Number of chapters per page
     * @param sortBy Chapter property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     *
     * @return Page<Chapter>
     */
    public static Page<Chapter> page(int page,
                                     int pageSize,
                                     String sortBy,
                                     String order,
                                     String filter) {
        return find.where()
                   .like("name", "%" + filter + "%")
                   .orderBy(sortBy + " " + order)
                   .findPagingList(pageSize)
                   .setFetchAhead(false)
                   .getPage(page);
    }

    public static Map<String, String> getMap() {
        LinkedHashMap<String, String> options = new LinkedHashMap<String,String>();
        for (Chapter chapter: Chapter.find.where().eq("deleted", false).orderBy("name").findList()) {
            options.put(chapter.id.toString(), chapter.name);
        }
        return options;
    }

    /** Returns chapter by id. */
	@Override
    public Chapter getById(Long id) {
		return find.byId(id);
	}

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.author = ControllerUtils.getCurrentUserDb();
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    public String getAuthorEmail() {
        return author == null ? null : author.email;
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }
}
