package models.core;

import com.avaje.ebean.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import dao.TopicDao;
import models.Deletable;
import models.ModelExtended;
import models.Status;
import models.partners.Course;
import models.partners.Partner;
import models.partners.Task;
import models.partners.TaskItem;
import play.Logger;
import play.data.validation.Constraints.MaxLength;
import play.data.validation.Constraints.Required;
import play.data.validation.ValidationError;
import utils.ControllerUtils;

import javax.persistence.*;

import java.util.*;

/**
 * Class for question
 *
 * @author Anton Ivinsky
 *
 */
@Entity
public class Question extends ModelExtended implements Deletable {
    private static final Logger.ALogger log = Logger.of(Question.class);

    private static final String SPLIT_REGEXP = "\\_[\\_ ]*";
    private static final int SHOW_SHORT_LENGTH = 20;

    public static final String MODEL_NAME = "question";

    @Column(name = "uuid")
    public String uuid;

    @Column(name = "is_free")
    public Boolean free;

    @Column(name = "is_public")
    public Boolean open;

    public String quizfulId;

    /** Marks question is ready to be asked to users. */
    public Boolean ready;

    /** A user that has created the question.*/
    @ManyToOne
    public User author;

    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    @ManyToOne
    public Partner creator;

    @ManyToOne
    public Chapter chapter;

    /** Question - a sentence or a question with an underlines gap. */
    @Required
    @MaxLength(1000)
    @Column(name="text")
    public String question;

    @OneToMany(mappedBy=Question.MODEL_NAME)
    public List<Choice> choices;

    public String hint;

    public String explanation;

    public String language;

    @OneToMany(mappedBy="question")
    public List<QuestionTranslation> translations;

    @ManyToMany
    @JoinTable(name = "Question_TestRule",
    joinColumns =        { @JoinColumn(name = "question_id") },
    inverseJoinColumns = { @JoinColumn(name = "rules_id") })
    public List<TestRule> rules;

    public List<TestRule> getRules() {
        return rules;
    }

    @OneToMany(mappedBy = Question.MODEL_NAME)
    public List<AnswerRecord> userAnswers;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    @ManyToOne
    public Course course;

    @JoinColumn(name = "topic_id", referencedColumnName = "id")
    @ManyToOne
    public Topic topic;

    @JoinTable(name = "question_task",
            joinColumns = { @JoinColumn(
                    name = "question_id",
                    referencedColumnName = "id",
                    nullable = false) },
                    inverseJoinColumns = { @JoinColumn(
                            name = "task_id",
                            referencedColumnName = "id",
                            nullable = false) }
            )
    @ManyToMany
    public List<Task> taskList;

    @OneToMany(mappedBy = "question")
    public List<TaskItem> taskItemList;

    @Enumerated(EnumType.STRING)
    public Status status;

    public Boolean deleted;


    /** Generic query helper for entity Question with id Long */
    static Finder<Long, Question> find =
            new Finder<Long, Question>(Long.class, Question.class);

    @Transient
    public String correctChoiceText;

    @Transient
    public List<String> wrongChoices;

    @Transient
    public String correctExplanation;

    @Transient
    public List<String> wrongExplanations;

    /**
     * WARNING!!!
     * NullPointerException - when qTags is not @Transient
     * random exceptions can occure in such cases
     */
    @Transient
    protected List<Tag> qtags = new ArrayList<Tag>();

    @Column(name = "question_type")
    public String questionType;

    @Column(name = "course_id")
    public Long courseId;

    @Column(name = "chapter_id")
    public Long chapterId;

    @Column(name = "author_id")
    public Long authorId;

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }

    public List<Tag> getQtags() {
        return qtags;
    }

    public void setQtags(List<Tag> tags) {
        qtags.clear();
        qtags.addAll(tags);
    }

    public List<Tag> getTags() {
        if (qtags.isEmpty()) {
            Set<Tag> tagsFromRules = Sets.newHashSet();
            for (TestRule rule : rules) {
                if (rule.tags != null) {
                    tagsFromRules.addAll(rule.tags);
                }
            }
            qtags = Lists.newArrayList(tagsFromRules);
        }
        return qtags;
    }

    protected List<Long> getRuleIds() {
        List<Long> ids = new ArrayList<Long>();
        if (rules != null && !rules.isEmpty()) {
            ids = new ArrayList<Long>(rules.size());
            for(TestRule rule: rules) {
                ids.add(rule.id);
            }
        }
        return ids;
    }

    public static int count() {
        return find.findRowCount();
    }

    /**
     * Return a page of question
     *
     * @param page Page to display
     * @param pageSize Number of questions per page
     * @param sortBy Question property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<Question> page(int page,
                                  int pageSize,
                                  String sortBy,
                                  String order,
                                  String filter) {
        return find.where()
                   .like("question", "%" + filter + "%")
                   .orderBy(sortBy + " " + order)
                   .fetch("choices")
                   .fetch("choices.choiceText")
                   .findPagingList(pageSize)
                   .setFetchAhead(false)
                   .getPage(page);
    }

    public static Collection<Question> getTaggedQuestions(Collection<Tag> tags) {
        log.debug("getTaggedQuestions(...) " + tags);
        Set<Question> questions = null;
        if (tags != null && !tags.isEmpty()) {
            questions = find.select("*")
               .where().eq("ready", true)
               .where().in("rules.tags", tags)
               .findSet();
        }
        log.debug(" questions with tags " + questions);
        return questions != null ? questions : new HashSet<Question>();
    }

    /**
     * Show the question itself if it is less than 60 characters,
     * else show first 60 characters + three dots.
     */
    @Override
    public String toString() {
        return question == null ? null :
            question.substring(0, Math.min(question.length(), 60))
                        + (question.length() > 60 ? "..." : "");
    }

    public List<String> getWrongAnswersAsList() {
        ArrayList<String> list = new ArrayList<String>();
        for (Choice c : choices) {
            if (!c.correct) {
                list.add(c.choiceText.text);
            }
        }
        return list;
    }

	public List<String> getAnswersAsList() {
	    ArrayList<String> list = new ArrayList<String>();
		for (Choice c : choices) {
		    list.add(c.choiceText.text);
		}
	    Collections.sort(list, new Comparator<String>() {
			@Override public int compare(String o1, String o2) {
				return o1.hashCode() - o2.hashCode();
			}
		});
		return list;
	}
	
	public static Question getRandom() {
	    return getRandom(new TopicDao().findByName("English"));
	};
	
    public static Question getRandom(Topic topicToQuery) {
        log.debug("Getting random question");
        List<Object> ids = Question.find.where().eq("ready", true)
                .eq("topic", topicToQuery).findIds();
        if (ids.isEmpty()) {
            return null;
        }
        int index = Math.abs(new Random(System.nanoTime()).nextInt()) % ids.size();
        return findById((Long) ids.get(index));
    }

	public static Question findById(Long id) {
		Question question = Question.find.select("*")
		        .fetch("choices")
		        .fetch("choices.choiceText")
		        .fetch("topic")
		        .setId(id).findUnique();
		if (question != null) question.postLoad();
		return question;
	}

    @Override
    public Question getById(Long id) {
    	return findById(id);
    }

    /** Is called after getById, jpa annotation @PostLoad do not work with ebean */
    public void postLoad() {
        fillCorrectChoiceText();
        fillWrongChoices();
    }

    private void fillCorrectChoiceText() {
        correctChoiceText = getCorrectChoice() == null ? null : getCorrectChoice().choiceText.text;
        correctExplanation = getCorrectChoice() == null ? null : getCorrectChoice().explanation;
    }

    private void fillWrongChoices() {
        this.wrongChoices = new ArrayList<String>();
        this.wrongExplanations = new ArrayList<String>();
        if (choices == null || choices.isEmpty()) return;
        List<Choice> wrongChoices = new ArrayList<Choice>();
        wrongChoices.addAll(choices);
        wrongChoices.remove(getCorrectChoice());
        for (Choice c : wrongChoices) {
            this.wrongChoices.add(c.choiceText.text);
            this.wrongExplanations.add(c.explanation);
        }
    }

    // /**
    //  * Returns number of questions that user never ever answered or skipped.
    //  * @param user
    //  * @return number of questions user never answered.
    //  */
    // public static int countNeverAnswered(User user) {
    //     Query<AnswerRecord> subQuery =
    //             AnswerRecord.find.select("question")
    //                              .where().eq("author", user)
    //                              .query();
    //     return Question.find.where().not(Expr.in("id", subQuery)).findRowCount();
    // }

    public static List<Object> allIds() {
        return find.findIds();
    }

    public Choice getCorrectChoice() {
        for (Choice choice : choices)
            if (choice.correct) return choice;
        return null;
    }

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        Choice.deleteByQuestionId(this.id);
        Choice.add(this, correctChoiceText, correctExplanation, true);
        saveWrongChoices();
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.author = ControllerUtils.getCurrentUserDb();
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
        Choice.add(this, correctChoiceText, correctExplanation, true);
        saveWrongChoices();
    }

    public void basicSave() {
        super.save();
    }

    private void saveWrongChoices() {
        if (wrongChoices != null) {
            int choicesSize = wrongChoices == null ? 0 : wrongChoices.size();
            int explanationsSize = wrongExplanations == null ? 0 : wrongExplanations.size();
            for (int index = 0; index < choicesSize; ++index) {
                String choiceText = wrongChoices.get(index);
                String explanation = index < explanationsSize ? wrongExplanations.get(index) : null;
                Choice.add(this, choiceText, explanation, false);
            }
        }
    }

    public String getPrettyAnswer(List<String> answers, boolean showShort) {
        return this.getPrettyAnswer(answers, showShort, false);
    }

    public String getPrettyAnswer(List<String> answers, boolean showShort, boolean addClearStyle) {
        String[] splittedQuestion = question.split(SPLIT_REGEXP);
        System.out.println(Arrays.toString(splittedQuestion));
        String result = splittedQuestion[0];
        int iter = 1;
        for(String str: answers) {
            if (iter < splittedQuestion.length) {
               result += " <strong class=\"" + (addClearStyle ? "nice-gap" : "") + "\"> "  + str + " </strong> " + splittedQuestion[iter];
            }
            ++iter;
        }
        if (iter < splittedQuestion.length) {
            result += splittedQuestion[iter];
        }
        if (showShort) {
            result = result.replaceAll("\\<.*?>","");
            return result.length() > SHOW_SHORT_LENGTH ? result.substring(0, SHOW_SHORT_LENGTH) : result;
        }
        return result;
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        if (!ready)
            return null;
        if (correctChoiceText == null || correctChoiceText.trim().equals("")) {
            errors.add(new ValidationError("correctChoiceText",
                    "Correct choice is required for production", null));
        }
        if (wrongChoices == null) errors.add(new ValidationError("wrongChoices",
                "Wrong answers are required for production", null));
        else {
            for (int i = wrongChoices.size() - 1; i >= 0; i--)
                if (wrongChoices.get(i) == null
                        || wrongChoices.get(i).trim().equals("")
                        || (correctChoiceText != null
                                  && wrongChoices.get(i).trim().equals(correctChoiceText.trim())) )
                    wrongChoices.remove(i);
            if (wrongChoices.isEmpty()) errors.add(new ValidationError("wrongChoices",
                    "Wrong answers are required for production", null));
        }
        return errors.isEmpty() ? null : errors;
    }

    public String getAuthorEmail() {
        return author == null ? null : author.email;
    }

	public String getChapterName() {
		return chapter == null ? null : chapter.name;
	}

    public String getPrettyQuestion() {
        String[] correctOptions = this.getCorrectChoice().choiceText.text.split(",");
        Arrays.fill(correctOptions, "_");
        return this.getPrettyAnswer(Arrays.asList(correctOptions), false, true);
    }

    public static List<Question> findByIds(List<Long> ids) {
        Logger.debug("---- findByIds ----" + ids);
        List<Question> questions = null;
        if (ids != null && !ids.isEmpty()) {
            questions = find.where().in("id", ids).findList();
        }
        return questions != null ? questions : new ArrayList<Question>();
    }

    public static List<Question> getQuestionsByRuleIds(List<Long> ruleIds) {
        if (ruleIds != null && !ruleIds.isEmpty()) {
           return  find.where().in("rules.id", ruleIds).findList();
        }
        return new ArrayList<Question>();
    }

    public static List<Question> filterByChapterAndTags(Long chapterId, List<Long> tagIds) {
//        String sql =
//            "SELECT id \n" +
//            "FROM question q\n" +
//                "JOIN Question_TestRule u1z_ ON u1z_.question_id = t0.id \n" +
//                "JOIN testrule u1 ON u1.id = u1z_.rules_id \n" +
//                "JOIN testrule_tag u2z_ ON u2z_.TestRule_id = u1.id \n" +
//                "JOIN tags u2 ON u2.id = u2z_.tags_id  " +
//            "GROUP BY id";
//        RawSql rawSql =
//            RawSqlBuilder
//                .parse(sql)
//                .columnMapping("id", "id")
//                .create();
        ExpressionList<Question> query =
//                find.setRawSql(rawSql).where();//.in("rules.tags.id", tagIds);
            find.where().in("rules.tags.id", tagIds);
        Logger.debug("query : "  + query.query().getGeneratedSql());
        if (tagIds != null && !tagIds.isEmpty()) {
            return query.findList();
        }
        return null;
    }

	/**
	 * Get all questions added to specified task
	 * @param taskId
	 * @return
	 */
    public static List<Question> getFromTask(Long taskId) {
		return find.fetch("taskList")
				.where()
					.eq("taskList.id", taskId)
				.findList();
    }

	/**
	 * Get all questions that available for adding
	 * @param taskId
	 * @return
	 */
	public static List<Question> getNotFromTask(Long taskId) {
		/**
		 * as 'fieldName' - you don't need columnMapping when using column alias
		 */
		String sql = "select distinct " +
					"t1.id as id, " +
					"t1.ready as ready, " +
					"t1.text as question, " +
					"t1.hint as hint, " +
					"t1.explanation as explanation, " +
					"t1.last_edited_at as lastEditedAt, " +
					"t1.author_id, " +
					"t1.chapter_id " +
				"from QUESTION_TASK t0 " +
					"right join Question  t1 " +
					"on t1.id = t0.Question_id " +
				"where t0.task_id IS NULL OR t0.task_id <> " +
				taskId;
		RawSql rawSql = RawSqlBuilder.parse(sql)
				.columnMapping("t1.author_id",  "author.id")
				.columnMapping("t1.chapter_id",  "chapter.id")
				.create();
		return find.setRawSql(rawSql)
				.findList();
	}

    public boolean isCorrect(String value) {
        return value.equals(this.correctChoiceText);
    }

}
