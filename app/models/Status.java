package models;

import java.util.LinkedHashMap;
import java.util.Map;

public enum Status {
    // Translation exists and it should be checked - edited on client UI
    PENDING("PENDING"),

    // Translation exists and is approved
    APPROVED("APPROVED"),

    // Translation exists and is rejected
    REJECTED("REJECTED"),

    // Translation exists and it should be checked - edited on Partners area
    WAITING_APPROVAL("WAITING_APPROVAL"),

    // Question is available for translation
    OPEN("OPEN");

    public final String status;

    Status(String status) {
        this.status = status;
    }

    public static Map<String, String> getMap() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        options.put(Status.PENDING.name(), "PENDING");
        options.put(Status.APPROVED.name(), "APPROVED");
        options.put(Status.REJECTED.name(), "REJECTED");
        options.put(Status.OPEN.name(), "OPEN");
        return options;
    }
}