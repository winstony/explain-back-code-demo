package models.secure

import play.api.libs.json._
import play.api.libs.functional.syntax._
import securesocial.core._


/**
 * The OAuth 1 details
 *
 * @param token the token
 * @param secret the secret
 */
case class OAuth1InfoTO(
	token: String, 
	secret: String)

object OAuth1InfoTO {
	implicit val oAuth1InfoTOFrmt = Json.format[OAuth1InfoTO]
}

/**
 * The Oauth2 details
 *
 * @param accessToken the access token
 * @param tokenType the token type
 * @param expiresIn the number of seconds before the token expires
 * @param refreshToken the refresh token
 */
case class OAuth2InfoTO(
	accessToken: String, 
	tokenType: Option[String] = None,
  	expiresIn: Option[Int] = None, 
  	refreshToken: Option[String] = None)

object OAuth2InfoTO {
	implicit val oAuth2InfoTOFrmt = Json.format[OAuth2InfoTO]
}

/**
 * The password details
 *
 * @param hasher the id of the hasher used to hash this password
 * @param password the hashed password
 * @param salt the optional salt used when hashing
 */
case class PasswordInfoTO(
	hasher: String, 
	password: String, 
	salt: Option[String] = None)

object PasswordInfoTO {
	implicit val passwordInfoTOFrmt = Json.format[PasswordInfoTO]
}

case class BasicProfileTO(
  providerId: String,
  userId: String,
  firstName: Option[String],
  lastName: Option[String],
  fullName: Option[String],
  email: Option[String],
  avatarUrl: Option[String],
  authMethod: String,
  oAuth1Info: Option[OAuth1InfoTO] = None,
  oAuth2Info: Option[OAuth2InfoTO] = None,
  passwordInfo: Option[PasswordInfoTO] = None,
  // only for save API call
  saveMode: Option[String] = None)

object BasicProfileTO {
	implicit val basicProfileTOFrmt = Json.format[BasicProfileTO]
}

case class UserTO(
	email: String
)

object UserTO {
	implicit val userTOFrmt = Json.format[UserTO]
}

case class UserPasswordTO(
	user: UserTO,
	password: PasswordInfoTO)

object UserPasswordTO {
	implicit val userPasswordTOFrmt = Json.format[UserPasswordTO]
}
