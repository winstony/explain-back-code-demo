package models.secure;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import models.ModelExtended;
import models.core.User;
import play.Logger;
import play.data.validation.Constraints.Required;
import scala.Option;
import securesocial.core.AuthenticationMethod;
import securesocial.core.BasicProfile;
import securesocial.core.GenericProfile;
import securesocial.core.PasswordInfo;
import securesocial.core.UserProfile;
import securesocial.core.providers.utils.PasswordHasher;

import com.avaje.ebean.Page;

/**
 * This class allows to store information about social users in database with
 * all the cool features of Play! 2.x Model parent class and ModelExtended.
 *
 * Do not use this class to get any information about users.
 * Use either SociualUser or just User.
 */
@Entity
@Table(name = "socialuseradapter")
public class SocialUserAdapter extends ModelExtended {

    // ---------------- old fields ----------------
    @ManyToOne
    @Required
    private User user;

    @Column(name = "school_id")
    public Long schoolId;

    //TODO create this three fields.
    public String firstName;
    public String lastName;
    public String fullName;
    public String email;

    @Column(name="accountId")
    public String accountId;

    public String provider;

    @Transient
    @Column(name="onlySocialAccount")
    @Deprecated //and needs to be deleted
    public Boolean onlySocialAccount;

    // ----------------- new fields -------------------

    /**
     * A URL pointing to the user avatar
     */
    public String avatarUrl;

    /**
     * The method that was used to authenticate the user.
     */
    //@Enumerated(EnumType.STRING)
    //@Column(columnDefinition = "text")
    public String authMethod;

    /**
     * The hashed user password
     */
    public String password;

    /**
     * The salt used to hash the password
     */
    public String salt;

    /**
     * Generic query helper for entity User with id Long
     */
    private static final Finder<Long, SocialUserAdapter> find =
            new Finder<Long, SocialUserAdapter>(Long.class, SocialUserAdapter.class);

    public static int count() {
        return find.findRowCount();
    }

    public void setPasswordInfo(PasswordInfo info) {
        password = info.password();
        salt = info.salt().isDefined() ? info.salt().get() : null;
        // not storing haser;
    }

    /**
     * Return a page of user adapter
     *
     * @param page Page to display
     * @param pageSize Number of users per page
     * @param sortBy User property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<SocialUserAdapter> page(int page,
            int pageSize,
            String sortBy,
            String order,
            String filter) {
        return find.where()
                .like("fullName", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static SocialUserAdapter create(BasicProfile account, Long schoolId) {
        if (account.userId() == null) {
            throw new NullPointerException("userId id is null");
        } else if (account.providerId() == null) {
            throw new NullPointerException("providerId id is null");
        } else if (schoolId == null) {
            throw new NullPointerException("schoolId id is null");
        }
        SocialUserAdapter adapter = fromBasicProfile(account, schoolId);
        adapter.user = User.findOrCreate(adapter);
        adapter.save();
        return adapter;
    }

    public static SocialUserAdapter find(String providerId, String userId, Long schoolId) {
        SocialUserAdapter adapter = find.fetch("user")
                .where()
                .eq("accountId", userId)
                .eq("provider", providerId)
                .eq("schoolId", schoolId)
                .findUnique();
        return adapter;
    }

    public static SocialUserAdapter findByEmailAndProvider(String providerId, String email, Long schoolId) {
        SocialUserAdapter adapter = find.fetch("user")
                .where()
                .eq("email", email)
                .eq("provider", providerId)
                .eq("schoolId", schoolId)
                .findUnique();
        return adapter;
    }

    public BasicProfile getBasicProfile() {
        BasicProfile basicProfile = new BasicProfile(
                provider,
                accountId,
                Option.apply(firstName),
                Option.apply(lastName),
                Option.apply(fullName),
                Option.apply(email),
                Option.apply(avatarUrl),
                new AuthenticationMethod(authMethod),
                //TODO: do we need to store this information in DB?
                // looks like we should store oauth1 an oauth2 info
                Option.apply(null), // oauth1info
                Option.apply(null), // oauth2info
                Option.apply(new PasswordInfo(
                        (new PasswordHasher.Default()).id(),
                        password,
                        Option.apply(salt)))
                );
        return basicProfile;
    }

    private static SocialUserAdapter fromBasicProfile(BasicProfile socialUser, Long schoolId) {
        if (socialUser == null) {
            return null;
        }
        SocialUserAdapter adapter = new SocialUserAdapter();
        adapter.schoolId = schoolId;
        adapter.accountId = socialUser.userId();
        adapter.provider = socialUser.providerId();
        adapter.email = socialUser.email().isDefined() ? socialUser.email().get() : null;
        adapter.authMethod = socialUser.authMethod().method();
        adapter.avatarUrl = socialUser.avatarUrl().isDefined() ? socialUser.avatarUrl().get() : null;
        if (socialUser.passwordInfo().isDefined()) {
            adapter.password = socialUser.passwordInfo().get().password();
            if (socialUser.passwordInfo().get().salt().isDefined()) {
                adapter.salt = socialUser.passwordInfo().get().salt().get();
            }
        }
        adapter.firstName = socialUser.firstName().isDefined() ? socialUser.firstName().get() : "";
        adapter.lastName = socialUser.lastName().isDefined() ? socialUser.lastName().get() : "";
        adapter.fullName = socialUser.fullName().isDefined() ? socialUser.fullName().get() : "";
        return adapter;
    }

    @Override
    public SocialUserAdapter getById(Long id) {
        return find.byId(id);
    }

    public User getUser() {
        user.profile = getBasicProfile();
        return user;
    }
}