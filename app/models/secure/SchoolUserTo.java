package models.secure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SchoolUserTo implements Serializable {
    private static final long serialVersionUID = 1L;

    @SuppressWarnings("unused")
    private SchoolUserTo() {
        // deserialization constructor
    }

    public SchoolUserTo(String fullName, Long userId, Long schoolId, String email) {
        this.fullName = fullName;
        this.id = userId;
        this.schoolId = schoolId;
        this.email = email;
    }

    public String avatarUrl;
    public String fullName;
    public Long id;
    public Long schoolId;
    public String email;
    public String username;
}
