package models.partners;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import models.ModelExtended;
import models.core.Question;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kirill-laptop
 * Date: 03.11.13
 * Time: 14:50
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="QUESTION_TASK")
public class TaskItem extends Model
		implements java.io.Serializable, Comparable<TaskItem> {

	@Embeddable
	public static class TaskItemId implements java.io.Serializable {
		private static final long serialVersionUID = 1L;

		@Basic
		private Long questionId;

		@Basic
		private Long taskId;

		public TaskItemId() {}

		public TaskItemId(Long taskId, Long questionId) {
			this.questionId = questionId;
			this.taskId = taskId;
		}

		public void setQuestionId(Long questionId) {
			this.questionId = questionId;
		}

		public Long getQuestionId() {
			return questionId;
		}

		public void setTaskId(Long taskId) {
			this.taskId = taskId;
		}

		public Long getTaskId() {
			return this.taskId;
		}

		@Override
		public boolean equals(Object otherOb) {
			if (this == otherOb) {
				return true;
			}
			if (!(otherOb instanceof TaskItemId)) {
				return false;
			}
			TaskItemId other = (TaskItemId) otherOb;
			return this.hashCode() == other.hashCode();
		}

		@Override
		public int hashCode() {
			int hash = 3;
			hash = 89 * hash + (questionId==null?0:questionId.hashCode());
			hash = 89 * hash + (taskId==null?0:taskId.hashCode());
			return hash;
		}
	}

//	@EmbeddedId
//	public TaskItemId id;

	@Column(name = "ORDER_NUMBER")
	private Integer order;

//	@MapsId("questionId")
	@ManyToOne
	@JoinColumn(name = "QUESTION_ID")
	private Question question;

//	@MapsId("taskId")
	@ManyToOne
	@JoinColumn(name="TASK_ID")
	private Task task;

	public static final Finder<Long, TaskItem> find;

	static {
		find = new Finder<Long, TaskItem>(Long.class, TaskItem.class);
	}

	public static TaskItem findByTaskIdQuestionId(Long taskId, Long questionId) {
		return find.where()
				.eq("task.id", taskId)
				.eq("question.id", questionId)
				.findUnique();
	}

    public static List<TaskItem> findByTaskIdQuestionIds(Long taskId, List<Long> questionIds) {
        return find.where()
                .eq("task.id", taskId)
                .in("question.id", questionIds)
                .findList();
    }

	public static void saveUpdate(Long id, Long questionId, Integer order) {
		TaskItem item;
		Boolean notExists;
		item = TaskItem.findByTaskIdQuestionId(id, questionId);
		notExists = item == null;
		if (notExists) {
			item = new TaskItem();
			Task tsk = new Task();
			tsk = tsk.getById(id);
			Question qstn = Question.findById(questionId);
			item.setQuestion(qstn);
			item.setTask(tsk);
		}

		item.setOrder(order);
		if (notExists) {
			item.save();
		} else {
			item.update();
		}
	}

	public static void remove(Long id, Long questionId) {
		TaskItem item;
		Boolean isExists;
		item = TaskItem.findByTaskIdQuestionId(id, questionId);
		isExists = item != null;
		if (isExists) {
			String deleteItemSQL = "DELETE FROM QUESTION_TASK WHERE QUESTION_ID=:questionId AND TASK_ID=:taskId";
			SqlUpdate update = Ebean.createSqlUpdate(deleteItemSQL)
					.setParameter("questionId", questionId)
					.setParameter("taskId", id);
			int rows = update.execute();
		} else {
			throw new NoResultException("There is no item with such taskId = "
					+ id + " and questionId = " + questionId);
		}
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Task getTask() {
		return this.task;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Question getQuestion() {
		return this.question;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Integer getOrder() {
		return this.order;
	}
//
//	public void setId(TaskItemId id) {
//		this.id = id;
//	}
//
//	public TaskItemId getId() {
//		return this.id;
//	}

	@Override
	public int compareTo(TaskItem that) {
		return this.getOrder() - that.getOrder();
	}
}
