package models.partners;

import com.avaje.ebean.Page;
import models.Deletable;
import models.ModelExtended;
import models.core.User;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "partner")
public class Partner extends ModelExtended implements Deletable {

    private static final long serialVersionUID = 201309102339L;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    @Nullable
    public User user;

    @OneToMany(mappedBy = "creator")
    public List<Course> coursesList;

    @Column(name="first_name")
    public String firstName;

    @Column(name = "last_name")
    public String lastName;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    public Boolean deleted;

    public Partner() {
    }

    public Partner(Long id) {
        this.id = id;
    }

    private static final Finder<Long, Partner> find = new Finder<Long, Partner>(Long.class, Partner.class);

    @Override
    public Partner getById(Long id) {
        return find.byId(id);
    }

    public static int count() {
        return find.findRowCount();
    }

    public static Page<? extends ModelExtended> page(
            int page,
            int pageSize,
            String sortBy,
            String order,
            String filter) {
        return find.where()
            .orderBy(sortBy + " " + order)
            .findPagingList(pageSize)
            .setFetchAhead(false)
            .getPage(page);
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }
}
