package models.partners;

import com.avaje.ebean.Page;
import models.Deletable;
import models.ModelExtended;
import models.Status;
import models.core.User;
import play.data.validation.Constraints;
import utils.ControllerUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kyrylo Holodnov
 */
@Entity
@Table(name = "task")
@NamedQueries({
		@NamedQuery(name = Task.FIND_ALL, query = "SELECT t FROM task t"),
		@NamedQuery(name = Task.FIND_BY_ID, query = "SELECT t FROM task t WHERE t.id = :id"),
		@NamedQuery(name = Task.FIND_BY_TYPE, query = "SELECT t FROM task t WHERE t.type = :type") })
public class Task extends ModelExtended implements Deletable {

	private static final long serialVersionUID = 201309102313L;

	public static final String FIND_ALL = "task.findAll";
	public static final String FIND_BY_ID = "task.findById";
	public static final String FIND_BY_TYPE = "task.findByType";

	public static final String MODEL_NAME = "task";

	@Constraints.Required
	public String name;

	@Column(length = 255)
	public String type;

	@Column(name="result_msg")
	public String resultMessage;

	@OneToMany(mappedBy = "task")
	public List<TaskItem> taskItemList;

	@ManyToOne
	@JoinColumn(
		name = "course_id",
		referencedColumnName = "id")
	public Course course;

    @ManyToOne
    public User author;

    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    @ManyToOne
    public Partner creator;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    public boolean activated;

    @Enumerated(EnumType.STRING)
    public Status status;

    public Boolean deleted;


	/**
	 * Generic query helper for entity Task with id Long
	 */
	public static final Finder<Long, Task> find = new Finder<Long, Task>(Long.class,
			Task.class);

    /**
     * Generic query helper for entity TaskItem with id Long
     */
    public static final Finder<Long, TaskItem> findTaskItem = new Finder<Long, TaskItem>(Long.class, TaskItem.class);

    public static int count() {
        return find.findRowCount();
    }

    /**
	 * Return a page of tag
	 *
	 * @param page Page to display
	 * @param pageSize Number of tags per page
	 * @param sortBy Tag property used for sorting
	 * @param order Sort order (either or asc or desc)
	 * @param filter Filter applied on the name column
	 */
	public static Page<Task> page(int page,
								 int pageSize,
								 String sortBy,
								 String order,
								 String filter) {
		return find.where()
				.like("name", "%" + filter + "%")
				.orderBy(sortBy + " " + order)
				.findPagingList(pageSize)
                .setFetchAhead(false)
				.getPage(page);
	}

	public Task() {}

	public Task(Long id) {
		this.id = id;
	}

    @Override
    public Task getById(Long id) {
        return find.byId(id);
    }

	public static Task findById(Long id) {
		return find.byId(id);
	}

    @Override
    public String toString() {
        return name;
    }

	public List<TaskItem> getTaskItemList() {
        this.taskItemList = findTaskItem.where().eq("task_id",id).findList();
		return this.taskItemList;
	}

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.author = ControllerUtils.getCurrentUserDb();
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    public String getAuthorEmail() {
        return author == null ? null : author.email;
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }
}
