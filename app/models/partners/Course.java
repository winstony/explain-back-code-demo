package models.partners;

import com.avaje.ebean.Page;

import models.Deletable;
import models.ModelExtended;
import models.Status;
import models.core.Chapter;
import models.core.Topic;
import models.core.User;
import play.data.validation.Constraints.MaxLength;

import javax.persistence.*;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/**
 * @author Kyrylo Holodnov
 */
@Entity
@Table(name = "course")
@NamedQueries({
		@NamedQuery(name = Course.FIND_ALL,         query = "SELECT c FROM course c"),
		@NamedQuery(name = Course.FIND_BY_ID,        query = "SELECT c FROM course c WHERE c.id = :id"),
		@NamedQuery(name = Course.FIND_BY_NAME,      query = "SELECT c FROM course c WHERE c.name = :name"),
		@NamedQuery(name = Course.FIND_BY_API_KEY,    query = "SELECT c FROM course c WHERE c.apikey = :apikey"),
		@NamedQuery(name = Course.FIND_BY_LOGO,      query = "SELECT c FROM course c WHERE c.logo = :logo"),
		@NamedQuery(name = Course.FIND_BY_SECRET_KEY, query = "SELECT c FROM course c WHERE c.secretkey = :secretkey") })
public class Course extends ModelExtended implements Deletable {

	private static final long serialVersionUID = 201309102320L;

	public static final String FIND_ALL = "course.findAll";
	public static final String FIND_BY_ID = "course.findById";
	public static final String FIND_BY_NAME =  "course.findByName";
	public static final String FIND_BY_API_KEY = "course.findByApikey";
	public static final String FIND_BY_LOGO = "course.findByLogo";
	public static final String FIND_BY_SECRET_KEY = "course.findBySecretkey";

    @Column
    public String alias;

	@Column(length = 255)
	public String name;
	@Column(length = 255)
	public String apikey;
	@Column(length = 255)
	public String logo;
	@Column(length = 255)
	public String secretkey;
	@JoinTable(name = "user_course",
	           joinColumns = { @JoinColumn(
	                   name = "COURSE_ID",
	                   referencedColumnName = "ID",
	                   nullable = false) },
	           inverseJoinColumns = { @JoinColumn(
	                   name = "USER_ID",
	                   referencedColumnName = "ID",
	                   nullable = false) })
	@ManyToMany
	public List<User> userList;
	@OneToMany(mappedBy = "course")
	public List<CourseAdmin> courseToAdminsList;
	@OneToMany(mappedBy = "course")
	public List<Task> taskList;


    @OneToMany(mappedBy = "course")
    public List<Chapter> chapters;

    @JoinColumn(name = "creator_id", referencedColumnName = "id")
	@ManyToOne
	public Partner creator;

    @ManyToOne
    @JoinColumn(name = "topic_id", referencedColumnName = "id")
    public Topic topic;

    @ManyToOne
    @JoinColumn(name = "school_id", referencedColumnName = "id")
    public School school;

    @Column(name = "school_id")
    public Long schoolId;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    @Column(name="short_description")
    @MaxLength(140)
    public String shortDescription;

    @MaxLength(2000)
    public String description;

    public boolean activated;

    @Enumerated(EnumType.STRING)
    public Status status;

    public Boolean deleted;

	public Course() {
	}

	public Course(Long id) {
		this.id = id;
	}

    private static final Finder<Long, Course> find = new Finder<Long, Course>(Long.class,
            Course.class);

    public Topic getTopic() {
        return topic;
    }

    @Override
    public Course getById(Long id) {
		return find.byId(id);
	}

    @Override
    public String toString() {
        return name;
    }

    public static int count() {
        return find.findRowCount();
    }

    public static Page<Course> page(int page, int pageSize, String sortBy, String order, String filter) {
        return find.where()
                .like("name", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static Map<String, String> getMap() {
        LinkedHashMap<String, String> options = new LinkedHashMap<String,String>();
        for (Course course : Course.find.where().eq("deleted", false).orderBy("name").findList()) {
            options.put(course.id.toString(), course.name);
        }
        return options;
    }

    public static Course findById(Long id) {
        return find.select("*").setId(id).findUnique();
    }

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }
}
