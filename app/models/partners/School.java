package models.partners;

import com.avaje.ebean.Page;

import models.Deletable;
import models.ModelExtended;
import models.Status;
import models.core.Product;
import play.data.validation.Constraints;
import play.data.validation.Constraints.MaxLength;

import javax.persistence.*;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "school")
public class School extends ModelExtended implements Deletable {

    private static final long serialVersionUID = 7321207625597716851L;

    @Column
    public String alias;

    @Column(name = "name")
    public String name;

    @Column(name = "logoUrl")
    public String logoUrl;

    @Column(name = "siteUrl")
    public String siteUrl;

    @Constraints.Email
    public String email;

    @ManyToOne
    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    public Partner creator;

    @OneToMany(mappedBy = "school")
    public List<Product> products;

    @Column(name="posted_at")
    public Date postedAt;

    @Column(name="last_edited_at")
    public Date lastEditedAt;

    @Column(name="short_description")
    @MaxLength(140)
    public String shortDescription;

    @MaxLength(2000)
    public String description;

    public boolean activated;

    @Enumerated(EnumType.STRING)
    public Status status;

    public Boolean deleted;

    private static final Finder<Long, School> find = new Finder<Long, School>(Long.class, School.class);

    @Override
    public ModelExtended getById(Long id) {
        return find.byId(id);
    }

    public static int count() {
        return find.findRowCount();
    }

    public static Page<School> page(int page, int pageSize, String sortBy, String order, String filter) {
        return find.where()
                .like("name", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void update(Object id) {
        this.id = (Long) id;
        this.lastEditedAt = new Date();
        super.update(this.id);
    }

    @Override
    public void save() {
        this.lastEditedAt = this.postedAt = new Date();
        super.save();
    }

    @Override
    public void setDeleted(boolean isDeleted) {
        this.deleted = isDeleted;
    }

    @Override
    public boolean isDeleted() {
        return this.deleted;
    }

    public static Map<String, String> getMap() {
        LinkedHashMap<String, String> options = new LinkedHashMap<String,String>();
        for (School school: School.find.where().eq("deleted", false).orderBy("name").findList()) {
            options.put(school.id.toString(), school.name);
        }
        return options;
    }
}
