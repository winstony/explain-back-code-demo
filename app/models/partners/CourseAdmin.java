package models.partners;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import models.ModelExtended;
import models.core.User;
import models.deadbolt.SecurityRole;
import models.deadbolt.UserPermission;

/**
 * @author Kyrylo Holodnov
 */
@Entity
@Table(name = "course_admin")
@NamedQueries({@NamedQuery(name = CourseAdmin.FIND_ALL,
                           query = "SELECT c FROM course_admin c"),
               @NamedQuery(name = CourseAdmin.FIND_BY_ID,
                           query = "SELECT c FROM course_admin c WHERE c.id = :id") })
public class CourseAdmin extends ModelExtended {

    private static final long serialVersionUID = 201309102349L;

	public static final String FIND_ALL = "course_admin.findAll";
	public static final String FIND_BY_ID = "course_admin.findById";

	@JoinTable(
            name = "partner_admin_permission",
            joinColumns = { @JoinColumn(
                    name = "partner_admin_id",
                    referencedColumnName = "id",
                    nullable = false) },
            inverseJoinColumns = { @JoinColumn(name = "permission_id",
                    referencedColumnName = "id", nullable = false) })
    @ManyToMany
    public List<UserPermission> userPermissionList;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    public User user;
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    @ManyToOne
    public Course course;
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    @ManyToOne
    public SecurityRole role;

    public CourseAdmin() {
    }

    public CourseAdmin(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    private static final Finder<Long, CourseAdmin> find = new Finder<Long, CourseAdmin>(Long.class,
            CourseAdmin.class);

    @Override
    public CourseAdmin getById(Long id) {
        return find.byId(id);
    }
}
