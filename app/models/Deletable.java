package models;

public interface Deletable {
    void setDeleted(boolean isDeleted);
    boolean isDeleted();
}
