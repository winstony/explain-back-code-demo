package scheduler;

import java.util.concurrent.TimeUnit;

import play.Play;
import play.libs.Akka;
import scala.concurrent.duration.Duration;
import scheduler.jobs.*;

public class SchedulerBase {
    public static void initSchedules() {
        if (false) {
            Akka.system().scheduler().schedule(
                    Duration.create(3, TimeUnit.HOURS),
                    Duration.create(1, TimeUnit.HOURS),
                    () -> new AddUuidsToQuestionsJob().run(),
                    Akka.system().dispatcher());
            Akka.system().scheduler().schedule(
                    Duration.create(4, TimeUnit.HOURS),
                    Duration.create(1, TimeUnit.HOURS),
                    () -> new InlineChoiceTextIntoChoice().run(),
                    Akka.system().dispatcher());
            Akka.system().scheduler().schedule(
                    Duration.create(1, TimeUnit.HOURS),
                    Duration.create(1, TimeUnit.DAYS),
                    () -> new CalculateChapterStatsJob().run(),
                    Akka.system().dispatcher());
        }
    }
}
