package scheduler.jobs;

import java.util.List;
import java.util.UUID;

import models.core.Choice;
import models.core.Question;

import com.avaje.ebean.Ebean;

import play.Logger;
import play.db.ebean.Model.Finder;

public class InlineChoiceTextIntoChoice implements Runnable {
    private static final Logger.ALogger log = Logger.of(InlineChoiceTextIntoChoice.class);

    @Override
    public void run() {
        log.info("Started");
        int rowsFound = 0;
        do {
            rowsFound = 0;
            Finder<Long, Choice> query =
                    new Finder<Long, Choice>(Long.class, Choice.class);
            List<Choice> choices = query.fetch("choiceText").where().isNull("text").setMaxRows(100).findList();
            for (Choice c : choices) {
                if (c.choiceText.text != null) {
                    rowsFound++;
                    log.info("Choice text found for " + c.id);
                } else {
                    break;
                }
                Choice update = new Choice();
                log.info("Choice text added for " + c.id);
                update.id = c.id;
                update.text = c.choiceText.text;
                Ebean.update(update);
            }
        } while (rowsFound > 0);
    }
}
