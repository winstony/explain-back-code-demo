package scheduler.jobs;

import java.util.Collections;
import java.util.Set;

import models.core.Chapter;
import models.partners.Course;
import dao.CourseDao;
import dao.QuestionDao;
import dao.SchoolDao;
import play.Logger;
import play.libs.F.Promise;
import service.ChapterStatsFactory;
import service.StatsService;
import to.api.QuestionShort;

public class CalculateChapterStatsJob implements Runnable {
    private static final Logger.ALogger log = Logger.of(CalculateChapterStatsJob.class);

    @Override
    public void run() {
        log.info("Started");
        ChapterStatsFactory progressService = new ChapterStatsFactory();
        long schoolId = SchoolDao.quizful().id;
        for (String courseAlias : new String[] {"java", "javascript", "htmlcss"}) {
            Course course = CourseDao.findByAlias(schoolId, courseAlias);
            if (course == null) {
                continue;
            }
            log.info("Started course " + course.alias);
            for (Chapter chapter : course.chapters) {
                QuestionDao.QueryBuilder params = new QuestionDao.QueryBuilder()
                .schoolId(schoolId)
                .chapterIds(Collections.singleton(chapter.id))
                .courseId(course.id);
                Set<QuestionShort> questions = QuestionDao.findQuestionsShort(params);
                log.info("  Started chapter " + chapter.alias);
                for (int userId = 1; userId < 1300; userId++) {
                    progressService.updateChapterStats(userId, course.id, chapter.id, questions);
                }
            }
        }
    }
}
