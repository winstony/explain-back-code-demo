package scheduler.jobs;

import java.util.UUID;

import com.avaje.ebean.Ebean;

import play.Logger;
import play.db.ebean.Model.Finder;
import models.core.Question;

public class AddUuidsToQuestionsJob implements Runnable {
    private static final Logger.ALogger log = Logger.of(AddUuidsToQuestionsJob.class);

    @Override
    public void run() {
        log.info("Started");
        Finder<Long, Question> query =
                new Finder<Long, Question>(Long.class, Question.class);
        query.where().isNull("uuid").findIds().forEach(id -> {
            Question q = new Question();
            q.id = (Long) id;
            q.uuid = UUID.randomUUID().toString().replace("-", "");
            log.info("Adding uuid to " + q.id);
            Ebean.update(q);
        });
    }

}
