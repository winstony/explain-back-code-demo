package scheduler.fixture;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.reactivecouchbase.CouchbaseBucket;
import org.reactivecouchbase.client.OpResult;
import org.reactivecouchbase.play.PlayCouchbase;

import play.Logger;
import play.api.Application;
import play.api.Play;
import play.libs.F.Promise;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;

import com.couchbase.client.protocol.views.DesignDocument;
import com.couchbase.client.protocol.views.SpatialViewDesign;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewDesign;

public class CouchbaseIndexDefinitionUpdater {
    private static final Logger.ALogger log = Logger.of(CouchbaseIndexDefinitionUpdater.class);

    private static final String COUCHBASE_BASE = "conf/couchbaseviews";
    private static final String MAP_JS = "map.js";
    private static final String REDUCE_JS = "reduce.js";
    private static final int TIMEOUT = 5000;

    private final Map<String, List<DesignDocument>> bucketDefinitions = new HashMap<>();
    private final ExecutionContext defaultContext;
    private final Application application;

    public CouchbaseIndexDefinitionUpdater() {
        defaultContext = play.api.libs.concurrent.Execution.defaultContext();
        application = Play.current();
    }

    public void initializeViews() {
        try {
            readBuckets(COUCHBASE_BASE);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        synchronizeWithCouchbase();

    }

    private void synchronizeWithCouchbase() {
        for (String bucketAlias : bucketDefinitions.keySet()) {
            log.info("Synchronizing bucket [" + bucketAlias + "]");
            CouchbaseBucket bucket = PlayCouchbase.bucket(bucketAlias, application);
            List<DesignDocument> documents = bucketDefinitions.get(bucketAlias);
            for (DesignDocument document : documents) {
                log.info("Synchronizing Design Document { " + document.getName() + " }");
                synchronizeDesignDocument(bucket, document);
            }
        }
    }

    private DesignDocument synchronizeDesignDocument(CouchbaseBucket bucket, DesignDocument definition) {
        return Promise.wrap(bucket.designDocument(definition.getName(), defaultContext)).map(doc -> {
            if (doc == null) {
                log.warn("Creating Design Document { " + definition.getName() + " } from scratch");
                return createDocument(bucket, definition);
            } else if (containsAll(doc, definition.getViews())) {
                log.info("Design Document { " + doc.getName() + " } already has all views");
                return doc;
            } else {
                log.warn("Populating Design Document { " + doc.getName() + " } with new views");
                return createDocument(bucket, combineViews(doc, definition));
            }
        }).get(TIMEOUT);
    }

    private DesignDocument combineViews(DesignDocument existing, DesignDocument newDefinitions) {
        Map<String, ViewDesign> views = new HashMap<>();
        for (ViewDesign existingView : existing.getViews()) {
            views.put(existingView.getName(), existingView);
        }
        for (ViewDesign newView : newDefinitions.getViews()) {
            views.put(newView.getName(), newView);
        }
        return new DesignDocument(newDefinitions.getName(), new ArrayList<>(views.values()), Collections.emptyList());
    }

    private boolean containsAll(DesignDocument doc, List<ViewDesign> views) {
        for (ViewDesign v : views) {
            boolean containsThisView = doc.getViews().stream()
                    .anyMatch(old -> Objects.equals(old.getName(), v.getName())
                            && Objects.equals(old.getMap(), v.getMap())
                            && Objects.equals(old.getReduce(), v.getReduce()));
            if (!containsThisView) {
                return false;
            }
        }
        return true;
    }

    private DesignDocument createDocument(CouchbaseBucket bucket, DesignDocument document) {
        OpResult opResult = Promise.wrap(bucket.createDesignDoc(document, defaultContext)).get(TIMEOUT);
        if (opResult.isSuccess()) {
            log.debug("Design Document { " + document.getName() + " } successfully created ");
            return Promise.wrap(bucket.designDocument(document.getName(), defaultContext)).get(TIMEOUT);
        } else {
            throw new RuntimeException(opResult.getMessage());
        }
    }

    private void readBuckets(String bucketsBase) throws IOException {
        for (Path bucketDir : Files.list(Paths.get(bucketsBase)).collect(Collectors.toList())) {
            String bucketAlias = bucketDir.getFileName().toString();
            log.debug("Found bucket [" + bucketAlias + "]");
            bucketDefinitions.put(bucketAlias, readDesignDocuments(bucketDir));
        }
    }

    private List<DesignDocument> readDesignDocuments(Path bucketDir) throws IOException {
        List<DesignDocument> disignDocuments = new ArrayList<>();
        for (Path designDocDir : Files.list(bucketDir).collect(Collectors.toList())) {
            String designDocName = designDocDir.getFileName().toString();
            log.debug("Found Design Document { " + designDocName + " }");
            disignDocuments.add(new DesignDocument(designDocName, readViews(designDocDir), readSpatial(designDocDir)));
        }
        return disignDocuments;
    }

    private List<SpatialViewDesign> readSpatial(Path designDocDir) {
        return Collections.emptyList();
    }

    private List<ViewDesign> readViews(Path disignDocDir) throws IOException {
        List<ViewDesign> views = new ArrayList<>();
        for (Path viewDir : Files.list(disignDocDir).collect(Collectors.toList())) {
            String viewName = viewDir.getFileName().toString();
            String map = readMap(viewDir);
            if (viewDir.resolve(REDUCE_JS).toFile().exists()) {
                String reduce = readReduce(viewDir);
                log.debug("Found View < " + viewName + " > (map, reduce)");
                views.add(new ViewDesign(viewName, map, reduce));
            } else {
                log.debug("Found View < " + viewName + " > (map)");
                views.add(new ViewDesign(viewName, map));
            }
        }
        return views;
    }

    private String readReduce(Path viewDir) throws IOException {
        return new String(Files.readAllBytes(viewDir.resolve(REDUCE_JS)), StandardCharsets.UTF_8);
    }

    private String readMap(Path viewDir) throws IOException {
        return new String(Files.readAllBytes(viewDir.resolve(MAP_JS)), StandardCharsets.UTF_8);
    }
}
