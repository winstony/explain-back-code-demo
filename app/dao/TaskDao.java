package dao;

import models.partners.Task;
import play.db.ebean.Model;

public class TaskDao {

    /**
     * Generic query helper for entity Task with id Long
     */
    public static final Model.Finder<Long, Task> find = new Model.Finder<Long, Task>(Long.class, Task.class);

    public static Task findById(Long id) {
        return find.where().eq("id", id).where().eq("deleted", false).findUnique();
    }
}
