package dao;

import models.partners.School;
import play.cache.Cache;
import play.db.ebean.Model;

public class SchoolDao {
    private static final Model.Finder<Long, School> find = new Model.Finder<Long, School>(Long.class, School.class);

    public static School findById(Long id) {
        if (id == null) {
            return null;
        }
        School cached = (School) play.cache.Cache.get("school_id::" + id);
        if (cached != null) {
            return cached;
        }
        School school = find.where()
                .eq("id", id)
                .eq("deleted", false)
                .findUnique();
        Cache.set("school_id::" + id, school, 60 * 60);
        return school;
    }

    public static School findByAlias(String alias) {
        if (alias == null) {
            return null;
        }
        School cached = (School) play.cache.Cache.get("school::" + alias);
        if (cached != null) {
            return cached;
        }
        School school = find.where()
                .eq("alias", alias)
                .eq("deleted", false)
                .findUnique();
        Cache.set("school::" + alias, school, 60 * 60);
        return school;
    }

    public static School explain() {
        return findByAlias("explain");
    }

    public static School quizful() {
        return findByAlias("quizful");
    }

}
