package dao;

import com.avaje.ebean.*;

import models.core.Tag;
import models.core.Topic;
import models.core.aggregators.ValuableTag;
import play.Logger;
import play.db.ebean.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TagDao {
    private static final Logger.ALogger log = Logger.of(TagDao.class);

    /** Generic query helper for entity Tag with id Long */
    private static final Model.Finder<Long, Tag> find = new Model.Finder<Long, Tag>(Long.class, Tag.class);

    public static List<Tag> findAll() {
        log.debug("FIND ALL TAGS");
        return find.select("*")
                .fetch("harderTags", new FetchConfig().query())
                .fetch("easierTags", new FetchConfig().query())
                .fetch("topic")
                .where()
                .eq("deleted","false")
                .eq("harderTags.deleted","false")
                .eq("easierTags.deleted","false")
                .findList();
    }

    public static Tag findByName(String filter) {
        return find.where().eq("deleted","false").eq("name", filter).findUnique();
    }

    public static Tag findById(Long id) {
        return find.select("*")
                .fetch("easierTags")
                .fetch("harderTags")
                .where()
                .eq("deleted","false")
                .eq("id", id).findUnique();
    }

    public static List<Tag> findByRuleIds(List<Long> ruleIds) {
        List<Tag> tags = null;
        if (ruleIds != null && !ruleIds.isEmpty()) {
            tags = find.where().eq("deleted","false").in("rules.id", ruleIds).findList();
        }
        return tags != null ? tags : new ArrayList<Tag>();
    }

    public static List<ValuableTag> getValuableTags() {
        List<ValuableTag> result;
        String sql = " select t.id, t.name, t.topic_id, t.activated, t.deleted, count(q.id) as questionsCount "
                + " from tags t"
                + " join testrule_tag tt on t.id = tt.tags_id "
                + " join testrule tr on tr.id = tt.testrule_id "
                + " join question_testrule qt on tr.id = qt.rules_id "
                + " join question q on q.id = qt.question_id "
                + " where t.deleted=false AND q.ready=1 AND q.deleted=false"
                + " group by t.id";
        RawSql rawSql = RawSqlBuilder.parse(sql)
                // map result columns to bean properties
                .columnMapping("t.id", "tag.id")
                .columnMapping("t.name", "tag.name")
                .columnMapping("t.activated", "tag.activated")
                .columnMapping("t.deleted", "tag.deleted")
                .columnMapping("t.topic_id", "tag.topic.id")
                .create();
        Query<ValuableTag> query = Ebean.find(ValuableTag.class);
        result = query.setRawSql(rawSql).findList();
        return  result != null ? result : new ArrayList<ValuableTag>();
    }

    public static List<Tag> findOnlyValuable(Topic topic) {
        return getValuableTags().stream()
                .filter(ValuableTag::getIsValuable)
                .map(tag -> tag.getTag())
                .filter(tag -> topic == null || Objects.equals(tag.topic, topic))
                .collect(Collectors.toList());
    }
}
