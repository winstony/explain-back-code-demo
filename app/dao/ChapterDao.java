package dao;

import java.util.Collection;

import com.avaje.ebean.ExpressionList;

import models.core.Chapter;
import models.partners.Course;
import play.cache.Cache;
import play.db.ebean.Model;

public class ChapterDao {
    private static final Model.Finder<Long, Chapter> chapterQuery = new Model.Finder<>(Long.class, Chapter.class);

    @Deprecated
    public static Chapter findById(Long schoolId, String courseAlias, String alias) {
        Chapter findUnique = chapterQuery.where()
                .eq("deleted", false)
                .eq("alias", alias)
                .eq("course.alias", courseAlias)
                .eq("course.schoolId", schoolId)
                .findUnique();
        return findUnique;
    }

    private static final int EXPIRATION_IN_SECONDS = 60 * 60;

    public static Chapter find(FindChapterParams params) {
        Object cached = Cache.get("chapter_" + params.toString());
        if (cached != null) {
            return (Chapter) cached;
        }
        ExpressionList<Chapter> query = chapterQuery.fetch("course")
                .fetch("course.school")
                .where()
                .eq("deleted", false);

        if (params.schoolAlias != null) {
            query.eq("course.school.alias", params.schoolAlias);
        }
        if (params.courseAlias != null) {
            query.eq("course.alias", params.courseAlias);
        }
        if (params.chapterAlias != null) {
            query.eq("alias", params.chapterAlias);
        }
        if (params.schoolId != null) {
            query.eq("course.schoolId", params.schoolId);
        }
        if (params.courseId != null) {
            query.eq("courseId", params.courseId);
        }
        if (params.chapterId != null) {
            query.eq("id", params.chapterId);
        }
        Chapter c = query.findUnique();
        Cache.set("chapter_" + params.toString(), c, EXPIRATION_IN_SECONDS);
        return c;
    }

    public static class FindChapterParams {
        @Override
        public String toString() {
            return "FindChapterParams [schoolAlias=" + schoolAlias
                    + ", schoolId=" + schoolId
                    + ", courseAlias=" + courseAlias
                    + ", courseId=" + courseId
                    + ", chapterAlias=" + chapterAlias
                    + ", chapterId=" + chapterId + "]";
        }
        public String schoolAlias;
        public Long schoolId;
        public String courseAlias;
        public Long courseId;
        public String chapterAlias;
        public Long chapterId;

    }
}
