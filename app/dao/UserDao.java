package dao;

import models.core.User;
import models.partners.School;
import play.db.ebean.Model;

import java.util.List;

public class UserDao {
    private static Model.Finder<Long, User> find = new Model.Finder<Long, User>(Long.class, User.class);

    public static List<User> findUsers(String schoolAlias, Long currentUserId, String username) {
        School school = SchoolDao.findByAlias(schoolAlias);
        return find.where()
                .eq("schoolId", school.id)
                .eq("deleted", false)
                .isNotNull("username")
                .ieq("username", username)
                .ne("id", currentUserId)
                .findList();
    }

    public static List<User> findUsersByIds(List<Long> userIds) {
        return find.where()
                .in("id", userIds)
                .eq("deleted", false)
                .findList();
    }

    public static User findByUsername(String username) {
        return find.where()
                .eq("username", username)
                .findUnique();
    }
}
