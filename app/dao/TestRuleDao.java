package dao;

import com.avaje.ebean.FetchConfig;
import com.avaje.ebean.Query;
import com.google.common.collect.Maps;
import models.Status;
import models.core.Tag;
import models.core.TestRule;
import play.db.ebean.Model.Finder;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class TestRuleDao {
    private static Finder<Long, TestRule> find = new Finder<Long, TestRule>(Long.class, TestRule.class);

    public Set<TestRule> findRules(Collection<Long> ids) {
        Set<TestRule> rules = find.fetch("tags", "id").where().in("id", ids).findSet();
        fillWithTags(rules);
        return rules;
    }

    private void fillWithTags(Set<TestRule> rules) {
        List<Tag> valuable = TagDao.findOnlyValuable(null);
        Map<Long, Tag> idxTagsById = Maps.uniqueIndex(valuable, tag -> tag.id);
        rules.forEach(rule -> {
            List<Tag> tags = rule.tags.stream()
                    .map(t -> idxTagsById.get(t.id))
                    .filter(t -> t != null)
                    .collect(Collectors.toList());
            rule.tags = tags;
        });
    }

    public Set<TestRule> findContainingTags(Collection<Long> tagIds) {
        Set<TestRule> rules = find.fetch("tags", "id").where().in("tags.id", tagIds).findSet();
        fillWithTags(rules);
        return rules;
    }

    public static List<TestRule> getSchoolRules(String topicAlias, Long schoolId, int size, int page) {
        List<Long> schoolTopicsIds = TopicDao.getSchoolTopicsIds(schoolId);

        Query<TestRule> rulesQuery  =  find
                .fetch("topic")
                .fetch("translations", new FetchConfig().query())
                .fetch("tags", new FetchConfig().query())
                .fetch("tags.translations")
                .where()
                .in("topic.id", schoolTopicsIds)
                .eq("deleted", false)
                .eq("status", Status.APPROVED)
                .setFirstRow(page * size)
                .setMaxRows(size);

        if (topicAlias != null && !topicAlias.isEmpty()) {
            return rulesQuery.where().eq("topic.alias", topicAlias).findList();
        }
        return rulesQuery.findList();
    }
}
