package dao;

import models.partners.Course;
import play.cache.Cache;
import play.db.ebean.Model;

import java.util.List;
import java.util.stream.Collectors;

public class CourseDao {
    private static final Model.Finder<Long, Course> coursesQuery = new Model.Finder<>(Long.class, Course.class);

    public static Course getById(Long id) {
        return coursesQuery.setId(id).where()
                .eq("deleted", false)
                .eq("activated", true)
                .findUnique();
    }

    public static List<Course> getUserCourses(Long userId) {
        return coursesQuery.where()
                .eq("deleted", false)
                .eq("activated", true)
                .contains("userList.id", userId.toString())
                .findList();
    }

    public static List<Course> getAvailableCoursesList() {
        return coursesQuery.where()
                .eq("activated", true)
                .eq("school.activated", true)
                .isNotNull("topic_id")
                .isNotNull("school_id")
                .eq("deleted", false)
                .eq("topic.deleted", false)
                .eq("school.deleted", false)
                .findList();
    }

    public static Course findByAlias(Long school, String alias) {
        Course course = coursesQuery.fetch("chapters")
                .where()
                .eq("deleted", false)
                .eq("alias", alias)
                .eq("schoolId", school)
                .findUnique();
        if (course != null) {
            course.chapters = course.chapters.stream()
                    .filter(c -> !c.deleted && c.activated)
                    .collect(Collectors.toList());
        }
        return course;
    }

    public static List<Course> getSchoolCourses(Long schoolId) {
        return coursesQuery.where()
                .eq("deleted", false)
                .eq("schoolId", schoolId)
                .eq("activated", true)
                .findList();
    }

    public static Course getCourse(GetCourseParams params) {
        Object cached = Cache.get("course_" + params.toString());
        if (cached != null) {
            return (Course) cached;
        }
        Course c = coursesQuery.where()
                .eq("deleted", false)
                .eq("alias", params.courseAlias)
                .eq("schoolId", params.schoolId)
                .eq("activated", true)
                .findUnique();
        Cache.set("course_" + params.toString(), c, 60 * 60);
        return c;
    }

    public static class GetCourseParams {
        public final Long schoolId;
        public String courseAlias;

        public GetCourseParams(long schoolId) {
            this.schoolId = schoolId;
        }

        @Override
        public String toString() {
            return "GetCourseParams [schoolId=" + schoolId
                    + ", courseAlias=" + courseAlias + "]";
        }

    }
}
