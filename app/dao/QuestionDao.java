package dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import models.core.Question;
import models.core.Tag;
import models.core.TestRule;
import models.core.User;
import play.Logger;
import play.db.ebean.Model;
import to.api.QuestionShort;
import to.api.convert.ApiConverter;
import utils.ServiceUtils;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.FetchConfig;
import com.avaje.ebean.Query;
import com.google.common.collect.Lists;

public class QuestionDao {

    static final Logger.ALogger log = Logger.of(QuestionDao.class);

    /** Generic query helper for entity Question with id Long */
    static Model.Finder<Long, Question> findQuestion =
            new Model.Finder<Long, Question>(Long.class, Question.class);

    @Deprecated
    public static List<Tag> getTags(Question question) {
        if (!question.isDeleted()) {
            if (question.getQtags() == null) {
                List<Tag> tagsFromRules = Lists.newArrayList();
                for (TestRule rule : question.rules) {
                    if (!rule.isDeleted() && rule.tags != null) {
                        for(Tag tag : rule.tags)
                            if (!tag.isDeleted()) {
                                tagsFromRules.add(tag);
                            }
                    }
                }
                if (!tagsFromRules.isEmpty()) {
                    question.setQtags(tagsFromRules);
                }
            }
            return question.getQtags();
        }
        log.error("Invocation getTags() on deleted question id=" + question.id);
        throw new IllegalArgumentException("Invocation getTags() on deleted question id=" + question.id);
    }

    public static Collection<Question> getTaggedQuestions(Collection<Tag> tags) {
        log.debug("getTaggedQuestions(...) " + tags);
        Set<Question> questions = null;
        if (tags != null && !tags.isEmpty()) {
            questions = findQuestion
                    .fetch("choices")
                    .fetch("choices.choiceText")
                    .where().eq("deleted", false)
                    .where().eq("ready", true)
                    .where().eq("rules.deleted", false)
                    .where().eq("rules.tags.deleted", false)
                    .where().in("rules.tags", tags)
                    .findSet();
        }
        log.debug(" questions with tags " + questions);
        return questions != null ? questions : new HashSet<Question>();
    }

    public static Collection<Question> getQuestionsFromUserCourses(Collection<Question> questionsToAsk, User user) {
        log.debug("Getting questions from user courses");
        return questionsToAsk.stream()
                .filter(question -> !question.course.deleted && question.course.userList.contains(user))
                .collect(Collectors.toSet());
    }

    public static Question getRandom() {
        log.debug("Getting random question");
        List<Object> ids = findQuestion.where("ready = true").where().eq("deleted", false).findIds();
        if (ids.isEmpty()) {
            return null;
        }
        int index = Math.abs(new Random(System.nanoTime()).nextInt()) % ids.size();
        return findById((Long) ids.get(index));
    }

    public static Question getRandomFromUserCourses(User user) {
        log.debug("Getting random question from user courses or from all courses if user didn't select courses");
        List<Object> ids = findQuestion
                .where()
                .eq("ready", true)
                .eq("deleted", false)
                .contains("course.userList.id", user.id.toString())
                .findIds();
        if (ids.isEmpty()) {
            List<Object> idsAll = findQuestion
                    .where()
                    .eq("ready", true)
                    .eq("deleted", false)
                    .findIds();
            if (idsAll.isEmpty())
                return null;
            int index = Math.abs(new Random(System.nanoTime()).nextInt()) % idsAll.size();
            return findById((Long) idsAll.get(index));
        }
        int index = Math.abs(new Random(System.nanoTime()).nextInt()) % ids.size();
        return findById((Long) ids.get(index));
    }

    public static String getRandomOpenQuuidFromCourse(String courseAlias) {
        List<Object> ids = findQuestion
                .where()
                .isNotNull("courseId")
                .eq("deleted", false)
                .eq("ready", true)
                .eq("free", true)
                .eq("open", true)
                .eq("course.schoolId", ServiceUtils.getSchoolId())
                .eq("course.alias", courseAlias)
                .eq("course.deleted", false)
                .eq("course.activated", true)
                .findIds();
        if (ids.isEmpty()) {
            return null;
        }
        int index = new Random().nextInt(ids.size());
        Long questionId = (Long) ids.get(index);
        Question question =  findQuestion.setId(questionId).findUnique();
        return question.uuid;
    }

    @Deprecated
    public static Question findById(Long id) {
        //TODO: use option
        Question question = findQuestion.select("*")
                .fetch("choices")
                .fetch("choices.choiceText")
                .setId(id)
                .where().eq("deleted", false)
                .findUnique();
        if (question != null) question.postLoad();
        return question;
    }

    @Deprecated
    public static List<Question> findByIds(List<Long> ids) {
        log.debug("---- findByIds ----" + ids);
        List<Question> questions = null;
        if (ids != null && !ids.isEmpty()) {
            questions = findQuestion.where().eq("deleted", false).where().in("id", ids).findList();
        }
        return questions != null ? questions : new ArrayList<Question>();
    }

    @Deprecated
    public static List<Question> getQuestionsByRuleIds(List<Long> ruleIds) {
        if (ruleIds != null && !ruleIds.isEmpty()) {
            return  findQuestion
                    .where().eq("deleted", false)
                    .where().eq("rules.deleted", false)
                    .where().in("rules.id", ruleIds)
                    .findList();
        }
        return new ArrayList<Question>();
    }

    public static List<Question> filterByChapterAndTags(Long chapterId, List<Long> tagIds) {
        ExpressionList<Question> query =
                findQuestion
                .where().eq("deleted", false)
                .where().eq("rules.deleted", false)
                .where().eq("rules.tags.deleted", false)
                .where().in("rules.tags.id", tagIds);
        log.debug("query : "  + query.query().getGeneratedSql());
        if (tagIds != null && !tagIds.isEmpty()) {
            return query.findList();
        }
        return null;
    }

    /**
     * Get all not deleted questions added to specified task
     * @param taskId
     * @return
     */
    public static List<Question> getFromTask(Long taskId) {
        return findQuestion.fetch("taskList")
                .where().eq("deleted", false)
                .where().eq("taskList.deleted", false)
                .where().eq("taskList.id", taskId)
                .findList();
    }

    public List<Question> findQuestions(Set<Long> questionIds) {
        return findQuestion.fetch("rules").where().in("id", questionIds).findList();
    }

    public static List<Question> getCourseQuestions(Long schoolId, Long courseId, List<Long> chapterIds) {
        QueryBuilder params = new QueryBuilder()
            .schoolId(schoolId)
            .courseId(courseId)
            .chapterIds(new HashSet<>(chapterIds));
        return getQuestions(params);
    }

    public static Optional<to.api.Question> getQuestion(QueryBuilder params) {
        return Optional.ofNullable(makeQuery(params).findUnique()).map(question -> ApiConverter.to(question));
    }

    public static List<Question> getQuestions(QueryBuilder params) {
        return getQuestionsInternal(params);
    }

    public static Set<QuestionShort> findQuestionsShort(QueryBuilder params) {
        Set<QuestionShort> ids = getQuestionsInternal(params.withoutChoices().withoutRules())
                .stream()
                .map(q -> ApiConverter.toQuestionShort(q))
                .collect(Collectors.toSet());
        return ids;
    }

    private static List<Question> getQuestionsInternal(QueryBuilder params) {
        return makeQuery(params).findList();
    }

    private static ExpressionList<Question> makeQuery(QueryBuilder params) {
        Query<Question> query = findQuestion
                .fetch("translations")
                .fetch("course")
                .fetch("chapter")
                .fetch("course.school");
        if (params.withChoices) {
            query.fetch("choices", new FetchConfig().query());
            if (params.withTranslations) {
                    query.fetch("choices.translations");
            }
        }
        if (params.withRules) {
            query.fetch("rules", new FetchConfig().query());
            if (params.withTranslations) {
                query.fetch("rules.translations");
            }
        }
        ExpressionList<Question> criteria = query.where()
                .isNotNull("chapterId")
                .isNotNull("courseId")
                .eq("ready", true)
                .eq("deleted", false)
                .eq("chapter.deleted", false)
                .eq("chapter.activated", true)
                .eq("course.deleted", false);
        withParams(params, criteria);
        return criteria;
    }

    public static Optional<Long> findIdByUuid(String uuid) {
        QueryBuilder params = new QueryBuilder().uuids(Collections.singleton(uuid));
        ExpressionList<Question> query = findQuestion.where()
                .eq("ready", true)
                .eq("deleted", false)
                .eq("chapter.deleted", false)
                .eq("course.deleted", false);
        withParams(params, query);
        return Optional.ofNullable(query.findUnique()).map(q -> q.id);
    }

    public static class QueryBuilder {

        // TODO maybe add public String schoolAlias;
        private Long schoolId;
        private Long courseId;
        private Set<Long> chapterIds;
        private String courseAlias;
        private String chapterAlias;
        private Boolean subscribed;
        private Set<String> uuids;
        private Set<Long> ids;

        private boolean withChoices = true;
        private boolean withTranslations = true;
        private boolean withRules = true;

        public QueryBuilder ids(Set<Long> ids) {
            this.ids = ids;
            return this;
        }

        public QueryBuilder ids(long id, long... ids) {
            return ids(Stream.concat(Stream.of(id), Arrays.stream(ids).boxed()).collect(Collectors.toSet()));
        }

        public QueryBuilder schoolId(Long schoolId) {
            this.schoolId = schoolId;
            return this;
        }

        public QueryBuilder courseId(Long courseId) {
            this.courseId = courseId;
            return this;
        }

        public QueryBuilder chapterIds(Set<Long> chapterIds) {
            this.chapterIds = chapterIds;
            return this;
        }

        public QueryBuilder courseAlias(String courseAlias) {
            this.courseAlias = courseAlias;
            return this;
        }

        public QueryBuilder chapterAlias(String chapterAlias) {
            this.chapterAlias = chapterAlias;
            return this;
        }

        public QueryBuilder subscribed(Boolean subscribed) {
            this.subscribed = subscribed;
            return this;
        }

        public QueryBuilder uuids(Set<String> uuids) {
            this.uuids = uuids;
            return this;
        }

        /* This does not work because ebean still lazily fetches objects afterwards */
        private QueryBuilder withoutChoices() {
            this.withChoices = false;
            return this;
        }

        private QueryBuilder withoutTranslations() {
            this.withTranslations = false;
            return this;
        }

        private QueryBuilder withoutRules() {
            this.withRules = false;
            return this;
        }
    }

    private static void withParams(QueryBuilder params, ExpressionList<Question> query) {
        if (params.ids != null) {
            query.in("id", params.ids);
        }
        if (params.schoolId != null) {
            query.eq("course.schoolId", params.schoolId);
        }
        if (params.courseId != null) {
            query.eq("courseId", params.courseId);
        }
        if (params.courseAlias != null) {
            query.eq("course.alias", params.courseAlias);
        }
        if (params.chapterIds != null) {
            query.in("chapterId", params.chapterIds);
        }
        if (params.chapterAlias != null) {
            query.eq("chapter.alias", params.chapterAlias);
        }
        if (params.uuids != null) {
            query.in("uuid", params.uuids);
        }
    }
}
