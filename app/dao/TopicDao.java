package dao;

import models.core.Topic;
import play.db.ebean.Model;

import java.util.List;
import java.util.stream.Collectors;

public class TopicDao {
    private static Model.Finder<Long, Topic> find = new Model.Finder<Long, Topic>(Long.class, Topic.class);

    public Topic findByName(String name) {
        return find.where().eq("title", name).eq("deleted", false).findUnique();
    }

    public static List<Long> getSchoolTopicsIds(Long schoolId) {
        return CourseDao.getSchoolCourses(schoolId)
                .stream()
                .map(c -> c.topic.id)
                .collect(Collectors.toList());
    }

    public static List<Topic> getSchoolTopics(Long schoolId) {
        List<Long> schoolTopicsIds = getSchoolTopicsIds(schoolId);
        return find
                .where()
                .in("id", schoolTopicsIds)
                .eq("activated", true)
                .eq("deleted", false)
                .findList();
    }
}
