package dao.couchbase;

public class JavaOpResult {
    private String message;
    boolean success;

    public JavaOpResult(Boolean ok, String message) {
        this.message = message;
        this.success = ok;
    }

    public String message() {
        return message;
    }

    public boolean success() {
        return success;
    }
}
