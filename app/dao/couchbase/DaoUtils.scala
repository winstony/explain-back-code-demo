package dao.couchbase

import play.api.libs.json.Json
import play.libs.{ Json => JavaJson }
import com.fasterxml.jackson.databind.JsonNode
import play.api.libs.json._
import java.util.UUID
import play.api.Logger
import com.couchbase.client.vbucket.config.Bucket
import org.reactivecouchbase.CouchbaseBucket
import scala.concurrent.Future
import java.util.Optional
import org.reactivecouchbase.play.PlayCouchbase
import org.reactivecouchbase.client.OpResult
import java.io.IOException
import com.couchbase.client.protocol.views.Query
import java.net.URLDecoder
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.couchbase.client.protocol.views.Stale

private[couchbase] object DaoUtils {
  val log = Logger(this.getClass)

  implicit val executionContext = play.api.libs.concurrent.Execution.Implicits.defaultContext
  implicit val app = play.api.Play.current

  def generateUuid() = UUID.randomUUID().toString().replace("-", "")

  def decode(query: Query): String = {
    URLDecoder.decode(query.toString().replace("+", "%2B"), "UTF-8").replace("%2B", "+")
  }

  // TODO in play 2.4 no need to do it, JsonNode has own serializer
  def toJson(value: Object): JsValue = {
    val string = JavaJson.stringify(JavaJson.toJson(value))
    log.debug("json to save -> " + string)
    Json.parse(string)
  }

  def fromJson[A](value: JsValue, clazz: java.lang.Class[A]) = {
    val string = value.toString()
    log.debug("json retrieved -> " + string)
    JavaJson.fromJson[A](JavaJson.parse(string), clazz)
  }

  def deleteDocument(bucket: CouchbaseBucket, key: String): Future[OpResult] = {
    bucket.delete(key)
  }

  // useful for tests
  def deleteDocumentQuietly(bucketAlias: String, key: String): Future[JavaOpResult] = {
    deleteDocument(PlayCouchbase.bucket(bucketAlias), key).map(x => new JavaOpResult(x.ok, x.getMessage))
  }

  def getDocumentJson(bucket: CouchbaseBucket, key: String): Future[Optional[JsonNode]] = {
    getDocument(bucket, key, classOf[JsonNode])
  }

  def getDocument[A](bucket: CouchbaseBucket, key: String, clazz: java.lang.Class[A]): Future[Optional[A]] = {
    val futureObject: Future[Option[JsValue]] = bucket.get[JsValue](key);
    futureObject.map(maybe => {
      if (maybe.isDefined) {
        Optional.of(fromJson(maybe.get, clazz))
      } else {
        Optional.empty()
      }
    })
  }

  def setDocument[T <: AnyRef](
    bucket: CouchbaseBucket,
    key: String,
    value: T,
    clazz: java.lang.Class[T],
    errorMessage: String): Future[T] = {
    bucket.set(key, toJson(value)).map(opResult => {
      if (opResult.ok) {
        fromJson(opResult.document.get, clazz)
      } else {
        log.error(errorMessage + " " + opResult.msg)
        throw new IOException(opResult.msg.getOrElse(errorMessage))
      }
    })
  }

  def add[T <: AnyRef](
    bucket: CouchbaseBucket,
    key: String,
    value: T,
    clazz: java.lang.Class[T],
    errorMessage: String): Future[T] = {
    bucket.add(key, toJson(value)).map(opResult => {
      if (opResult.ok) {
        fromJson(opResult.document.get, clazz)
      } else {
        throw new IOException(opResult.msg.getOrElse(errorMessage))
      }
    })
  }

  def findDocuments[T <: AnyRef](
      bucket: CouchbaseBucket,
      designDoc: String,
      view: String,
      query: Query,
      clazz: java.lang.Class[T]): Future[java.util.List[T]] = {
    query.setReduce(false).setIncludeDocs(true)
    if (query.getArgs.get("stale") == null) {
      log.debug("set stale to false")
      query.setStale(Stale.FALSE)
    }
    log.debug("query list of documents" + designDoc + " " + view + " " + decode(query))
    bucket.search[JsValue](designDoc, view)(query).toList.map { list =>
      list.map(row => fromJson[T](row.document, clazz)).asJava
    }
  }

}
