package dao.couchbase

import play.api.Logger
import org.reactivecouchbase.play.PlayCouchbase
import play.api.libs.json._
import play.api.libs.json.JsObject
import scala.concurrent.Future
import java.util.List
import java.util.Map
import java.util.UUID
import play.api.libs.json.Json
import play.libs.{ Json => JavaJson }
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import org.reactivecouchbase.client.OpResult
import java.lang.Long
import play.api.Logger
import com.fasterxml.jackson.databind.JsonNode
import java.util.Optional
import com.couchbase.client.protocol.views.View
import com.couchbase.client.protocol.views.Query
import com.couchbase.client.protocol.views.ComplexKey
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.couchbase.client.protocol.views.Stale
import org.reactivecouchbase.client.TypedRow
import org.reactivecouchbase.client.Row
import org.reactivecouchbase.client.RawRow
import java.io.IOException
import dao.couchbase.DaoUtils._
import CouchbaseNotificationDao._
import to.db.notification.Notification
import to.api.NotificationsCount
import to.db.notification.NotificationType
import to.db.notification.AchievementNotification

object CouchbaseNotificationDao {
  val DesignDocNotification = "notification"
  val ViewCommentByUserDate = "comment_by_user_date"
  val ViewTimelineByUserDate = "timeline_by_user_date"
  val NOTIFICATIONS_BUCKET_ALIAS = "notifications"
}

class CouchbaseNotificationDao {
  private val log = Logger(this.getClass)
  private val bucket = PlayCouchbase.bucket(NOTIFICATIONS_BUCKET_ALIAS)

  def save(notification: Notification): Future[Notification] = {
    if (notification.id != null) {
      save(notification, notification.id)
    } else {
      save(notification, generateUuid())
    }
  }

  private def save(notification: Notification, id: String): Future[Notification] = {
    val futereNewRow = bucket.add(id, toJson(notification)).map(opResult => {
      if (opResult.ok) {
        val savedNotification = fromJson(opResult.document.get, classOf[Notification])
        savedNotification.id = id
        savedNotification
      } else {
        log.error("Could not save notification" + opResult.msg)
        throw new IOException(opResult.msg.getOrElse("Could not save notification"))
      }
    })
    futereNewRow.onFailure { case error => log.error("Could not save notification", error) }
    futereNewRow
  }

  def update(notification: Notification): Future[Notification] = {
    val futereNewRow: Future[Notification] =
      bucket.replace(notification.id, toJson(notification)).map(opResult => {
        if (opResult.ok) {
          // id should have already be there
          fromJson(opResult.document.get, classOf[Notification])
        } else {
          log.error("Could not update notification" + opResult.msg)
          throw new IOException(opResult.msg.getOrElse("Could not update notification"))
        }
      })
    futereNewRow.onFailure { case error => log.error("Could not update notification", error) }
    futereNewRow
  }

  def getNotification[A <: Notification](notificationId: String, clazz: java.lang.Class[A]): Future[Optional[A]] = {
    DaoUtils.getDocument[A](bucket, notificationId, clazz).map { maybeNotification =>
      if (maybeNotification.isPresent()) {
        val notification = maybeNotification.get()
        notification.id = notificationId
        Optional.of(notification)
      } else {
        Optional.empty()
      }
    }

  }

  private def parseNotification(notification: Option[JsObject]): Optional[JsonNode] = {
    if (notification.isDefined) {
      Optional.of(JavaJson.parse(notification.get.toString()))
    } else {
      Optional.empty()
    }
  }

  def findCommentNotifications(userId: Long, before: Long, limit: Int): Future[List[Notification]] = {
    findLatestNotifications(userId, before, limit, ViewCommentByUserDate)
  }

  def findTimelineNotifications(userId: Long, before: Long, limit: Int): Future[List[Notification]] = {
    findLatestNotifications(userId, before, limit, ViewTimelineByUserDate)
  }

  private def findLatestNotifications(userId: Long, before: Long, limit: Int, view: String) = {
    val query = queryByUserDate(userId, before, limit);
    log.debug("query " + decode(query))
    val futureTimelineNotifications: Future[List[TypedRow[JsObject]]] =
      bucket.search[JsObject](DesignDocNotification, view)(query).toList.map(_.asJava)
    futureTimelineNotifications.map(parseCommentNotifications(_))
  }

  private def queryByUserDate(userId: Long, before: Long, limit: Int): Query = {
    new Query()
      .setIncludeDocs(true)
      .setRangeEnd(ComplexKey.of(userId).forceArray(true))
      .setStale(Stale.FALSE)
      .setLimit(limit)
      .setDescending(true)
      .setReduce(false)
      .setRangeStart(ComplexKey.of(userId, before).forceArray(true))
  }

  def parseCommentNotifications(list: List[TypedRow[JsObject]]): List[Notification] = {
    list.map(row => {
      val notification = fromJson(row.document, classOf[Notification])
      notification.id = row.id.get;
      notification
    })
  }

  def findCommentCounts(userId: Long): Future[NotificationsCount] = {
    findCounts(userId, ViewCommentByUserDate)
  }

  def findTimelineCounts(userId: Long): Future[NotificationsCount] = {
    findCounts(userId, ViewTimelineByUserDate)
  }

  private def findCounts(userId: Long, view: String): Future[NotificationsCount] = {
    val query = queryCounts(userId)
    log.debug("query " + decode(query))
    val futureUnseenCount: Future[Seq[RawRow]] =
      bucket.rawSearch(DesignDocNotification, view)(query).toList
    futureUnseenCount.map(list => parseCounts(list.headOption))
  }

  private def queryCounts(userId: Long): Query = {
    new Query()
      .setIncludeDocs(false)
      .setRangeStart(ComplexKey.of(userId).forceArray(true))
      .setStale(Stale.OK)
      .setReduce(true)
      .setGroupLevel(1)
      .setRangeEnd(ComplexKey.of(Long.valueOf(userId + 1)).forceArray(true))
  }

  private def parseCounts(row: Option[RawRow]): NotificationsCount = {
    row.map(row => fromJson(Json.parse(row.value), classOf[NotificationsCount]))
      .getOrElse(new NotificationsCount())
  }
}
