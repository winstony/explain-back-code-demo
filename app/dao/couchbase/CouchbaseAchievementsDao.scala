package dao.couchbase

import play.api.Logger
import org.reactivecouchbase.play.PlayCouchbase
import play.api.libs.json._
import play.api.libs.json.JsObject
import scala.concurrent.Future
import java.util.List
import java.util.Map
import java.util.UUID
import play.api.libs.json.Json
import play.libs.{ Json => JavaJson }
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import org.reactivecouchbase.client.OpResult
import java.lang.Long
import play.api.Logger
import com.fasterxml.jackson.databind.JsonNode
import java.util.Optional
import com.couchbase.client.protocol.views.View
import com.couchbase.client.protocol.views.Query
import com.couchbase.client.protocol.views.ComplexKey
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.couchbase.client.protocol.views.Stale
import org.reactivecouchbase.client.TypedRow
import org.reactivecouchbase.client.Row
import org.reactivecouchbase.client.RawRow
import java.io.IOException
import dao.couchbase.DaoUtils._
import CouchbaseAchievementsDao._
import to.db.notification.Notification
import to.api.NotificationsCount
import to.db.notification.NotificationType
import to.db.notification.AchievementNotification
import to.api.achieve.Achievement

object CouchbaseAchievementsDao {
  val AchievementsBucketAlias = "default"
  val AchievementsDesignDoc = "achievement"
  val ViewUserTime = "by_user_time"
}

class CouchbaseAchievementsDao {
  private val log = Logger(this.getClass)
  private val bucket = PlayCouchbase.bucket(AchievementsBucketAlias)

  def get(key: String) = {
    DaoUtils.getDocument(bucket, key, classOf[Achievement])
  }

  def add(achievement: Achievement): Future[Achievement] = {
    DaoUtils.add(bucket, achievement.id, achievement, classOf[Achievement], "Could not add achievement")
  }

  def set(achievement: Achievement): Future[Achievement] = {
    DaoUtils.setDocument(bucket, achievement.id, achievement, classOf[Achievement], "Could not set achievement")
  }

  def getList(userId: Long): Future[List[Achievement]] = {
    val query = new Query().setRange(
      ComplexKey.of(userId).forceArray(true),
      ComplexKey.of(Long.valueOf(userId + 1)).forceArray(true))
    return DaoUtils.findDocuments(bucket, AchievementsDesignDoc, ViewUserTime, query, classOf[Achievement])
  }

}
