package dao.couchbase

import models.partners.Course
import scala.collection.JavaConverters._
import play.api.Logger
import org.reactivecouchbase.play.PlayCouchbase
import CouchbaseStatsDao._
import to.api.Stats
import dao.couchbase.DaoUtils._
import com.couchbase.client.protocol.views.Query
import com.couchbase.client.protocol.views.ComplexKey
import scala.concurrent.Future
import com.couchbase.client.protocol.views.Stale
import org.reactivecouchbase.CouchbaseRWImplicits._
import scala.collection.JavaConversions._
import to.api.HistoryElement
import org.reactivecouchbase.play.PlayCouchbase
import play.api.libs.json._
import scala.concurrent.Future
import play.api.libs.json.Json
import play.libs.{ Json => JavaJson }
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import org.reactivecouchbase.client.OpResult
import play.api.Logger
import com.fasterxml.jackson.databind.JsonNode
import java.util.Optional
import com.couchbase.client.protocol.views.View
import com.couchbase.client.protocol.views.Query
import com.couchbase.client.protocol.views.ComplexKey
import java.util.ArrayList
import scala.collection.JavaConversions._
import com.couchbase.client.protocol.views.Stale
import org.reactivecouchbase.client.TypedRow
import org.reactivecouchbase.client.Row
import org.reactivecouchbase.CouchbaseRWImplicits._
import org.reactivecouchbase.client.RawRow
import to.api.Stats
import dao.couchbase.DaoUtils._
import java.io.IOException
import java.lang.Long

object CouchbaseStatsDao {
  val DesignDocHistoryElement = CouchbaseHistoryElementDao.DESIGN_DOC_HISTORY_ELEMENT
  val ViewStats = "stats"
  val HistoryElementBucketAlias = CouchbaseHistoryElementDao.HISTORY_ELEMENT_BUCKET_ALIAS
}

class CouchbaseStatsDao {
  val log = Logger(this.getClass)
  val bucket = PlayCouchbase.bucket(HistoryElementBucketAlias)

  def getStatsByQuestion(userId: Long, courseId: Long): Future[java.util.List[Stats]] = {
    val query = new Query()
      .setRangeStart(ComplexKey.of(userId, courseId))
      .setRangeEnd(ComplexKey.of(userId, Long.valueOf(courseId + 1)))
      .setStale(Stale.FALSE)
      .setGroupLevel(3)
    retrieveStats(query)
  }

  def getStatsByQuestion(userId: Long, courses: java.util.List[Course]): Future[java.util.List[Stats]] = {
    val listFutures = courses.map { course =>
      val query = new Query()
        .setRangeStart(ComplexKey.of(userId, course.id))
        .setRangeEnd(ComplexKey.of(userId, Long.valueOf(course.id + 1)))
        .setStale(Stale.FALSE)
        .setGroupLevel(3)
      retrieveStats(query)
    }
    Future.sequence(listFutures).map(listStats => listStats.flatten.asJava)
  }

  private def retrieveStats(query: Query): Future[java.util.List[Stats]] = {
//    log.debug(query.getArgs.toString)
    val statsByQuestion: Future[List[RawRow]] = bucket.rawSearch(DesignDocHistoryElement, ViewStats)(query).toList
    statsByQuestion.onFailure { case error => log.error("Could not fetch stats for query " + query.getArgs, error) }
    statsByQuestion.map(list => list.map(row => extractStats(row)))
  }

  private def extractStats(row: RawRow): Stats = {
      val stats = fromJson(Json.parse(row.value), classOf[Stats])
      val key = JavaJson.parse(row.key)
      stats.userId = key.path(0).asInt()
      stats.courseId = key.path(1).asInt()
      stats.questionId = key.path(2).asInt()
      stats
  }

}
