package dao.couchbase;

import java.util.List;
import java.util.Optional;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.view.ViewQuery;
import com.couchbase.client.java.view.ViewRow;

import play.Logger;
import play.Logger.ALogger;
import play.Play;
import play.libs.Json;
import plugins.JavaCouchbasePlugin;
import to.api.ChoiceStatistics;

public class JavaDaoUtils {
    private static final ALogger log = Logger.of(JavaDaoUtils.class);

    private Bucket getBucket(String alias) {
        return Play.application().plugin(JavaCouchbasePlugin.class).getBucket(alias);
    }

    public <T> Optional<T> findOneValue(ViewQuery query, Class<T> clazz, String bucket) {
        query.development(Play.isDev());
        log.debug("Find one value, query: " + query.toString());
        List<ViewRow> allRows = getBucket(bucket).query(query).allRows();
        if (allRows.size() > 1) {
            throw new RuntimeException("Value not unique for query " + query.toString());
        }
        return allRows.stream().findFirst().map(row -> parse(row.value().toString(), clazz));
    }

    private <T> T parse(String value, Class<T> clazz) {
        log.debug("json retrieved -> " + value);
        return Json.fromJson(Json.parse(value), clazz);
    }

}
