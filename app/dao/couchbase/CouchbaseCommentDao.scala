package dao.couchbase

import to.api.Comment
import org.reactivecouchbase.play.PlayCouchbase
import play.api.libs.json._
import scala.concurrent.Future
import java.util.UUID
import play.libs.{ Json => JavaJson }
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import org.reactivecouchbase.client.OpResult
import java.lang.Long
import play.api.Logger
import com.fasterxml.jackson.databind.JsonNode
import java.util.Optional
import com.couchbase.client.protocol.views.View
import com.couchbase.client.protocol.views.Query
import com.couchbase.client.protocol.views.ComplexKey
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import com.couchbase.client.protocol.views.Stale
import org.reactivecouchbase.client.TypedRow
import org.reactivecouchbase.client.Row
import org.reactivecouchbase.CouchbaseRWImplicits._
import org.reactivecouchbase.client.RawRow
import dao.couchbase.DaoUtils._
import java.io.IOException
import CouchbaseCommentDao._
import java.net.URLDecoder

object CouchbaseCommentDao {
  val DESIGN_DOC_COMMENT = "comment"
  val VIEW_BY_USER = "by_user"
  val VIEW_BY_QUESTION = "by_question"
  val COMMENTS_BUCKET_ALIAS = "default"

  val DesignDocumentComments = "comments"

  val ViewBySchoolDate = "by_school_date"
  val ViewBySchoolCourseDate = "by_school_course_date"
  val ViewByQuestionDate = "by_question_date"
  val ViewByUserDate = "by_user_date"
}

class CouchbaseCommentDao {
  val log = Logger(this.getClass)
  val bucket = PlayCouchbase.bucket(COMMENTS_BUCKET_ALIAS)

  def getComment(uuid: String): Future[Optional[Comment]] = {
    DaoUtils.getDocument[Comment](bucket, uuid, classOf[Comment]);
  }

  def updateComment(uuid: String, comment: Comment): Future[Comment] = {
    comment.id = uuid
    setDocument(bucket, uuid, comment, classOf[Comment], "Could not update comment with uuid " + uuid);
  }

  def parseComments(list: List[TypedRow[JsObject]]): java.util.List[Comment] = {
    list.map(row => {
      val comment = fromJson(row.document, classOf[Comment])
      comment.id = row.id.get
      comment
    })
  }

  def saveComment(comment: Comment): Future[Comment] = {
    val generatedKey = generateUuid()
    comment.id = generatedKey;
    val futereNewRow: Future[Comment] =
      bucket.add(generatedKey, toJson(comment)).map(opResult => {
        if (opResult.ok) {
          fromJson(opResult.document.get, classOf[Comment])
        } else {
          log.error("Could not save comment" + opResult.msg)
          throw new IOException(opResult.msg.getOrElse("Could not save comment"))
        }
      })
    futereNewRow.onFailure { case error => log.error("Could not save comment", error) }
    futereNewRow
  }

  def getComments(questionId: Long, limit: Integer, startKeyDocId: String): Future[java.util.List[Comment]] = {
    val query = new Query()
      .setIncludeDocs(true)
      .setRangeStart(ComplexKey.of(questionId, Long.valueOf(Long.MAX_VALUE)))
      .setRangeEnd(ComplexKey.of(questionId).forceArray(true))
      .setDescending(true)
      .setStale(Stale.FALSE)
      .setReduce(false)
      .setLimit(limit)
      if (startKeyDocId != null) {
        query.setStartkeyDocID(startKeyDocId)
      }
    log.debug(URLDecoder.decode(query.toString().replace("+", "%2B"), "UTF-8").replace("%2B", "+"))
    val futureQuestionComments: Future[List[TypedRow[JsObject]]] =
      bucket.search[JsObject](DesignDocumentComments, ViewByQuestionDate)(query).toList
    futureQuestionComments.onFailure { case error => log.error("Could not fetch comments " + query.getArgs, error) }
    futureQuestionComments.map(parseComments(_))
  }

  def getUserComments(userId: Long, limit: Integer, startKeyDocId: String)
      : Future[java.util.List[Comment]] = {
    val query = new Query()
      .setIncludeDocs(true)
      .setRangeStart(ComplexKey.of(userId, Long.valueOf(Long.MAX_VALUE)))
      .setRangeEnd(ComplexKey.of(userId).forceArray(true))
      .setDescending(true)
      .setStale(Stale.FALSE)
      .setReduce(false)
      .setLimit(limit)
      if (startKeyDocId != null) {
        query.setStartkeyDocID(startKeyDocId)
      }
      log.debug(URLDecoder.decode(query.toString().replace("+", "%2B"), "UTF-8").replace("%2B", "+"))
      val futureUserComments: Future[List[TypedRow[JsObject]]] =
      bucket.search[JsObject](DesignDocumentComments, ViewByUserDate)(query).toList
    futureUserComments.onFailure { case error => log.error("Could not fetch comments " + query.getArgs, error) }
    futureUserComments.map(parseComments(_))
  }

  def getSchoolComments(schoolId: Long, limit: Integer, startKeyDocId: String): Future[java.util.List[Comment]] = {
    val query = new Query()
      .setIncludeDocs(true)
      .setRangeStart(ComplexKey.of(schoolId, Long.valueOf(Long.MAX_VALUE)))
      .setRangeEnd(ComplexKey.of(schoolId).forceArray(true))
      .setDescending(true)
      .setStale(Stale.FALSE)
      .setReduce(false)
      .setLimit(limit)
    if (startKeyDocId != null) {
      query.setStartkeyDocID(startKeyDocId)
    }
    log.debug(URLDecoder.decode(query.toString().replace("+", "%2B"), "UTF-8").replace("%2B", "+"))
    val futureSchoolComments: Future[List[TypedRow[JsObject]]] =
            bucket.search[JsObject](DesignDocumentComments, ViewBySchoolDate)(query).toList
    futureSchoolComments.onFailure { case error => log.error("Could not fetch comments " + query.getArgs, error) }
    futureSchoolComments.map(parseComments(_))
  }

  def getCourseComments(schoolId: Long, courseId:Long, limit: Integer, startKeyDocId: String): Future[java.util.List[Comment]] = {
    val query = new Query()
      .setIncludeDocs(true)
      .setRangeStart(ComplexKey.of(schoolId, courseId, Long.valueOf(Long.MAX_VALUE)))
      .setRangeEnd(ComplexKey.of(schoolId, courseId))
      .setDescending(true)
      .setStale(Stale.FALSE)
      .setReduce(false)
      .setLimit(limit)
    if (startKeyDocId != null) {
      query.setStartkeyDocID(startKeyDocId)
    }
    log.debug(URLDecoder.decode(query.toString().replace("+", "%2B"), "UTF-8").replace("%2B", "+"))
    val futureSchoolComments: Future[List[TypedRow[JsObject]]] =
            bucket.search[JsObject](DesignDocumentComments, ViewBySchoolCourseDate)(query).toList
    futureSchoolComments.onFailure { case error => log.error("Could not fetch comments " + query.getArgs, error) }
    futureSchoolComments.map(parseComments(_))
  }
}
