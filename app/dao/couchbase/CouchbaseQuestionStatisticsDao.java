package dao.couchbase;

import java.util.Optional;

import com.couchbase.client.java.view.ViewQuery;

import to.api.ChoiceStatistics;

public class CouchbaseQuestionStatisticsDao {

    private static final String BUCKET_ALIAS = "default";
    private static final String DESIGN_DOCUMENT = "question_statistics";
    private static final String VIEW = "choice_statistics";

    private JavaDaoUtils template = new JavaDaoUtils();

    public ChoiceStatistics get(long questionId ) {
        ViewQuery query = ViewQuery.from(DESIGN_DOCUMENT, VIEW).group().reduce().key(questionId);
        Optional<ChoiceStatistics> result = template.findOneValue(query, ChoiceStatistics.class, BUCKET_ALIAS);
        return result.orElse(new ChoiceStatistics(questionId));
    }

}
