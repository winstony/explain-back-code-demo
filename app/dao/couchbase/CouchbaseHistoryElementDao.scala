package dao.couchbase

import to.api.HistoryElement
import org.reactivecouchbase.play.PlayCouchbase
import play.api.libs.json._
import scala.concurrent.Future
import play.api.libs.json.Json
import play.libs.{ Json => JavaJson }
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import org.reactivecouchbase.client.OpResult
import play.api.Logger
import com.fasterxml.jackson.databind.JsonNode
import java.util.Optional
import com.couchbase.client.protocol.views.View
import com.couchbase.client.protocol.views.Query
import com.couchbase.client.protocol.views.ComplexKey
import java.util.ArrayList
import scala.collection.JavaConversions._
import com.couchbase.client.protocol.views.Stale
import org.reactivecouchbase.client.TypedRow
import org.reactivecouchbase.client.Row
import org.reactivecouchbase.CouchbaseRWImplicits._
import org.reactivecouchbase.client.RawRow
import CouchbaseHistoryElementDao._
import to.api.Stats
import dao.couchbase.DaoUtils._
import java.io.IOException
import java.lang.Long

object CouchbaseHistoryElementDao {
  val DESIGN_DOC_HISTORY_ELEMENT = "history_element"
  val VIEW_BY_USER = "by_user"
  val HISTORY_ELEMENT_BUCKET_ALIAS = "default"
}

class CouchbaseHistoryElementDao {
  val log = Logger(this.getClass)
  val bucket = PlayCouchbase.bucket(HISTORY_ELEMENT_BUCKET_ALIAS)

  def saveHistoryElement(record: HistoryElement): Future[HistoryElement] = {
    record.couchbaseId = if (record.couchbaseId == null) generateUuid() else record.couchbaseId
    DaoUtils.setDocument(bucket, record.couchbaseId, record, classOf[HistoryElement], "Could not save history element!")
  }

  def getCourseAnswers(userId: Long, courseId: Long): Future[java.util.List[HistoryElement]] = {
    val query = new Query()
      .setIncludeDocs(true)
      .setRange(ComplexKey.of(userId, courseId),
        ComplexKey.of(userId, Long.valueOf(courseId + 1)))
      // TODO stale may slow down server
      .setStale(Stale.FALSE)
      .setReduce(false)
    retrieveHistoryElements(query)
  }

  def getChapterAnswers(userId: Long, courseId: Long, chapterId: Long): Future[java.util.List[HistoryElement]] = {
    getChapterAnswers(userId, courseId, chapterId, 100);
  }

  def getChapterAnswers(userId: Long, courseId: Long, chapterId: Long, limit: Integer): Future[java.util.List[HistoryElement]] = {
    val query = new Query()
      .setIncludeDocs(true)
      .setDescending(true)
      .setRangeStart(ComplexKey.of(userId, courseId, Long.valueOf(chapterId + 1)))
      .setRangeEnd(ComplexKey.of(userId, courseId, chapterId))
      // TODO stale may slow down server
      .setStale(Stale.FALSE)
      .setReduce(false)
      .setLimit(limit)
    retrieveHistoryElements(query)
  }

  private def retrieveHistoryElements(query: Query): Future[java.util.List[HistoryElement]] = {
//    log.debug(query.getArgs.toString())
    val b: Future[List[TypedRow[JsObject]]] =
      bucket.search[JsObject](DESIGN_DOC_HISTORY_ELEMENT, VIEW_BY_USER)(query).toList
    b.onFailure { case error => log.error("Could not fetch history elements for query " + query.getArgs, error) }
    b.map(list => {
      list.map(row => {
        val record = fromJson(row.document, classOf[HistoryElement])
        record.couchbaseId = row.id.get
        record
      })
    })
  }

  def getCourseAnswerIds(userId: Long, courseId: Long): Future[java.util.List[String]] = {
    val query = new Query()
      .setIncludeDocs(false)
      .setRange(ComplexKey.of(userId, courseId),
        ComplexKey.of(userId, Long.valueOf(courseId + 1)))
      .setStale(Stale.FALSE)
      .setReduce(false)
    val futureIds: Future[List[RawRow]] =
      bucket.rawSearch(DESIGN_DOC_HISTORY_ELEMENT, VIEW_BY_USER)(query).toList
    futureIds.onFailure { case error => log.error("Could not fetch history element keys: ", error); }
    futureIds.map(list => list.map(_.id.get))
  }

  // for testing
  def delete(key: String) = {
    DaoUtils.deleteDocument(bucket, key);
  }

}
