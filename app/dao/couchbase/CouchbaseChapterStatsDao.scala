package dao.couchbase

import play.api.Logger
import org.reactivecouchbase.play.PlayCouchbase
import CouchbaseChapterStatsDao._
import dao.couchbase.DaoUtils._
import to.api.ChapterStats
import scala.concurrent.Future
import java.util.Optional

object CouchbaseChapterStatsDao {
  val CHAPTER_STATS_BUCKET_ALIAS = "default"
  val DesignDocumentChapterStats = "chapter_stats"

  val ViewByChapterProgressUser = "by_chapter_progress_user"
}

class CouchbaseChapterStatsDao {
  val log = Logger(this.getClass)
  val bucket = PlayCouchbase.bucket(CHAPTER_STATS_BUCKET_ALIAS)

  def getStats(userId: Long, chapterId: Long): Future[Optional[ChapterStats]] = {
    val uuid = "chapterStats::" + chapterId + "::" + userId
    DaoUtils.getDocument[ChapterStats](bucket, uuid, classOf[ChapterStats])
  }

  def saveStats(stats: ChapterStats) {
    val uuid = "chapterStats::" + stats.chapterId + "::" + stats.userId
    DaoUtils.setDocument(bucket, uuid, stats, classOf[ChapterStats], "Could not save chapter stats for " + uuid)
  }
}
