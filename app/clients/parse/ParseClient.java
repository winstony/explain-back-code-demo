package clients.parse;

import org.parse4j.ParseException;
import org.parse4j.ParseFile;
import org.parse4j.ParseObject;
import org.parse4j.ParseQuery;

import controllers.api.exceptions.ApiException;
import controllers.api.exceptions.ApiNotFoundException;
import play.Play;

public class ParseClient {

    public Image get(String imageId) {
        try {
            if (imageId.contains(".")) {
                imageId = imageId.substring(0, imageId.indexOf("."));
            }

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Image");

            ParseObject imageObject = query.get(imageId);
            if (imageObject == null) {
                throw new ApiNotFoundException("No such image");
            }
            imageObject.setObjectId(imageId);
            ParseFile file = (ParseFile) imageObject.get("file");
            return new Image(file.getName(), imageObject.get("type").toString(), file.getData());
        } catch (ParseException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * @return imageId
     */
    public String upload(String title, byte[] image, String contentType) {
        try {
            title = title.replaceAll("[^A-Za-z0-9_/./-]", "");
            ParseFile file = new ParseFile(title, image, contentType);
            file.save();
            ParseObject imageObject = new ParseObject("Image");
            imageObject.put("file", file);
            imageObject.put("size", file.getData().length);
            imageObject.put("mode", Play.mode().toString());
            imageObject.put("type", contentType);
            imageObject.save();
            String filetype = "";
            if (title != null && title.contains(".")) {
                filetype = title.substring(title.lastIndexOf("."), title.length());
            }
            return imageObject.getObjectId() + filetype;
        } catch (ParseException e) {
            throw new ApiException(e.getMessage());
        }
    }

    public static class Image {
        public final String title;
        public final String type;
        public final byte[] data;

        public Image(String title, String type, byte[] data) {
            this.title = title;
            this.type = type;
            this.data = data;
        }
    }

}
