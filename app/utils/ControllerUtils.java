package utils;

import models.core.User;
import models.partners.School;
import models.secure.SchoolUserTo;
import play.Logger;
import play.Play;
import play.api.mvc.Codec;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results.Status;
import securesocial.core.java.SecureSocial;
import to.api.ApiError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import controllers.api.exceptions.ApiBadRequestException;
import controllers.api.exceptions.ApiException;
import dao.SchoolDao;

public class ControllerUtils {
    public static final String JSON = "application/json";

    private static final Logger.ALogger log = Logger.of(ControllerUtils.class);

    static public Map<String, String> workAroundFixBindingForm() {
        Map<String,String> newData = new HashMap<String, String>();
        Map<String, String[]> urlFormEncoded = play.mvc.Controller.request().body().asFormUrlEncoded();
        Logger.debug(urlFormEncoded.toString());
        if (urlFormEncoded != null) {
            for (String key : urlFormEncoded.keySet()) {
                String[] value = urlFormEncoded.get(key);
                if (value.length == 1) {
                    newData.put(key, value[0]);
                } else if (value.length > 1) {
                    String keyPrefix = key;
                    String keyPostfix = "";
                    int pos = key.indexOf(".");
                    if (pos > -1) {
                        keyPrefix = key.substring(0, pos);
                        keyPostfix = key.substring(pos, key.length());
                    }
                    int actualIndex = 0;
                    for (int index = 0; index < value.length; index++) {
                        if (value[index] != "") {
                            newData.put(keyPrefix + "[" + actualIndex + "]"
                                    + keyPostfix, value[index]);
                            actualIndex++;
                        }
                    }
                }
            }
        }
        return newData;
    }

    public static SchoolUserTo getCurrentUser() {
        return ServiceUtils.getCurrentUser();
    }

    public static User getCurrentUserDb() {
        return ServiceUtils.getCurrentUserDb();
    }

    public static Result apiInternalServerError(String message) {
        ApiError error = new ApiError(Http.Status.INTERNAL_SERVER_ERROR, message);
        return Controller.internalServerError(Json.toJson(error)).as(JSON);
    }

    public static Result unauthorized() {
        ApiError error = new ApiError(Http.Status.UNAUTHORIZED, "Credentials required");
        return Controller.unauthorized(Json.toJson(error)).as(JSON);
    }

    public static Result forbidden(String message) {
        ApiError error = new ApiError(Http.Status.FORBIDDEN, message);
        return Controller.forbidden(Json.toJson(error)).as(JSON);
    }

    public static Result paymentRequired() {
        ApiError error = new ApiError(Http.Status.PAYMENT_REQUIRED, "Payment required");
        return Controller.status(Http.Status.PAYMENT_REQUIRED, Json.toJson(error), "utf-8").as(JSON);
    }

    public static Result apiBadRequest(List<String> errors) {
        ApiError error = new ApiError(Http.Status.BAD_REQUEST, errors);
        return Controller.badRequest(Json.toJson(error)).as(JSON);
    }

    public static Result apiNotFound(String message) {
        ApiError error = new ApiError(Http.Status.NOT_FOUND, message);
        return Controller.notFound(Json.toJson(error)).as(JSON);
    }

    public static Result apiNoContent() {
        ApiError message = new ApiError(Http.Status.NO_CONTENT, "No content");
        Status s = new Status(play.core.j.JavaResults.Status(200), Json.toJson(message), Codec.javaSupported("utf-8"));
        return s.as(JSON);
    }

}
