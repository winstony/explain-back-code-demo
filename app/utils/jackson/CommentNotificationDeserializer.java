package utils.jackson;

import java.io.IOException;

import play.libs.Json;
import to.db.notification.CommentNotification;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class CommentNotificationDeserializer extends JsonDeserializer<CommentNotification> {

    private static final NotificationDeserializer notificationDeserializer = new NotificationDeserializer();

    @Override
    public CommentNotification deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException,
    JsonProcessingException {
        return (CommentNotification) notificationDeserializer.deserialize(jp, ctxt);
    }

}
