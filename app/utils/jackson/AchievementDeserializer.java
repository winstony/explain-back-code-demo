package utils.jackson;

import java.io.IOException;

import play.libs.Json;
import to.api.achieve.Achievement;
import to.api.achieve.AchievementType;
import to.db.notification.Notification;
import to.db.notification.NotificationType;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class AchievementDeserializer extends JsonDeserializer<Achievement> {

    @Override
    public Achievement deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException,
    JsonProcessingException {
        JsonNode notification = jp.getCodec().readTree(jp);
        AchievementType type = Json.fromJson(notification.path("achievementType"), AchievementType.class);
        return Json.fromJson(notification, type.getAchievementClass());
    }

}
