package service;

import java.util.Set;

import play.Logger;
import play.libs.F.Promise;
import to.api.ChapterStats;
import to.api.ItemStatus;
import to.api.QuestionShort;
import dao.QuestionDao;
import dao.couchbase.CouchbaseChapterStatsDao;

public class ChapterStatsFactory {

    private static final Logger.ALogger log = Logger.of(ChapterStatsFactory.class);

    public Promise<ChapterStats> updateChapterStats(long userId, long schoolId, long courseId, long chapterId) {
        QuestionDao.QueryBuilder params = new QuestionDao.QueryBuilder()
        .schoolId(schoolId)
        .courseId(courseId);
        return updateChapterStats(userId, courseId, chapterId, QuestionDao.findQuestionsShort(params));
    }

    public Promise<ChapterStats> updateChapterStats(
            long userId,
            long courseId,
            long chapterId,
            Set<QuestionShort> questions) {
        return new StatsService().countStatsForQuestions(userId, courseId, questions).map(itemStatus -> {
            ChapterStats stats = new ChapterStats(userId, courseId, chapterId);
            if (itemStatus.numberOfAnswers > 0) {
                stats.progress = itemStatus.getProgress();
                stats.freeProgress = itemStatus.getFreeProgress();
                new CouchbaseChapterStatsDao().saveStats(stats);
                log.debug("Updating stats for user " + userId
                        + " course " + courseId
                        + " chapter " + chapterId
                        + " progress " + stats.progress);
            } else {
                log.debug("No need to update stats for user " + userId + " chapter " + chapterId);
            }
            return stats;
        });
    }

}
