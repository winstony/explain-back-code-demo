package service;

import com.google.common.collect.Maps;

import dao.QuestionDao;
import dao.QuestionDao.QueryBuilder;
import dao.UserDao;
import dao.couchbase.CouchbaseCommentDao;
import dao.couchbase.CouchbaseNotificationDao;
import models.core.User;
import models.secure.SchoolUserTo;
import play.Logger;
import play.Logger.ALogger;
import play.libs.F.Promise;
import play.libs.Json;
import to.api.Comment;
import to.api.CommentType;
import to.api.QuestionShort;
import to.db.notification.NewCommentNotification;

import java.util.*;
import java.util.stream.Collectors;

public class CommentsService {
    private static final ALogger log = Logger.of(CommentsService.class);

    private static CouchbaseCommentDao commentsDao = new CouchbaseCommentDao();
    private static CouchbaseNotificationDao notificationsDao = new CouchbaseNotificationDao();

    public Promise<Comment> addQuestionComment(SchoolUserTo user, Comment newComment, String questionUuid) {
        newComment.user = user;
        newComment.timestamp = System.currentTimeMillis();
        newComment.schoolId = user.schoolId;
        QueryBuilder query = new QuestionDao.QueryBuilder().uuids(Collections.singleton(questionUuid));
        QuestionDao.findQuestionsShort(query).stream().findFirst().ifPresent(qId -> {
            newComment.question = qId;
            newComment.questionId = qId.id;
        });
        Promise<Comment> commentPromise = Promise.wrap(commentsDao.saveComment(newComment));
        if (CommentType.TEXT.equals(newComment.commentType)) {
            commentPromise.onRedeem(comment -> postSavedCommentNotifications(comment));
        }
        return commentPromise;
    }

    private void postSavedCommentNotifications(Comment savedComment) {
        Promise.wrap(commentsDao.getComments(savedComment.questionId, 10000, null)).onRedeem(oldComments -> {
            Set<Long> commenters = oldComments.stream()
                    .filter(old -> CommentType.TEXT.equals(old.commentType))
                    .map(old -> old.user.id).collect(Collectors.toCollection(HashSet::new));
            commenters.remove(savedComment.user.id);
            if (savedComment.parentId != null) {
                // TODO notification "Someone replied to your comment" to author of parent
                // commenters.remove( parentCommenter )
            }
            updateQuestionCommentNotifications(
                    savedComment.schoolId,
                    savedComment.question,
                    commenters,
                    savedComment.id);
        });
    }

    private void updateQuestionCommentNotifications(Long schoolId,
            QuestionShort question,
            Set<Long> commenters,
            String commentId) {
        commenters.stream().forEach(
                userId -> {
                    String id = "notify:question_comment:" + userId + ":" + question.questionId;
                    Promise.wrap(notificationsDao.getNotification(id, NewCommentNotification.class)).onRedeem(
                            maybe -> {
                                if (maybe.isPresent()) {
                                    NewCommentNotification notification = maybe.get();
                                    notification.read = false;
                                    notification.seen = false;
                                    notification.updatedTime = System.currentTimeMillis();
                                    notificationsDao.update(notification);
                                } else {
                                    NewCommentNotification notification =
                                            new NewCommentNotification(question.uuid, commentId);
                                    notification.id = id;
                                    notification.userId = userId;
                                    notification.schoolId = schoolId;
                                    notification.updatedTime = System.currentTimeMillis();
                                    notificationsDao.save(notification);
                                }
                            });
                });
    }

    public Optional<Promise<List<Comment>>> getComments(String questionUuid, int limit, String startKey) {
        Optional<Long> maybeId = QuestionDao.findIdByUuid(questionUuid);
        return maybeId.map(id -> Promise.wrap(commentsDao.getComments(id, limit, startKey))
                .map(cl -> withUsernames(cl)));
    }

    public Promise<Optional<Comment>> updateQuestionComment(Long userId, String commentUuid, Comment update) {
        Promise<Optional<Comment>> promise = Promise.wrap(commentsDao.getComment(commentUuid));
        Optional<Comment> notFound = Optional.empty();
        return promise.flatMap(maybeOld -> {
            return maybeOld.filter(old -> userId.equals(old.user.id)).map(old -> {
                old.text = update.text;
                old.editedAt = new Date().getTime();
                old.deleted = update.deleted;
                Promise<Optional<Comment>> updatedPromise = Promise.wrap(commentsDao.updateComment(commentUuid, old))
                        .map(updated -> Optional.of(updated));
                return updatedPromise;
            }).orElse(Promise.pure(notFound));
        });
    }

    public Promise<Optional<Comment>> deleteComment(Long userId, String commentUuid) {
        Promise<Optional<Comment>> promise = Promise.wrap(commentsDao.getComment(commentUuid));
        Optional<Comment> notFound = Optional.empty();
        return promise.flatMap(maybeOld -> {
            return maybeOld.filter(old -> userId.equals(old.user.id)).map(comment -> {
                comment.deleted = true;
                Promise<Comment> updated = Promise.wrap(commentsDao.updateComment(commentUuid, comment));
                Promise<Optional<Comment>> result = updated.map(u -> Optional.of(u));
                return result;
            }).orElse(Promise.pure(notFound));
        });
    }

    public Promise<List<Comment>> getCourseComments(long schoolId, long courseId, int limit, String startKey) {
        return Promise.wrap(commentsDao.getCourseComments(schoolId, courseId, limit, startKey))
                .map(cl -> withShortQuestions(cl))
                .map(cl -> withUsernames(cl));
    }

    public Promise<List<Comment>> getSchoolComments(long schoolId, int limit, String startKey) {
        return Promise.wrap(commentsDao.getSchoolComments(schoolId, limit, startKey))
                .map(cl -> withShortQuestions(cl))
                .map(cl -> withUsernames(cl));
    }

    public Promise<List<Comment>> getUserComments(long userId, Integer limit, String startKey) {
        return Promise.wrap(commentsDao.getUserComments(userId, limit, startKey))
                .map(cl -> withShortQuestions(cl))
                .map(cl -> withUsernames(cl));
    }

    private List<Comment> withShortQuestions(List<Comment> cl) {
        QueryBuilder params = new QuestionDao.QueryBuilder()
                .ids(cl.stream().map(c -> c.questionId).collect(Collectors.toSet()));
        Map<Long, QuestionShort> questions = Maps.uniqueIndex(QuestionDao.findQuestionsShort(params), q -> q.id);
        cl.stream().forEach(c -> c.question = questions.get(c.questionId));
        return cl.stream().filter(c -> c.question != null).collect(Collectors.toList());
    }

    private List<Comment> withUsernames(List<Comment> comments) {
        List<Long> userIds = comments.stream()
                .map(comment -> comment.user.id)
                .collect(Collectors.toList());
        List<User> users = UserDao.findUsersByIds(userIds);
        Map<Long, String> mappedIdsUsernames = users.stream()
                .collect(HashMap::new, (m, v) -> m.put(v.id, v.username), HashMap::putAll);
        return comments.stream()
                .map(comment -> {
                    comment.user.username = mappedIdsUsernames.get(comment.user.id);
                    return comment;
                })
                .collect(Collectors.toList());
    }

}