package service;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;

import dao.CourseDao;
import dao.QuestionDao;
import dao.couchbase.CouchbaseStatsDao;
import models.partners.Course;
import play.Logger;
import play.libs.F.Promise;
import scala.concurrent.Future;
import to.api.ItemStatus;
import to.api.QuestionShort;
import to.api.Stats;

import java.util.*;

public class StatsService {
    private static final Logger.ALogger log = Logger.of(StatsService.class);

    public Promise<ItemStatus> getChapterStats(long userId, long schoolId, long courseId, long chapterId) {
        // TODO retrieve precalculated value from db;
        return countChapterStats(userId, schoolId, courseId, chapterId);
    }

    public Promise<ItemStatus> countChapterStats(long userId, long schoolId, long courseId, long chapterId) {
        QuestionDao.QueryBuilder params = new QuestionDao.QueryBuilder()
            .schoolId(schoolId)
            .chapterIds(Collections.singleton(chapterId))
            .courseId(courseId);
        Promise<Set<QuestionShort>> questionsPromise = Promise.promise(() -> QuestionDao.findQuestionsShort(params));
        return questionsPromise.flatMap(questions -> countStatsForQuestions(userId, courseId, questions)).map(stats -> {
            stats.chapterId = chapterId;
            return stats;
        });
    }

    public Promise<ItemStatus> getCourseStats(long userId, long schoolId, long courseId) {
        QuestionDao.QueryBuilder params = new QuestionDao.QueryBuilder()
            .schoolId(schoolId)
            .courseId(courseId);
        Promise<Set<QuestionShort>> questionsPromise = Promise.promise(() -> QuestionDao.findQuestionsShort(params));
        return questionsPromise.flatMap(questions -> countStatsForQuestions(userId, courseId, questions));
    }

    public Promise<ItemStatus> countStatsForQuestions(long userId, long courseId, Set<QuestionShort> questions) {
        Future<List<Stats>> statsByQuestionFuture = new CouchbaseStatsDao().getStatsByQuestion(userId, courseId);
        Promise<ItemStatus> result = Promise.wrap(statsByQuestionFuture).map(statsByQuestion -> {
            ItemStatus stats = countStats(questions, statsByQuestion);
            stats.courseId = courseId;
            return stats;
        });
        return result;
    }

    public Promise<List<ItemStatus>> getCourseStatsByChapter(long userId, long schoolId, long courseId) {
        Future<List<Stats>> statsByQuestion = new CouchbaseStatsDao().getStatsByQuestion(userId, courseId);
        QuestionDao.QueryBuilder params = new QuestionDao.QueryBuilder()
            .schoolId(schoolId)
            .courseId(courseId);
        Promise<Set<QuestionShort>> idsPromise = Promise.promise(() -> QuestionDao.findQuestionsShort(params));
        return Promise.wrap(statsByQuestion).flatMap(list -> {
            Promise<List<ItemStatus>> statsPromise = idsPromise.map(ids -> {
                ListMultimap<Long, QuestionShort> idsByChapter = Multimaps.index(ids, id -> id.chapterId);
                List<ItemStatus> statuses = new ArrayList<>();
                for (Long chapterId : idsByChapter.keySet()) {
                    Set<QuestionShort> questions = new HashSet<>(idsByChapter.get(chapterId));
                    ItemStatus status = countStats(questions, list);
                    status.chapterId = chapterId;
                    status.courseId = courseId;
                    statuses.add(status);
                }
                return statuses;
            });
            return statsPromise;
        });
    }

    public Promise<List<ItemStatus>> getStatsByCourse(long userId, long schoolId) {
        List<Course> courses = CourseDao.getSchoolCourses(schoolId);
        Future<List<Stats>> statsByQuestion = new CouchbaseStatsDao().getStatsByQuestion(userId, courses);
        QuestionDao.QueryBuilder params = new QuestionDao.QueryBuilder().schoolId(schoolId);
        Promise<Set<QuestionShort>> idsPromise = Promise.promise(() -> QuestionDao.findQuestionsShort(params));
        return Promise.wrap(statsByQuestion).flatMap(list -> {
            Promise<List<ItemStatus>> statsPromise = idsPromise.map(ids -> {
                ListMultimap<Long, QuestionShort> idsByCourse = Multimaps.index(ids, id -> id.courseId);
                List<ItemStatus> statuses = new ArrayList<>();
                for (Long courseId : idsByCourse.keySet()) {
                    Set<QuestionShort> questions = new HashSet<>(idsByCourse.get(courseId));
                    ItemStatus status = countStats(questions, list);
                    status.courseId = courseId;
                    statuses.add(status);
                }
                return statuses;
            });
            return statsPromise;
        });
    }

    private ItemStatus countStats(
            Set<QuestionShort> ids,
            List<Stats> statsByQuestion) {

        ItemStatus status = new ItemStatus();
        status.numberOfQuestions = ids.size();

        Map<Long, Stats> idxStatsByQuestionId = Maps.uniqueIndex(statsByQuestion, s -> s.questionId);

        for (QuestionShort questionIdentifier : ids) {
            Stats answerStats = idxStatsByQuestionId.get(questionIdentifier.questionId);
            if (answerStats == null) {
                continue;
            }
            if (answerStats.lastCorrect) {
                status.numberOfQuestionsSucceeded++;
            } else {
                status.numberOfQuestionsFailed++;
            }
            status.numberOfCorrectAnswers += answerStats.correct;
            status.numberOfAnswers += answerStats.total;
        }
        if (ids.size() != 0) {
            status.freePercent = (int) ids.stream().filter(q -> q.free).count() * 100 / ids.size();
        }
        return status;
    };
}
