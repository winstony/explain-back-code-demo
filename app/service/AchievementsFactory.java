package service;

import play.libs.F.Promise;
import to.api.ChapterStats;
import to.api.achieve.Achievement;
import to.api.achieve.ChapterFreeCompleteAchievement;
import dao.couchbase.CouchbaseAchievementsDao;

/**
 * Создает ачивмент и сохраняет его
 */
public class AchievementsFactory {
    private static CouchbaseAchievementsDao achievementsDao = new CouchbaseAchievementsDao();

    public Promise<ChapterFreeCompleteAchievement> grantChapterFreeAchievement(ChapterStats chapterStats) {
        ChapterFreeCompleteAchievement achievement = new ChapterFreeCompleteAchievement();
        achievement.chapterId = chapterStats.chapterId;
        achievement.courseId = chapterStats.courseId;
        achievement.timestamp = System.currentTimeMillis();
        achievement.userId = chapterStats.userId;
        achievement.id = "achievement_" + achievement.getAchievementType()
                + "_user_" + achievement.userId
                + "_chapter_" + achievement.chapterId;
        return Promise.wrap(achievementsDao.add(achievement)).map(a -> (ChapterFreeCompleteAchievement) a);
    }

}
