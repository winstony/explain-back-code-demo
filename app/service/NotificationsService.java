package service;

import dao.couchbase.CouchbaseNotificationDao;
import models.core.User;
import models.secure.SchoolUserTo;
import play.Logger;
import play.Logger.ALogger;
import play.libs.F.Promise;
import to.api.Notifications;
import utils.ControllerUtils;

import java.util.Optional;

/**
 * Получает нотификейшены, модифицирует нотификейшены
 */
public class NotificationsService {
    private static final ALogger log = Logger.of(NotificationsService.class);
    private static CouchbaseNotificationDao notificationsDao = new CouchbaseNotificationDao();

    public Promise<Notifications> findCommentNotifications(SchoolUserTo user, long before, int limit) {
        Long schoolId = user.schoolId;
        return Promise.wrap(notificationsDao.findCommentNotifications(user.id, before, limit))
                .flatMap(list -> {
                    Promise<Notifications> result = Promise.wrap(notificationsDao.findCommentCounts(user.id))
                            .map(counts -> {
                                Notifications notifications = new Notifications();
                                notifications.counts = counts;
                                notifications.list = list;
                                return notifications;
                            });
                    return result;
                });
    }

    public Promise<Notifications> findTimelineNotifications(SchoolUserTo user, long before, int limit) {
        return Promise.wrap(notificationsDao.findTimelineNotifications(user.id, before, limit))
                .flatMap(list -> {
                    Promise<Notifications> result = Promise.wrap(notificationsDao.findTimelineCounts(user.id))
                            .map(counts -> {
                                Notifications notifications = new Notifications();
                                notifications.counts = counts;
                                notifications.list = list;
                                return notifications;
                            });
                    return result;
                });
    }
}
