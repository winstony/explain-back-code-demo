package service;

import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import controllers.api.exceptions.ApiNotFoundException;
import dao.QuestionDao;
import dao.couchbase.CouchbaseQuestionStatisticsDao;
import dao.couchbase.CouchbaseQuestionStatisticsDao;
import play.libs.F.Promise;
import to.api.AnswerType;
import to.api.ChoiceStatistics;
import to.api.Question;

public class QuestionStatisticsService {

    private CouchbaseQuestionStatisticsDao dao = new CouchbaseQuestionStatisticsDao();

    public Promise<ChoiceStatistics> get(String questionUuid) {
        long id = QuestionDao.findIdByUuid(questionUuid).orElseThrow(notFound(questionUuid));
        QuestionDao.QueryBuilder questionQuery = new QuestionDao.QueryBuilder().ids(id);
        Promise<Question> questionPromise = Promise.promise(() -> QuestionDao.getQuestion(questionQuery))
                .map(question -> question.orElseThrow(notFound(questionUuid)));
        Promise<ChoiceStatistics> promise = Promise.promise(() -> dao.get(id));
        return promise.flatMap(stats -> questionPromise.map(question -> correctRemovedChoices(stats, question)));
    }

    private Supplier<? extends ApiNotFoundException> notFound(String questionUuid) {
        return () -> new ApiNotFoundException("No question for " + questionUuid + " found");
    }

    private ChoiceStatistics correctRemovedChoices(ChoiceStatistics s, Question question) {
        // Почему-то есть некоторые старые ответы в которых не совпадают айдишники вариантов ответа
        Set<Integer> actualChoices = question.choices.stream().map(c -> c.id).collect(Collectors.toSet());
        ChoiceStatistics result = new ChoiceStatistics(s.questionId);
        for (Integer choiceId : actualChoices) {
            result.choices.put(choiceId, s.choices.getOrDefault(choiceId, 0));
        }
        if (AnswerType.SINGLE_CHOICE.name().equals(question.getAnswerType())) {
            result.total = result.choices.values().stream().mapToInt(Integer::intValue).sum();
            int correctId = question.getCorrectChoiceIds().stream().findFirst().orElse(0);
            result.correct = result.choices.getOrDefault(correctId, 0);
        } else {
            result.total = s.total;
            result.correct = s.correct;
        }
        return result;
    }

}
