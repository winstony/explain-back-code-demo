package service;

import controllers.api.exceptions.ApiNotFoundException;
import dao.QuestionDao;
import dao.QuestionDao.QueryBuilder;
import dao.couchbase.CouchbaseHistoryElementDao;
import models.secure.SchoolUserTo;
import play.libs.F.Promise;
import service.queue.RepeatIncorrectQueueBuilder;
import to.api.HistoryElement;
import to.api.Question;
import to.api.convert.ApiConverter;
import utils.ServiceUtils;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class QuestionsService {

    private static CouchbaseHistoryElementDao historyElementDao = new CouchbaseHistoryElementDao();

    public static Promise<List<Question>> getChapterQuestions(String courseAlias, String chapterAlias, Integer size) {
        SchoolUserTo user = ServiceUtils.getCurrentUser();
        QueryBuilder params = new QuestionDao.QueryBuilder()
            .schoolId(user.schoolId)
            .courseAlias(courseAlias)
            .chapterAlias(chapterAlias);
        List<models.core.Question> questions = QuestionDao.getQuestions(params);
        List<to.api.Question> questionsPool = AccessControlService.buildQuestionsPool(
                questions.stream().map(q -> ApiConverter.to(q)).collect(Collectors.toList()));
        if (questions.isEmpty()) {
            return Promise.pure(Collections.emptyList());
        }
        Long chapterId = questions.iterator().next().chapterId;
        Long courseId = questions.iterator().next().courseId;
        Integer limit = questions.size() * 3 / 2;
        Promise<List<HistoryElement>> recordsPromise = Promise.wrap(
                historyElementDao.getChapterAnswers(user.id, courseId, chapterId, limit));
        return recordsPromise.map(records -> {
            return new RepeatIncorrectQueueBuilder(records, questionsPool, user.id).buildQueue(size);
        });
    }

    public static String getOpenQuestionUuidForCourse(String courseAlias) {
        String qUuid = QuestionDao.getRandomOpenQuuidFromCourse(courseAlias);
        if (qUuid == null) {
            throw new ApiNotFoundException("Open question not found");
        }
        return qUuid;
    }

    public static to.api.Question getQuestionByUuid(String uuid) {
        Question question = getQuestionsByUuids(Collections.singleton(uuid)).stream().findFirst().orElse(null);
        return AccessControlService.readQuestion(question);
    }

    public static List<to.api.Question> getQuestionsByUuids(Set<String> uuids) {
        if (uuids == null) {
            uuids = Collections.emptySet();
        }
        QueryBuilder params = new QuestionDao.QueryBuilder().uuids(uuids);
        List<models.core.Question> questions = QuestionDao.getQuestions(params);
        return questions.stream().map(q -> ApiConverter.to(q)).collect(Collectors.toList());
    }

}
