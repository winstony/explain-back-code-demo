package service.translate;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.FetchConfig;
import dao.QuestionDao;
import models.Status;
import models.core.*;
import org.apache.commons.lang3.time.DateUtils;
import play.db.ebean.Model;
import to.ChoiceTO;
import to.LangString;
import to.api.convert.ApiConverter;
import to.api.translate.QuestionForTranslation;
import play.libs.mailer.Email;
import play.libs.mailer.MailerPlugin;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TranslationsService {
    private static Model.Finder<Long, Question> findQuestion =
            new Model.Finder<Long, Question>(Long.class, Question.class);

    private static ExpressionList<Question> queryQuestions() {
        return findQuestion
                .fetch("translations")
                .fetch("choices", new FetchConfig().query())
                .fetch("choices.translations")
                .fetch("topic")
                .order().asc("lastEditedAt")
                .order().asc("id")
                .where()
                .eq("deleted", false);

    }
    public QuestionForTranslation open(Long userId, String topic) {
        Date nowMinusHour = DateUtils.addHours(new Date(), -1);
        List<QuestionForTranslation> my = my(userId);
        QuestionForTranslation qft = null;
        List<QuestionForTranslation> pending = my.stream()
                .filter(q -> q.status != Status.APPROVED )
                .filter(q -> topic.equals(q.topic))
                .collect(Collectors.toList());
        long approved = my.stream()
                .filter(q -> q.status == Status.APPROVED).count();
        Question update = new Question();
        if (pending.size() >= Math.max(10, approved * 2)) {
            return null;
        } else {
            Question question = queryQuestions()
                    .eq("status", Status.OPEN)
                    .lt("lastEditedAt", nowMinusHour)
                    .eq("topic.alias", topic)
                    .setMaxRows(1)
                    .findUnique();
            if (question == null) {
                return null;
            }
            if (question.uuid == null) {
                update.uuid = java.util.UUID.randomUUID().toString().replace("-", "");
            }
            removeTranslation(question);
            qft = transform(question);
        }
        update.id = qft.id;
        update.authorId = userId;
        update.lastEditedAt = new Date();
        Ebean.update(update);
        new TranslateLogService().open(userId, qft);
        return qft;
    }

    private void removeTranslation(Question question) {
        QuestionTranslation translation = question.translations.stream()
                .filter(tr -> "en".equals(tr.language))
                .findFirst()
                .orElse(new QuestionTranslation());
        translation.language = "en";
        translation.questionId = question.id;
        translation.status = Status.OPEN.toString();
        translation.ready = false;
        translation.deleted = false;
        translation.explanation = "";
        translation.text = "";
        Ebean.save(translation);
        for (Choice c : question.choices) {
            ChoiceTranslation choiceTranslation = c.translations.stream()
                    .filter(tr -> "en".equals(tr.language))
                    .findFirst()
                    .orElse(new ChoiceTranslation());
            choiceTranslation.setChoiceId(c.id);
            choiceTranslation.text = "";
            choiceTranslation.language = "en";
            Ebean.save(choiceTranslation);
        }

    }

    public QuestionForTranslation save(QuestionForTranslation record, String uuid, Long userId) {
        Question fromDB = queryQuestions().where().eq("uuid", uuid).findUnique();
        mailUs(userId, fromDB.id);
        record.id = fromDB.id;
        new TranslateLogService().save(userId, record);
        Question update = new Question();
        update.id = fromDB.id;
        update.authorId = userId;
        update.lastEditedAt = new Date();
        if (fromDB.status == Status.OPEN) {
            update.status = Status.PENDING;
        }
        Ebean.update(update);
        QuestionTranslation en = fromDB.translations.stream()
                .filter(t -> "en".equals(t.language))
                .findFirst().orElse(new QuestionTranslation());
        en.deleted = false;
        en.explanation = record.explanationTranslations.stream()
                .filter(tt -> "en".equals(tt.language)).findFirst()
                .map(tt -> tt.text).orElse("");
        en.text = record.textTranslations.stream()
                .filter(tt -> "en".equals(tt.language)).findFirst()
                .map(tt -> tt.text).orElse("");
        en.language = "en";
        en.questionId = update.id;
        en.ready = false;
        en.status = Status.PENDING.toString();
        Ebean.save(en);
        for (ChoiceTO cto : record.choices) {
            ChoiceTranslation choiceTranslation = new ChoiceTranslation();
            choiceTranslation.id = fromDB.choices.stream().filter(c -> c.id.equals(cto.id)).findFirst().get()
                    .translations.stream().filter(t -> "en".equals(t.language)).findFirst().get().id;
            choiceTranslation.language = "en";
            choiceTranslation.setChoiceId(cto.id);
            choiceTranslation.text = cto.textTranslations.stream()
                    .filter(ls -> "en".equals(ls.language))
                    .findFirst()
                    .map(ls -> ls.text).orElse("");
            Ebean.update(choiceTranslation);
        }
        return record;
    }

    private void mailUs(Long userId, Long questionId) {
        User user = User.findById(userId);
        Question question = QuestionDao.findById(questionId);
        String subject = "Translation: " + questionId + " by " + user.fullname + " " + user.email + " (" + user.id + ")";
        String address = "info@quizful.com";
        String text = subject + "\r\n" +
                "Topic: " + question.topic.title + "\r\n" +
                "Link: http://explain.ws/#/question/edit/" + questionId + "?limit=5&offset=0";
        Email email = new Email();
        email.setSubject(subject);
        email.setFrom("explain@explain.com.ua");
        email.addTo(address);
        email.setBodyText(text);
        email.setBodyHtml("<html><body><p>"+ text + "</p></body></html>");
        MailerPlugin.send(email);
    }

    public QuestionForTranslation get(String uuid, Long userId) {
        Question q = queryQuestions().where()
                .eq("uuid", uuid)
                .eq("authorId", userId)
                .findUnique();
        Question update = new Question();
        update.id = q.id;
        update.lastEditedAt = new Date();
        Ebean.update(update);
        QuestionForTranslation qft = transform(q);
        new TranslateLogService().open(userId, qft);
        return qft;
    }

    public void cancel(String uuid, Long userId) {
        Question q = queryQuestions().where()
                .eq("uuid", uuid)
                .eq("authorId", userId)
                .findUnique();
        q.lastEditedAt = DateUtils.addHours(new Date(), -1);
        q.status = Status.OPEN;
        Ebean.update(q);
        QuestionForTranslation qft = transform(q);
        new TranslateLogService().cancel(userId, qft);
    }

    public List<QuestionForTranslation> my(Long userId) {
        Date nowMinusHour = DateUtils.addHours(new Date(), -1);
        List<Question> questions = queryQuestions()
                .where()
                .or(
                        Expr.and(Expr.gt("lastEditedAt", nowMinusHour), Expr.eq("status", Status.OPEN)),
                        Expr.ne("status", Status.OPEN))
                        .eq("authorId", userId)
                        .findList();
        return questions.stream().map(q -> transform(q)).collect(Collectors.toList());
    }

    public static List<to.api.Topic> getSchoolTranslatableTopics(Long schoolId) {
        List<Question> openQuestionTopics = findQuestion
                .select("topic")
                .setDistinct(true)
                .where()
                .eq("status", Status.OPEN)
                .eq("course.school.id", schoolId)
                .eq("deleted", false)
                .findList();

        return openQuestionTopics.stream()
                .map(question -> question.topic)
                .distinct()
                .map(ApiConverter::to)
                .collect(Collectors.toList());
    }

    public static List<to.api.Topic> getTopicsByAliases(String[] topicsAliases) {
        Model.Finder<Long, Topic> findTopic = new Model.Finder<>(Long.class, Topic.class);
        return findTopic
                .where()
                .in("alias", topicsAliases)
                .findList()
                .stream()
                .map(ApiConverter::to)
                .collect(Collectors.toList());
    }

    private QuestionForTranslation transform(Question q) {
        QuestionForTranslation t = new QuestionForTranslation();
        t.explanation = q.explanation;
        t.text = q.question;
        t.id = q.id;
        t.uuid = q.uuid;
        t.language = q.language;
        t.status = q.status;
        t.lastEditedAt = q.lastEditedAt;
        if (q.translations != null) {
            t.textTranslations = q.translations.stream()
                    .map(qt -> new LangString(qt.language, qt.text))
                    .collect(Collectors.toList());
            t.explanationTranslations = q.translations.stream()
                    .map(qt -> new LangString(qt.language, qt.explanation))
                    .collect(Collectors.toList());
        }
        if (q.choices != null) {
            t.choices = q.choices.stream().map(c -> transform(c)).collect(Collectors.toList());
        }
        t.topic = q.topic.alias;
        t.answerType = q.questionType;
        return t;
    }

    private ChoiceTO transform(Choice c) {
        ChoiceTO t = new ChoiceTO();
        t.text = c.text;
        t.id = c.id;
        if (c.translations != null) {
            t.textTranslations = c.translations.stream()
                    .map(ct -> new LangString(ct.language, ct.text))
                    .collect(Collectors.toList());
        }
        return t;
    }

}
