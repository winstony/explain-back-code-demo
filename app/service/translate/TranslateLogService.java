package service.translate;

import java.util.Date;

import to.api.translate.QuestionForTranslation;

import com.avaje.ebean.Ebean;

import models.core.Question;
import models.translate.TranslateLog;

public class TranslateLogService {

    public void open(Long userId, QuestionForTranslation q) {
        TranslateLog translateLog = new TranslateLog();
        translateLog.action = "open";
        translateLog.questionId = q.id;
        translateLog.userId = userId;
        persist(translateLog);
    }

    public void cancel(Long userId, QuestionForTranslation q) {
        TranslateLog translateLog = new TranslateLog();
        translateLog.action = "cancel";
        translateLog.questionId = q.id;
        translateLog.userId = userId;
        persist(translateLog);
    }

    public void save(Long userId, QuestionForTranslation q) {
        TranslateLog translateLog = new TranslateLog();
        translateLog.action = "save";
        translateLog.questionId = q.id;
        translateLog.userId = userId;
        persist(translateLog);
    }

    private void persist(TranslateLog record) {
        record.time = new Date();
        Ebean.save(record);
    }

}
