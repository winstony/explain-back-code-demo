package service;

import play.libs.F.Promise;
import dao.couchbase.CouchbaseNotificationDao;
import to.api.achieve.Achievement;
import to.db.notification.AchievementNotification;
import to.db.notification.Notification;

public class NotificationsFactory {
    private static CouchbaseNotificationDao notificationsDao = new CouchbaseNotificationDao();

    public Promise<Notification> createAchievementNotification(Achievement achievement) {
        AchievementNotification notification = new AchievementNotification(achievement.id);
        notification.achievementId = achievement.id;
        notification.userId = achievement.userId;
        notification.id = "notification_" + achievement.id;
        return Promise.wrap(notificationsDao.save(notification));
    }
}
