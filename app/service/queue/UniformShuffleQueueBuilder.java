package service.queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.google.common.collect.Maps;

import to.api.HistoryElement;
import to.api.Question;

public class UniformShuffleQueueBuilder {
    public List<Question> buildQueue(
            Long userId,
            List<Question> questionsPool,
            List<HistoryElement> records,
            Integer size) {
        records = records.stream()
            .sorted((l, r) -> Long.compare(l.answeredAt, r.answeredAt))
            .collect(Collectors.toList());
        List<Question> queue = new ArrayList<>();
        Integer activeHistorySize = questionsPool.size() * 4 / 3;
        List<OrderedRecord> orderedAnswers = new ArrayList<>();
        for (int index = 0; index < records.size(); index++) {
            orderedAnswers.add(new OrderedRecord(records.get(index).question.id, index));
        }
        // System.out.println("ahs -> " + activeHistorySize + " q " + questionsPool.size());
        if (true) {
            for (int i = 0; i < size; i++) {
                // System.out.println("q-> " + queue.stream().map(q -> q.id).collect(Collectors.toList()));
                Question next = getNext(questionsPool, orderedAnswers, activeHistorySize, userId);
                queue.add(next);
                orderedAnswers.add(new OrderedRecord(next.id, orderedAnswers.size()));
            }
        } else {
            queue = nextQueue(questionsPool, orderedAnswers, activeHistorySize, userId, size);
        }
        return queue;
    }

    /**
     * This must be more efficient version but cannot compare on small dataset
     */
    private List<Question> nextQueue(
            List<Question> questionsPool,
            List<OrderedRecord> historyQueue,
            Integer activeHistorySize,
            Long userId,
            Integer size) {
        Map<Long, Question> idxQuestionById = Maps.uniqueIndex(questionsPool, q -> q.id);
        List<Question> queue = new ArrayList<>();

        Map<Long, Frequency> frequency = questionsPool.stream()
                .collect(Collectors.toMap(q -> q.id, q -> new Frequency(q.id, 0, 0)));

        List<OrderedRecord> activeHistory = getLast(historyQueue, activeHistorySize);
        activeHistory = activeHistory.stream()
                .filter(rec -> frequency.get(rec.questionId) != null)
                .collect(Collectors.toList());
        activeHistory.stream().forEach(record -> {
            Frequency f = frequency.remove(record.questionId);
            frequency.put(f.getQuestionId(), new Frequency(f.getQuestionId(), f.getCount() + 1, record.order));
        });
        Integer orderIndex = historyQueue.size();
        TreeSet<Frequency> orderedFrequency = new TreeSet<>(new FrequencyComparator());
        orderedFrequency.addAll(frequency.values());
        for (int iteration = 0; iteration < size; iteration++) {
            if (activeHistory.size() > activeHistorySize) {
                OrderedRecord record = activeHistory.get(0);
                Frequency f = frequency.remove(record.questionId);
                Frequency newFrequency = new Frequency(f.getQuestionId(), f.getCount() - 1, f.getOrder());
                frequency.put(newFrequency.getQuestionId(), newFrequency);
                orderedFrequency.remove(f);
                orderedFrequency.add(newFrequency);
                activeHistory = getLast(activeHistory, activeHistorySize);
            }

            List<Frequency> limited = orderedFrequency.stream()
                    .limit((questionsPool.size() + 2) / 3)
                    .collect(Collectors.toList());
            Integer n = limited.get(0).getCount();
            limited = limited.stream()
                    .filter(f -> f.getCount().equals(n))
                    .collect(Collectors.toList());

            Long previousIdSeed = activeHistory.isEmpty() ? 0 : activeHistory.get(activeHistory.size() - 1).questionId;
            Random random = new Random(userId + previousIdSeed);
            int index = random.nextInt(100000) % limited.size();
            Long questionId = limited.get(index).getQuestionId();

            Question next = idxQuestionById.get(questionId);
            Frequency f = frequency.remove(next.id);
            Frequency newFrequency = new Frequency(f.getQuestionId(), f.getCount() + 1, orderIndex);
            frequency.put(newFrequency.getQuestionId(), newFrequency);
            orderedFrequency.remove(f);
            orderedFrequency.add(newFrequency);

            //  --
            OrderedRecord newRecord = new OrderedRecord(next.id, orderIndex);
            orderIndex++;
            activeHistory.add(newRecord);
            queue.add(next);
        }

        return queue;
    }

    private Question getNext(List<Question> questionsPool,
            List<OrderedRecord> historyQueue,
            Integer activeHistorySize,
            Long userId) {
        List<OrderedRecord> activeHistory = getLast(historyQueue, activeHistorySize);
        Map<Long, Frequency> frequencies = new HashMap<>();
        for (Question q : questionsPool) {
            frequencies.put(q.id, new Frequency(q.id, 0, 0));
        }
        activeHistory = activeHistory.stream()
                .filter(rec -> frequencies.get(rec.questionId) != null)
                .collect(Collectors.toList());
        for (OrderedRecord order : activeHistory) {
            Frequency f = frequencies.get(order.questionId);
            frequencies.put(f.getQuestionId(), new Frequency(f.getQuestionId(), f.getCount() + 1, order.order));
        }

        List<Frequency> sortedTopThird = frequencies.values().stream().sorted(new FrequencyComparator())
                .limit((questionsPool.size() + 2) / 3)
                .collect(Collectors.toList());

        Integer minCount = sortedTopThird.get(0).getCount();
        List<Frequency> limited = sortedTopThird.stream()
                .filter(f -> f.getCount().equals(minCount))
                .collect(Collectors.toList());

        Long previousIdSeed = activeHistory.isEmpty() ? 0 : activeHistory.get(activeHistory.size() - 1).questionId;
        int index = getRandomCandidate(userId, limited.size(), previousIdSeed);
        Long questionId = limited.get(index).getQuestionId();
        return questionsPool.stream().filter(q -> q.id.equals(questionId)).findFirst().get();
    }

    private int getRandomCandidate(Long userId, int candidatesSize, Long previousIdSeed) {
        Random random = new Random(userId + previousIdSeed);
        int index = random.nextInt(100000) % candidatesSize;
        return index;
    }

    private <T> List<T> getLast(List<T> list, int size) {
        return list.subList(Math.max(0, list.size() - size), list.size());
    }

    private static class OrderedRecord {
        public OrderedRecord(Long questionId, Integer order) {
            this.questionId = questionId;
            this.order = order;
        }

        public final Long questionId;
        public final Integer order;

    }
}

class FrequencyComparator implements Comparator<Frequency> {

    @Override
    public int compare(Frequency o1, Frequency o2) {
        int countCompare = Integer.compare(o1.getCount(), o2.getCount());
        if (countCompare != 0) {
            return countCompare;
        }
        int countOrder = Integer.compare(o1.getOrder(), o2.getOrder());
        if (countOrder != 0) {
            return countOrder;
        }
        return Long.compare(o1.getQuestionId(), o2.getQuestionId());
    }

}

class Frequency {
    public Frequency(Long questionId, Integer count, Integer order) {
        this.questionId = questionId;
        this.count = count;
        this.order = order;
    }

    private final Long questionId;
    private final Integer order;
    private final Integer count;

    public Long getQuestionId() {
        return questionId;
    }

    public Integer getOrder() {
        return order;
    }

    public Integer getCount() {
        return count;
    }
}
