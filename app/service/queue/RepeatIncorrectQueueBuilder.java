package service.queue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import to.api.HistoryElement;
import to.api.Question;
import to.api.QuestionShort;

/**
 * Вопросы должны быть равномерно распределены. Но если человек уже ответил на все вопросы и на нескколько из них неправильно,
 * предпочтительней выдать вопросы на которых человек ошибся.
 *
 * @author ivinsky
 */
public class RepeatIncorrectQueueBuilder {

    private final long userIdSeed;
    private List<HistoryElement> answers;
    private List<Question> questionsPool;
    private final long activeHistoryTimeMs = 1000L * 60 * 60 * 24 * 90; // 90 days

    private List<OrderedAnswer> initialAnswers = new ArrayList<>();
    private List<Question> queue = new ArrayList<>();
    private Map<Long, AnswerInfo> stateMap; // questionId to answers info
    private LinkedList<OrderedAnswer> answersAndQueue;

    public RepeatIncorrectQueueBuilder(List<HistoryElement> answersHistory, List<Question> questionsPool, long userId) {
        userIdSeed = userId;
        // sort answers chronologically
        Set<Long> questionIds = questionsPool.stream().map(q -> q.id).collect(Collectors.toSet());
        answers = answersHistory.stream()
                .filter(a -> a.answeredAt != null)
                .filter(a -> a.isCorrect != null)
                .filter(a -> questionIds.contains(a.question.id))
                .sorted((l, r) -> Long.compare(l.answeredAt, r.answeredAt))
                .collect(Collectors.toList());
        this.questionsPool = questionsPool;
        for (Integer order = 0; order < answers.size(); order++) {
            HistoryElement record = answers.get(order);
            initialAnswers.add(new OrderedAnswer(order, record.isCorrect, record.question.id, record.answeredAt));
        }
    }

    public List<Question> buildQueue(Integer size) {
        stateMap = new HashMap<>();
        for (Question q : questionsPool) {
            stateMap.put(q.id, new AnswerInfo(q));
        }
        List<OrderedAnswer> activeAnswers = initialAnswers.stream()
                .filter(a -> a.timestamp > System.currentTimeMillis() - activeHistoryTimeMs)
                .collect(Collectors.toList());

        // Initialize state
        for (OrderedAnswer answer: activeAnswers) {
            stateMap.get(answer.questionId).answers.add(answer);
        }
        answersAndQueue = new LinkedList<>(activeAnswers);
        for (int i = 0; i < size; i++) {
            queue.add(getNext());
        }
        return queue;
    }

    // Modifies stateMap, answersAndQueue
    private Question getNext() {
        List<AnswerInfo> sortedStates = stateMap.values().stream()
                .sorted(new AnswerInfoComparator())
                .collect(Collectors.toList());
        List<AnswerInfo> zeroAnswered = sortedStates.stream()
                .filter(f -> f.answers.size() == 0)
                .collect(Collectors.toList());
        List<AnswerInfo> candidates = null;
        if (!zeroAnswered.isEmpty()) {
            candidates = zeroAnswered;
        } else {
            long incorrect = sortedStates.stream()
                    .filter(info -> !info.answers.get( info.answers.size() - 1).correct).count();
            long orderLimit = getLastElement(answersAndQueue).map(q -> q.order).orElse(0) - (incorrect + 1 / 2);
            List<AnswerInfo> forRepeat = sortedStates.stream()
                    .filter(info -> info.answers.get( info.answers.size() - 1).order <= orderLimit)
                    .filter(info -> info.answers.get( info.answers.size() - 1).correct == false)
                    .collect(Collectors.toList());
            List<AnswerInfo> topThird = sortedStates.stream()
                    .limit((questionsPool.size() + 2) / 3)
                    .collect(Collectors.toList());
            candidates = forRepeat.isEmpty() ? topThird : forRepeat;
        }
        Long previousIdSeed = getLastElement(answersAndQueue).map(q -> q.questionId)
                .orElse(answers.stream().reduce((a, b) -> b).map(r -> r.answeredAt).orElse(0L));
        int chosenIndex = getRandomCandidate(candidates.size(), previousIdSeed);
        AnswerInfo chosen = candidates.get(chosenIndex);
        OrderedAnswer enqueued = new OrderedAnswer(
                answersAndQueue.size(),
                true,
                chosen.question.id,
                System.currentTimeMillis());
        answersAndQueue.add(enqueued);
        chosen.answers.add(enqueued);
        return chosen.question;
    }

    private int getRandomCandidate(int candidatesSize, Long previousIdSeed) {
        Random random = new Random(userIdSeed + previousIdSeed);
        int index = random.nextInt(100000) % candidatesSize;
        return index;
    }

    private <T> List<T> getLast(List<T> list, int size) {
        return list.subList(Math.max(0, list.size() - size), list.size());
    }

    public <T> Optional<T> getLastElement(List<T> list) {
        return Optional.ofNullable(list != null && !list.isEmpty() ? list.get(list.size() - 1) : null);
    }

    private static class OrderedAnswer {
        int order;
        long timestamp;
        boolean correct;
        long questionId;

        public OrderedAnswer(int order, boolean correct, long questionId, long timestamp) {
            this.order = order;
            this.correct = correct;
            this.questionId = questionId;
            this.timestamp = timestamp;
        }

    }

    private static class AnswerInfo {
        List<OrderedAnswer> answers = new ArrayList<>();
        Question question;

        public AnswerInfo(Question question) {
            this.question = question;
        }
    }

    private class AnswerInfoComparator implements Comparator<AnswerInfo> {
        @Override
        public int compare(AnswerInfo o1, AnswerInfo o2) {
            if (o1.answers.isEmpty() || o2.answers.isEmpty()) {
                return Integer.compare(o1.answers.size(), o2.answers.size());
            }
            boolean correct1 = o1.answers.get(o1.answers.size() - 1).correct;
            boolean correct2 = o2.answers.get(o2.answers.size() - 1).correct;
            if (Boolean.compare(correct1, correct2) != 0) {
                return Boolean.compare(correct1, correct2);
            }
            int order1 = o1.answers.get(o1.answers.size() - 1).order;
            int order2 = o2.answers.get(o2.answers.size() - 1).order;
            return Integer.compare(order1, order2);
        }
    }

}
