package service;

import java.util.List;
import java.util.stream.Collectors;

import controllers.api.exceptions.ApiNotFoundException;
import controllers.api.exceptions.ApiPaymentRequiredException;
import controllers.api.exceptions.ApiUnauthorizedException;
import models.secure.SchoolUserTo;
import models.core.UserProduct;
import dao.UserProductDao;
import to.api.Question;
import utils.ServiceUtils;
import java.util.Optional;

public class AccessControlService {
    public static Question readQuestion(Question question) {
        if (question == null) {
            throw new ApiNotFoundException("Question not found");
        }
        if (question.open != null && question.open) {
            return question;
        }
        SchoolUserTo user = ServiceUtils.getCurrentUser();
        if (user == null) {
            throw new ApiUnauthorizedException();
        }
        String schoolAlias = question.school;
        if ((question.free != null && question.free) || hasFullAccess(schoolAlias)) {
            return question;
        }
        throw new ApiPaymentRequiredException();
    }

    private static boolean hasFullAccess(String schoolAlias) {
        SchoolUserTo user = ServiceUtils.getCurrentUser();
        UserProduct  userProduct = UserProductDao.hasUserProductNow(user.id, schoolAlias);
        if (userProduct != null) {
            //TODO: and userProduct is not deleted!
            return true;
        }
        return false;
    }

    public static List<Question> buildQuestionsPool(List<Question> questions) {
        Optional<Question> question = questions.stream().findFirst();
        String schoolAlias = question.isPresent() ? question.get().school : "";
        if (!hasFullAccess(schoolAlias)) {
            return questions.stream().filter(q -> q.free).collect(Collectors.toList());
        }
        return questions;
    }
}
