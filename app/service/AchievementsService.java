package service;

import java.util.List;
import java.util.Optional;

import controllers.api.exceptions.ApiNotFoundException;
import play.libs.F.Promise;
import to.api.achieve.Achievement;
import dao.couchbase.CouchbaseAchievementsDao;

/**
 * Получает и модифицирует ачивменты. Работает только с дао
 */
public class AchievementsService {
    private static CouchbaseAchievementsDao dao = new CouchbaseAchievementsDao();

    public Promise<Optional<Achievement>> get(String id) {
        return Promise.wrap(dao.get(id));
    }

    public Promise<Achievement> addImageUrl(String url, String id) {
        return Promise.wrap(dao.get(id)).flatMap(maybe -> {
            Achievement achievement = maybe.orElseThrow(() -> new ApiNotFoundException("no achievement with id " + id));
            achievement.imageUrl = url;
            return Promise.wrap(dao.set(achievement));
        });
    }

    public Promise<List<Achievement>> getList(long userId) {
        return Promise.wrap(dao.getList(userId));
    }

}
