package service;

import java.util.ArrayList;
import java.util.List;

import models.secure.SchoolUserTo;
import play.libs.F.Promise;
import scala.concurrent.Future;
import to.api.HistoryElement;
import dao.couchbase.CouchbaseHistoryElementDao;

public class HistoryElementsService {

    private EventsService eventsService = new EventsService();
    private CouchbaseHistoryElementDao historyDao = new CouchbaseHistoryElementDao();

    public Promise<HistoryElement> saveHistoryElement(SchoolUserTo user, HistoryElement record) {
        Promise<HistoryElement> result = saveRecord(user, record);
        result.onRedeem(r -> eventsService.onNewHistoryElement(user, r));
        return result;
    }

    public Promise<List<HistoryElement>> saveHistoryElementsList(SchoolUserTo user, HistoryElement[] records) {
        List<Promise<HistoryElement>> recordsPromise = new ArrayList<>();
        for (HistoryElement record : records) {
            recordsPromise.add(saveRecord(user, record));
        }
        for (HistoryElement record : records) {
            recordsPromise.add(saveRecord(user, record));
        }
        Promise<List<HistoryElement>> sequence = Promise.sequence(recordsPromise);
        sequence.onRedeem(list -> list.forEach(r -> eventsService.onNewHistoryElement(user, r)));
        return sequence;
    }

    private Promise<HistoryElement> saveRecord(SchoolUserTo user, HistoryElement record) {
        record.userId = user.id;
        record.userEmail = user.email;
        if (record.answeredAt == null) {
            record.answeredAt = System.currentTimeMillis();
        }
        record.schoolId = user.schoolId;
        record.isCorrect = record.chosenChoiceIds.equals(record.question.getCorrectChoiceIds());
        return Promise.wrap(historyDao.saveHistoryElement(record));
    }
}
