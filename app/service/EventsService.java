package service;

import models.secure.SchoolUserTo;
import play.libs.F.Promise;
import to.api.ChapterStats;
import to.api.HistoryElement;
import to.api.achieve.Achievement;
import to.api.achieve.ChapterFreeCompleteAchievement;
import to.db.notification.Notification;

/**
 * Логика создания ачивментов и нотификейшенов, работает только с фэктори и дао
 */
public class EventsService {
    private static NotificationsFactory notificationsFactory = new NotificationsFactory();
    private static AchievementsFactory achievementsFactory = new AchievementsFactory();

    public void onNewHistoryElement(SchoolUserTo user, HistoryElement record) {
        Promise<ChapterStats> chapterStats = new ChapterStatsFactory().updateChapterStats(
                user.id,
                user.schoolId,
                record.question.courseId,
                record.question.chapterId);
        chapterStats.onRedeem(s -> onChapterStatsUpdated(s));
    }

    private void onChapterStatsUpdated(ChapterStats chapterStats) {
        if (chapterStats.freeProgress == 100 /* TODO and no this Achievement granted */) {
            Promise<ChapterFreeCompleteAchievement> achievementSaved =
                    achievementsFactory.grantChapterFreeAchievement(chapterStats);
            Promise<Notification> notificationSaved = achievementSaved.flatMap(ach -> onAchievementSaved(ach));
            notificationSaved.flatMap(notification -> achievementSaved)
            .onRedeem(ach -> endAchievementCycle(ach));
        }
    }

    private void endAchievementCycle(ChapterFreeCompleteAchievement ach) {
        // TODO Write somewhere in user statistics that he has already been granted this achievement
    }

    public Promise<Notification> onAchievementSaved(Achievement achievement) {
        return notificationsFactory.createAchievementNotification(achievement);
    }

}
