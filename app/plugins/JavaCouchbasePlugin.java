package plugins;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import play.Application;
import play.Configuration;
import play.Logger;
import play.Logger.ALogger;
import play.Plugin;
import play.api.PlayException;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;

public class JavaCouchbasePlugin extends Plugin {

    private static final ALogger log = Logger.of("JavaCouchbasePlugin");

    private final Application app;

    private volatile Cluster cluster;

    private Map<String, String> bucketNameByAlias;

    private Map<String, Bucket> bucketByName;

    public JavaCouchbasePlugin(Application app) {
        this.app = app;
    }

    @Override
    public void onStart() {
        log.info("Starting java couchbase plugin");
        internalShutdown();
        internalSetup();
        log.info("Java couchbase plugin started");
    }

    @Override
    public void onStop() {
        log.info("Stopping java couchbase plugin");
        internalShutdown();
        log.info("Java couchbase plugin stopped");
    }

    private synchronized void internalShutdown() {
        if (cluster != null) {
            cluster.disconnect();
            cluster = null;
        }
        bucketNameByAlias = null;
        bucketByName = null;
    }

    private synchronized void internalSetup() {
        List<String> hosts = Arrays.stream(app.configuration().getString("couchbase.hosts").split(","))
                .map(String::trim)
                .collect(Collectors.toList());
        if (hosts.isEmpty()) {
            throw new PlayException(
                    "Java couchbase plugin error",
                    "Config 'couchbase.hosts' must contain valid hosts of your cluster nodes but is empty");
        }
        cluster = CouchbaseCluster.create(hosts);
        bucketNameByAlias = new HashMap<>();
        bucketByName = new HashMap<>();

        for (Configuration config : app.configuration().getConfigList("couchbase.buckets")) {
            String alias = config.getString("alias");
            String name = config.getString("bucket");
            if (bucketNameByAlias.containsKey(alias) && name.equals(bucketNameByAlias.get(alias))) {
                log.warn("Duplicate bucket definition bucket: " + name + ", alias: " + alias);
                continue;
            }
            if (bucketNameByAlias.containsValue(name)) {
                log.info("Skipped '" + alias + "' alias, already connected to bucket " + name);
                continue;
            }
            if (bucketNameByAlias.containsKey(alias)) {
                throw new PlayException(
                        "Java couchbase plugin error",
                        "Couchbase alias " + alias
                                + " is associated  with different buckets: " + name
                                + ", " + bucketNameByAlias.get(alias));
            }
            bucketNameByAlias.put(alias, name);
            bucketByName.put(name, cluster.openBucket(name, config.getString("pass")));
            log.info("Connected to couchbase bucket " + name);
        }
    }

    public Bucket getBucket(String alias) {
        return bucketByName.get(bucketNameByAlias.get(alias));
    }
}
