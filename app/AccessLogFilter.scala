
import play.api.mvc.Filter
import play.api.Logger
import play.api.mvc.RequestHeader
import scala.concurrent.Future
import play.api.mvc.Result
import scala.concurrent.ExecutionContext.Implicits.global

class AccessLogFilter extends Filter {
  def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    val startTime = System.currentTimeMillis
    nextFilter(requestHeader).map { result =>
      val endTime = System.currentTimeMillis
      val requestTime = endTime - startTime
      if (requestTime < 10 &&
              (requestHeader.uri.startsWith("/assets/")
            || requestHeader.uri.startsWith("/webjars/")) ) {
        // skip not important requests
      } else {
        Logger.info(s"${requestHeader.method} ${requestHeader.uri} " +
          s"returned status ${result.header.status} took ${requestTime} ms")
      }
      val requestWithHeader = result.withHeaders("Request-Time" -> requestTime.toString)
      if (requestHeader.uri.startsWith("/v") && requestHeader.uri.charAt(2).isDigit ) {
        requestWithHeader.as("application/json")
      }
      requestWithHeader
    }
  }

}
