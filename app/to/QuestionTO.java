package to;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuestionTO {

    // public String type = "question";

    public String text;

    @JsonProperty("quizfulId")
    public String quizfulId;

    public long id;

    public String explanation;

    public List<ChoiceTO> choices;

    public TopicTO topic;

    public String language;

    public Long courseId;

    @JsonProperty("explanation_translations")
    public List<LangString> explanationTranslations = new ArrayList<>();

    @JsonProperty("text_translations")
    public List<LangString> textTranslations = new ArrayList<>();

    public String answerType;

    // TODO if adding rules to TO check they do not create n+1 db queries
    // public List<RuleTO> rules;
}
