package to;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import models.partners.Course;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseTO {

    public String alias;

    public Long id;

    public String courseName;

    public String shortDescription;

    public Long schoolId;

    public String schoolName;

    public List<ChapterTO> chapters = new ArrayList<>();

    public TopicTO topic;

    public CourseTO(Course course) {
        this.alias = course.alias;
        this.id = course.id;
        this.courseName = course.name;
        this.schoolId = course.school.id;
        this.shortDescription = course.shortDescription;
        this.schoolName = course.school.name;
    }
}
