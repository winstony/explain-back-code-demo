package to.db.notification;

import play.libs.Json;

import com.fasterxml.jackson.databind.JsonNode;

public enum NotificationType {
    ACHIEVEMENT(AchievementNotification.class),
    NEW_COMMENT(NewCommentNotification.class);

    private final Class<? extends Notification> clazz;

    NotificationType(Class<? extends Notification> clazz) {
        this.clazz = clazz;
    }

    public Class<? extends Notification> getNotificationClass() {
        return clazz;
    }
}
