package to.db.notification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class CommentNotification extends Notification {
    private static final String DOC_TYPE = "comment_notification";

    CommentNotification() {
        // deserialization constructor
    }

    public CommentNotification(NotificationType notificationType) {
        super(DOC_TYPE, notificationType);
    }

}