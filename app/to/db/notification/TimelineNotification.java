package to.db.notification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class TimelineNotification extends Notification {
    private static final String DOC_TYPE = "timeline_notification";

    TimelineNotification() {
        // deserialization constructor
    }

    public TimelineNotification(NotificationType type) {
        super(DOC_TYPE, type);
    }
}
