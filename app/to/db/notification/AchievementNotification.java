package to.db.notification;

public class AchievementNotification extends TimelineNotification {

    public String achievementId;

    private AchievementNotification() {
        // deserialization constructor
    };

    public AchievementNotification(String achievementId) {
        super(NotificationType.ACHIEVEMENT);
        this.achievementId = achievementId;
    }

}
