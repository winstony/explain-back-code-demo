package to.db.notification;

public class NewCommentNotification extends CommentNotification {

    public String questionUuid;
    public String commentId;

    private NewCommentNotification() {
        // deserializationConstructor
    }

    public NewCommentNotification(String questionUuid, String commentId) {
        super(NotificationType.NEW_COMMENT);
        this.questionUuid = questionUuid;
        this.commentId = commentId;
    }

}