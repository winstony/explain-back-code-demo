package to.db.notification;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Notification {
    // couchbase doctype;
    private String type;
    // notification type
    private NotificationType notificationType;

    public String id;
    public Long updatedTime;
    public boolean seen;
    public boolean read;
    public Long userId;
    public Long schoolId;

    Notification() {
        // deserialization constructor;
    }

    public Notification(String docType, NotificationType notificationType) {
        this.type = docType;
        this.notificationType = notificationType;
        this.updatedTime = System.currentTimeMillis();
    }

    @JsonProperty("notificationType")
    public final NotificationType getNotificationType() {
        return notificationType;
    }

    @JsonProperty("type")
    public final String getType() {
        return type;
    }

}
