package to;

public class LangString {
    public String language;
    public String text;

    // deserialization constructor
    public LangString() {

    }
    public LangString(String language, String text) {
        this.language = language;
        this.text = text;
    }
}
