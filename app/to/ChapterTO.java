package to;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChapterTO {
    public Long id;
    public String name;
    public Integer level;
    public Boolean free;
    public List<QuestionTO> questions;
}
