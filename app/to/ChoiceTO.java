package to;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChoiceTO {

    public long id;

    @JsonProperty("choiceText")
    public String text;

    public String explanation;

    public boolean correct;

    public List<LangString> explanationTranslations = new ArrayList<>();

    public List<LangString> textTranslations = new ArrayList<>();

}

