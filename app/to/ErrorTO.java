package to;

public class ErrorTO {
    public int code;
    public String message;

    public ErrorTO(int codeNumber, String messageText) {
        code = codeNumber;
        message = messageText;
    }
}