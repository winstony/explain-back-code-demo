package to;

import com.fasterxml.jackson.annotation.JsonProperty;
import models.core.Tag;

import java.util.ArrayList;
import java.util.List;

public class RuleTO {
    public Long id;
    public String name;

    @JsonProperty("tag_ids")
    public List<Long> tagIds;

    public RuleTO() {}

    public RuleTO(Long id, String name, List<Tag> tags) {
        this.id = id;
        this.name = name;
        tagIds = new ArrayList<>();
        for(Tag tagItem : tags) {
            tagIds.add(tagItem.id);
        }
    }
}
