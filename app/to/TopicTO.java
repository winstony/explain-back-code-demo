package to;

import com.fasterxml.jackson.annotation.JsonProperty;
import models.core.Topic;

import java.util.List;

public class TopicTO {
    @JsonProperty("topic_id")
    public Long topicId;

    @JsonProperty("topic_name")
    public String topicName;

    public List<CourseTO> courses;

    public TopicTO(Topic topic, List<CourseTO> courses) {
        this.topicId = topic.id;
        this.topicName = topic.title;
        this.courses = courses;
    }
}
