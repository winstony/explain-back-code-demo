package to.api;

import java.util.List;

import to.db.notification.Notification;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class Notifications {

    public List<Notification> list;

    @JsonUnwrapped
    public NotificationsCount counts;
}
