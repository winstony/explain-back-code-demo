package to.api;

public class ApiError {
    public int code;
    public Object description;

    public ApiError(int errorCode, Object description) {
        this.code = errorCode;
        this.description = description;
    }
}
