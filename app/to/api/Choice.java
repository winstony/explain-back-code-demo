package to.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Choice {
    public String choiceText;
    public Boolean correct;
    public Integer id;

    public List<String> validate() {
        List<String> errors = new ArrayList<>();
        if (id == null) {
            errors.add("id is required");
        }
        if (choiceText == null) {
            errors.add("choiceText is required");
        }
        if (correct == null) {
            errors.add("correct is required");
        }
        return errors;
    }
}
