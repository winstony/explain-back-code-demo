package to.api.translate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Status;
import to.ChoiceTO;
import to.LangString;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionForTranslation {
    public String text;

    public Status status;

    public Long id;

    public String uuid;

    public String explanation;

    public List<ChoiceTO> choices;

    public String language;

    public Date lastEditedAt;

    public List<LangString> explanationTranslations = new ArrayList<>();

    public List<LangString> textTranslations = new ArrayList<>();

    public String answerType;

    public String topic;
}
