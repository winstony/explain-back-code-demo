package to.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tag {
    public Integer id;
    public String name;

    public List<String> validate() {
        List<String> errors = new ArrayList<>();
        if (id == null) {
            errors.add("id is required");
        }
        if (name == null) {
            errors.add("name is required");
        }
        return errors;
    }
}
