package to.api;

public enum AnswerType {
    MULTI_CHOICE,
    SINGLE_CHOICE,
    TEXT;
}
