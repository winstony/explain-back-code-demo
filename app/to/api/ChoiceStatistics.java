package to.api;

import java.util.HashMap;
import java.util.Map;

public class ChoiceStatistics {
    public Long questionId;
    public Integer total = 0;
    public Integer correct = 0;
    public Map<Integer, Integer> choices = new HashMap<>();

    public ChoiceStatistics() {

    }

    public ChoiceStatistics(Long questionId) {
        this.questionId = questionId;
    }
}
