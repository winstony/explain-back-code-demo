package to.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationsCount {
    public Integer unseen = 0;
    public Integer unread = 0;
}
