package to.api.convert;

import org.apache.commons.lang3.StringUtils;

import to.api.*;
import utils.ServiceUtils;

import java.util.stream.Collectors;

public class ApiConverter {

    public static Chapter to(models.core.Chapter chapter) {
        Chapter to = new Chapter();
        to.id = chapter.id;
        to.level = chapter.level;
        to.name = chapter.name;
        to.free = chapter.free;
        to.alias = chapter.alias;
        return to;
    }

    public static Course to(models.partners.Course course) {
        Course to = new Course();
        to.alias = course.alias;
        to.id = course.id;
        to.name = course.name;
        to.description = course.description;
        to.shortDescription = course.shortDescription;
        if (course.chapters != null) {
            to.chapters = course.chapters.stream().map(chapter -> to(chapter)).collect(Collectors.toList());
        }
        return to;
    }

    public static Topic to(models.core.Topic topic) {
        Topic to = new Topic();
        to.alias = topic.alias;
        to.name = topic.title;
        return to;
    }

    public static TestRule to(models.core.TestRule t, String language, boolean withTags) {
        TestRule to = new TestRule();
        to.id = t.id.intValue();
        to.topic = t.topic.alias;
        if (language.equals(t.language)) {
            to.rule = t.rule;
        } else {
            to.rule = t.translations.stream()
                    .filter(tt -> language.equals(tt.language))
                    .map(tt -> tt.rule).findFirst().orElse(null);
        }
        if (withTags && t.tags != null && !t.tags.isEmpty()) {
            to.tags = t.tags.stream()
                    .map(tag -> to(tag, language))
                    .collect(Collectors.toList());
        }
        return to;
    }

    public static Tag to(models.core.Tag t, String language) {
        Tag to = new Tag();
        to.id = t.id.intValue();
        if (language.equals(t.language)) {
            to.name = t.name;
        } else {
            to.name = t.translations.stream()
                    .filter(tt -> language.equals(tt.language))
                    .map(tt -> tt.name).findFirst().orElse(null);
        }
        return to;
    }

    public static QuestionShort toQuestionShort(models.core.Question q) {
        String language = ServiceUtils.getLang();
        QuestionShort qto = new QuestionShort();

        qto.id = q.id;
        qto.questionId = q.id;
        qto.uuid = q.uuid;
        qto.free = q.free;
        qto.open = q.open;
        qto.chapter = q.chapter.alias;
        qto.course = q.course.alias;
        qto.school = q.course.school.alias;
        qto.language = language;
        qto.courseId = q.courseId;
        qto.chapterId = q.chapterId;
        qto.schoolId = q.course.schoolId;

        String questionText = null;
        if (language.equals(q.language)) {
            questionText = q.question;
        } else {
            questionText = q.translations.stream()
                    .filter(qt -> language.equals(qt.language))
                    .map(qt -> qt.text).findFirst().orElse(null);
        }
        if (questionText != null) {
            String normalized = StringUtils.normalizeSpace(questionText);
            qto.shortText = normalized.substring(0, Math.min(26, normalized.length()));
        }
        return qto;
    }

    public static Question to(models.core.Question q) {
        String language = ServiceUtils.getLang();
        Question qto = new Question();

        qto.id = q.id;
        qto.uuid = q.uuid;
        qto.free = q.free;
        qto.open = q.open;
        qto.chapter = q.chapter.alias;
        qto.course = q.course.alias;
        qto.school = q.course.school.alias;
        qto.quizfulId = q.quizfulId;
        qto.language = language;
        qto.setAnswerType(q.questionType);
        qto.courseId = q.courseId;
        qto.chapterId = q.chapterId;
        if (language.equals(q.language)) {
            qto.explanation = q.explanation;
            qto.text = q.question;
        } else {
            qto.text = q.translations.stream()
                    .filter(qt -> language.equals(qt.language))
                    .map(qt -> qt.text).findFirst().orElse(null);
            qto.explanation = q.translations.stream()
                    .filter(qt -> language.equals(qt.language))
                    .map(qt -> qt.explanation).findFirst().orElse(null);
        }
        qto.choices = q.choices.stream()
                .map(c -> to(c, q.language, language))
                .collect(Collectors.toList());
        qto.rules = q.rules.stream()
                .map(r -> to(r, language, false))
                .collect(Collectors.toList());
        return qto;
    }

    private static Choice to(models.core.Choice c, String questionLanguage, String language) {
        Choice to = new Choice();
        to.correct = c.correct;
        to.id = c.id.intValue();
        if (language.equals(questionLanguage)) {
            to.choiceText = c.getText();
        } else {
            to.choiceText = c.translations.stream()
                    .filter(ct -> language.equals(ct.language))
                    .map(ct -> ct.text).findFirst().orElse(null);
        }
        return to;
    }
}
