package to.api;

import java.util.List;

public class UserAnswer {
    public Long questionId;
    public List<Choice> choices;
}