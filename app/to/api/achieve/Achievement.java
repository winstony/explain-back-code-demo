package to.api.achieve;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class Achievement {

    // Couchbase doctype
    @JsonProperty("type")
    private String type = "achievement";

    private AchievementType achievementType;

    public String id;

    public Long userId;
    public String imageUrl;
    public Long schoolId;
    public Long courseId;
    public Long chapterId;
    public Long timestamp;

    public Achievement(AchievementType achievementType) {
        this.achievementType = achievementType;
    }

    // deserialization constructor
    private Achievement() {

    }

    @JsonProperty("achievementType")
    public AchievementType getAchievementType() {
        return achievementType;
    }

}
