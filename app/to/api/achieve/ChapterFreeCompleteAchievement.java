package to.api.achieve;

public class ChapterFreeCompleteAchievement extends Achievement {

    public ChapterFreeCompleteAchievement() {
        super(AchievementType.CHAPTER_FREE_COMPLETE);
    }

}
