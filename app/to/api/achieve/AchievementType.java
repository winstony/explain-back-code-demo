package to.api.achieve;

public enum AchievementType {
    CHAPTER_FREE_COMPLETE(ChapterFreeCompleteAchievement.class);

    private Class<? extends Achievement> clazz;

    AchievementType(Class clazz) {
        this.clazz = clazz;
    }

    public Class<? extends Achievement> getAchievementClass() {
        return clazz;
    }
}
