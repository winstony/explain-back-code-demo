package to.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChapterStats {

    @JsonProperty(value = "type")
    public final String type = "chapter_stats";

    public long chapterId;
    public long courseId;
    public long userId;

    public int progress;
    public int freeProgress;

    public ChapterStats(long userId, long courseId, long chapterId) {
        this.chapterId = chapterId;
        this.courseId = courseId;
        this.userId = userId;
    }
}
