package to.api;

import java.util.List;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Chapter {
    public Long id;
    public String alias;
    public String name;
    public Boolean free;
    public Integer level;
    public List<Question> questions;

    // progress of this chapter by user
    public int progress;
    public boolean open = true;
    public ItemStatus status;
}
