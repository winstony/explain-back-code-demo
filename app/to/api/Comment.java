package to.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import models.secure.SchoolUserTo;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = Include.NON_NULL)
public class Comment {
    @JsonProperty("type")
    public final String docType = "comment";
    public String id; // couchbaseId
    public String parentId;
    public CommentType commentType = CommentType.TEXT; // "text" or "like", default is text
    public SchoolUserTo user; // is filled on back-end
    public Long questionId;
    public Long schoolId; // is filled on back-end
    public String text; // only for type "text"
    public Boolean isPositive; // only for type "like"
    public Long timestamp; // createdAt
    public Long editedAt;
    public boolean deleted;
    public QuestionShort question;

    @JsonInclude(value=Include.NON_EMPTY)
    public SortedSet<Comment> childs = new TreeSet<>();

    public List<String> validate() {
        List<String> errors = new ArrayList<>();
        if (commentType == CommentType.TEXT && StringUtils.isBlank(text)) {
            errors.add("text is required for comment type TEXT");
        }
        if (commentType == CommentType.LIKE && isPositive == null) {
            errors.add("isPositive is required for comment type LIKE");
        }
        if (commentType == CommentType.LIKE && parentId == null) {
            errors.add("parentId is required for comment type LIKE");
        }
        return errors;
    }
}
