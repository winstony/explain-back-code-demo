package to.api;

public class Stats {
    public Integer score;
    public Long lastTime;
    public Boolean lastCorrect;
    public Integer total;
    public Integer correct;

    // key
    public Long userId;
    public Long courseId;
    // public Long chapterId;
    public Long questionId;

}
