package to.api;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItemStatus {

    public Long courseId;
    public Long chapterId;

    public long lastAnswerTimestamp;

    public int numberOfQuestions;
    public int numberOfQuestionsSucceeded;
    public int numberOfQuestionsFailed;

    public int numberOfAnswers;
    public int numberOfCorrectAnswers;

    public int freePercent = 100;

    public int level;

    @JsonProperty("progress")
    public Integer getProgress() {
        if (numberOfQuestions == 0) {
            return 0;
        }
        return numberOfQuestionsSucceeded * 100 / numberOfQuestions;
    }

    @JsonProperty("freeProgress")
    public Integer getFreeProgress() {
        if (numberOfQuestions == 0) {
            return 0;
        }
        return numberOfQuestionsSucceeded * freePercent / numberOfQuestions;
    }

    public ItemStatus(Long courseId, Long chapterId) {
        this.chapterId = chapterId;
        this.courseId = courseId;
    }

    public ItemStatus() {

    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ItemStatus)) {
            return false;
        }
        ItemStatus that = (ItemStatus) obj;
        return Objects.equals(courseId, that.courseId)
                && Objects.equals(chapterId, that.chapterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseId, chapterId);
    }

    @JsonProperty("nextOpen")
    public boolean getNextOpen() {
        return freePercent * 3 <= 4 * getProgress();
    }

}
