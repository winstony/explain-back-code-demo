package to.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryElement {

    @JsonProperty(value = "type")
    public final String type = "history_element";

    @JsonProperty(value = "id")
    public String couchbaseId;

    public Question question;
    public Long answeredAt;
    public Set<Integer> chosenChoiceIds;
    public Boolean isCorrect;
    public Integer score;
    public Integer topicId;

    @Deprecated
    public String userEmail;

    public Long userId;

    public Long schoolId;

    /**
     * Use this to validate user input
     */
    public List<String> validate() {
        List<String> errors = new ArrayList<>();
        if (question == null) {
            errors.add("question is required");
        } else {
            question.validate().forEach(error -> errors.add("question." + error));
        }
        if (chosenChoiceIds == null) {
            errors.add("chosenChoiceIds is required");
        }
        return errors;
    }

}
