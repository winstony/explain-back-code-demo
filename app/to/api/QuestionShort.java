package to.api;

import java.util.Objects;

public class QuestionShort {

    public Long schoolId;
    public Long courseId;
    public Long chapterId;
    public Long questionId;
    public Boolean free;

    public String school;
    public String course;
    public String chapter;
    public String topic;

    public String language;
    public Boolean open;

    public Long id; // duplicate for compatibility
    public String uuid;

    public String shortText;

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof QuestionShort)) {
            return false;
        }
        QuestionShort that = (QuestionShort) obj;
        return Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
