package to.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Course {
    public String alias;
    public String name;
    public Long id;
    public String description;
    public String shortDescription;
    public List<Chapter> chapters;
}
