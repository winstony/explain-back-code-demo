package to.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestRule {
    public String rule;
    public Integer id;
    public List<Tag> tags;
    public String topic;

    public List<String> validate() {
        List<String> errors = new ArrayList<>();
        if (id == null) {
            errors.add("id is required");
        }
        if (rule == null) {
            errors.add("rule text is required");
        }
        return errors;
    }
}
