package to.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Question {
    private AnswerType answerType;
    @JsonIgnore private String answerTypeString;
    public String text;
    public List<Choice> choices;
    public List<TestRule> rules;
    public String explanation;
    @Deprecated public Long id;

    @Deprecated public String quizfulId;
    @Deprecated public Long courseId;
    @Deprecated public Long chapterId;
    @Deprecated public Integer topicId;

    public String language;
    public Boolean free;
    public String uuid;
    public Boolean open;

    public String school;
    public String course;
    public String chapter;
    public String topic;

    public List<String> validate() {
        List<String> errors = new ArrayList<>();
        if (id == null) {
            errors.add("id is required");
        }
        if (answerType == null) {
            if (answerTypeString == null ) {
                errors.add("answerType is required");
            } else {
                errors.add("answerType is unknown: '" + answerTypeString + "'");
            }
        }
        if (text == null) {
            errors.add("text is required");
        }
        if (choices == null) {
            errors.add("choices are required");
        } else {
            for (Choice choice: choices) {
                choice.validate().forEach(error -> errors.add("choice." + error));
            }
        }
        if (chapterId == null) {
            errors.add("chaptrId is required");
        }
        if (courseId == null) {
            errors.add("courseId is required");
        }
        return errors;
    }

    @JsonProperty
    public void setAnswerType(String answerType) {
        this.answerTypeString = answerType;
        try {
            this.answerType = AnswerType.valueOf(answerType);
        } catch (IllegalArgumentException | NullPointerException e) {
            // nothing to do here
        }
    }

    public String getAnswerType() {
        return answerType.name();
    }

    @JsonIgnore
    public Set<Integer> getCorrectChoiceIds() {
        return choices.stream()
                .filter(c -> c.correct)
                .map(c -> c.id)
                .collect(Collectors.toSet());
    }
}
