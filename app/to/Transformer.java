package to;

import java.util.stream.Collectors;

import models.core.Choice;
import models.core.Question;
import models.core.Tag;
import models.core.TestRule;
import models.core.User;

public class Transformer {

    @Deprecated
    public static QuestionTO convert(Question q) {
        QuestionTO qto = new QuestionTO();
        qto.id = q.id;
        qto.quizfulId = q.quizfulId;
        qto.explanation = q.explanation;
        qto.text = q.question;
        qto.choices = q.choices.stream().map(c -> convert(c))
                .sorted((c1, c2) -> Integer.compare(c1.text.hashCode(), c2.text.hashCode()))
                .collect(Collectors.toList());
        // qto.topic = new TopicTO(q.topic == null ? new Topic() : q.topic, Collections.emptyList());
        // qto.rules = q.rules.stream().map(r -> convert(r)).collect(Collectors.toList());
        q.translations.forEach(translation -> {
            qto.explanationTranslations.add(new LangString(translation.language, translation.explanation));
            qto.textTranslations.add(new LangString(translation.language, translation.text));
        });
        return qto;
    }

    public static ChoiceTO convert(Choice c) {
        ChoiceTO cto = new ChoiceTO();
        cto.id = c.id;
        cto.explanation = c.explanation;
        cto.text = c.getText();
        cto.correct = c.correct;
        c.translations.forEach(translation -> {
            cto.explanationTranslations.add(new LangString(translation.language, translation.explanation));
            cto.textTranslations.add(new LangString(translation.language, translation.text));
        });
        return cto;
    }

    public static RuleTO convert(TestRule r) {
        RuleTO rto = new RuleTO();
        rto.id = r.id;
        rto.name = r.rule;
        rto.tagIds = r.tags.stream().map(tag -> tag.id).collect(Collectors.toList());
        return rto;
    }

    public static UserTO convert(User user) {
        UserTO to = new UserTO();
        to.fullName = user.fullname;
        return to;
    }

    public static TagTO convert(Tag tag) {
        TagTO to = new TagTO();
        to.name = tag.name;
        return to;
    }

    public static QuestionTO convert(Question q, String language) {
        QuestionTO qto = new QuestionTO();
        qto.id = q.id;
        qto.quizfulId = q.quizfulId;
        qto.language = language;
        qto.answerType = q.questionType;
        qto.explanationTranslations = null;
        qto.textTranslations = null;
        qto.courseId = q.courseId;
        if (language.equals(q.language)) {
            qto.explanation = q.explanation;
            qto.text = q.question;
        } else {
            qto.text = q.translations.stream()
                    .filter(qt -> language.equals(qt.language))
                    .map(qt -> qt.text).findFirst().orElse(null);
            qto.explanation = q.translations.stream()
                    .filter(qt -> language.equals(qt.language))
                    .map(qt -> qt.explanation).findFirst().orElse(null);
        }
        qto.choices = q.choices.stream()
                .map(c -> convert(c, q.language, language))
                .collect(Collectors.toList());
        return qto;
    }

    private static ChoiceTO convert(Choice c, String questionLanguage, String language) {
        ChoiceTO cto = new ChoiceTO();
        cto.correct = c.correct;
        cto.id = c.id;
        cto.explanationTranslations = null;
        cto.textTranslations = null;
        if (language.equals(questionLanguage)) {
            cto.explanation = c.explanation;
            cto.text = c.getText();
        } else {
            cto.text = c.translations.stream()
                    .filter(ct -> language.equals(ct.language))
                    .map(ct -> ct.text).findFirst().orElse(null);
            cto.text = c.translations.stream()
                    .filter(ct -> language.equals(ct.language))
                    .map(ct -> ct.text).findFirst().orElse(null);
        }
        return cto;
    }
}
