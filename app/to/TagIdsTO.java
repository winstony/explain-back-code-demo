package to;

import com.fasterxml.jackson.annotation.JsonProperty;
import models.core.Tag;

import java.util.ArrayList;
import java.util.List;

public class TagIdsTO {
    @JsonProperty("tag_ids")
    public List<Long> tagIds;

    public TagIdsTO() {}

    public TagIdsTO(List<Tag> tags) {
        tagIds = new ArrayList<>();
        for(Tag tagItem : tags) {
            tagIds.add(tagItem.id);
        }
    }
}