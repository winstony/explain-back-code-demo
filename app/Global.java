import static utils.ControllerUtils.apiNotFound;
import play.Application;
import play.Configuration;
import play.GlobalSettings;
import play.Logger;
import play.Mode;
import play.Play;
import play.libs.Akka;
import play.libs.F;
import play.libs.Json;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;
import scala.concurrent.duration.Duration;
import scheduler.SchedulerBase;
import scheduler.fixture.CouchbaseIndexDefinitionUpdater;
import securesocial.core.RuntimeEnvironment;
import security.securesocial.service.MyEnvironment;
import service.cache.JedisBackedCache;
import to.db.notification.Notification;
import utils.ControllerUtils;
import utils.jackson.NotificationDeserializer;

import java.io.File;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.parse4j.Parse;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import config.ObjectMapperHolder;
import controllers.api.exceptions.ApiException;

public class Global extends GlobalSettings {

    private static final Logger.ALogger log = Logger.of(Global.class);

    private RuntimeEnvironment env = new MyEnvironment();

    @Override
    public <T extends play.api.mvc.EssentialFilter> Class<T>[] filters() {
        return new Class[] {AccessLogFilter.class, ApiErrorHandlerFilter.class};
    }

    @Override
    public Configuration onLoadConfig(Configuration config, File path, ClassLoader classloader, Mode mode) {
        Config modeConf = ConfigFactory.load("application." + mode.toString().toLowerCase() + ".conf");
        Properties systemProperties = System.getProperties();
        systemProperties.put("net.spy.log.LoggerImpl", "net.spy.memcached.compat.log.SLF4JLogger");
        if (mode == Mode.DEV) {
            systemProperties.put("cbclient.viewmode", "development");
        } else {
            systemProperties.put("cbclient.viewmode", "production");
        }
        System.setProperties(systemProperties);
        return new Configuration(modeConf.withFallback(config.underlying()));
    }

    @Override
    public void onStart(Application app) {
        Json.setObjectMapper(ObjectMapperHolder.getInstance());

        new CouchbaseIndexDefinitionUpdater().initializeViews();
        JedisBackedCache.init();
        SchedulerBase.initSchedules();
        log.info("Application has started");
    }

    @Override
    public void onStop(Application app) {
        log.info("Application shutdown...");
    }

    @Override
    public F.Promise<Result> onHandlerNotFound(RequestHeader request) {
        if (request.accepts("application/json")) {
            String slashWarning = (request.uri().toString().endsWith("/")) ? ". Remove trailing slash" : "";
            return F.Promise.pure(apiNotFound("Route " + request.uri().toString() + " not found" + slashWarning));
        }
        if (Play.isProd()) {
            return F.Promise.<Result>pure(Results.notFound(views.html.errors.page404.render()));
        } else {
            return super.onHandlerNotFound(request);
        }
    }

    @Override
    public F.Promise<Result> onBadRequest(RequestHeader request, String error) {
        if (request.accepts("application/json")) {
            return F.Promise.pure(ControllerUtils.apiBadRequest(Collections.singletonList(error)));
        }
        return super.onBadRequest(request, error);
    }

    @Override
    public <A> A getControllerInstance(Class<A> controllerClass) throws Exception {
        A result;
        try {
            result = controllerClass.getDeclaredConstructor(RuntimeEnvironment.class).newInstance(env);
        } catch (NoSuchMethodException e) {
            // the controller does not receive a RuntimeEnvironment, delegate creation to base class.
            result = super.getControllerInstance(controllerClass);
        }
        return result;
    }
}
