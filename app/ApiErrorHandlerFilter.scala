
import play.api.mvc.Filter
import play.api.Logger
import play.api.mvc.RequestHeader
import scala.concurrent.Future
import play.api.mvc.Result
import scala.concurrent.ExecutionContext.Implicits.global
import controllers.api.exceptions.ApiException
import play.libs.F

class ApiErrorHandlerFilter extends Filter {
  def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    nextFilter(requestHeader).recover {
      case e: ApiException => {
        Logger.debug("Caught error in filter: " + e.getClass.toString() + " " + e.getMessage)
        e.httpResult().toScala()
      }
    }
  }
}
