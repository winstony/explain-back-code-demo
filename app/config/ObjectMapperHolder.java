package config;

import to.api.achieve.Achievement;
import to.db.notification.CommentNotification;
import to.db.notification.Notification;
import to.db.notification.TimelineNotification;
import utils.jackson.AchievementDeserializer;
import utils.jackson.CommentNotificationDeserializer;
import utils.jackson.NotificationDeserializer;
import utils.jackson.TimelineNotificationDeserializer;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class ObjectMapperHolder {
    private static final ObjectMapper INSTANCE;

    static {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.setSerializationInclusion(Include.NON_NULL);
        SimpleModule deserializers = new SimpleModule("Deserializers", new Version(1, 0, 0, null, null, null))
                .addDeserializer(Notification.class, new NotificationDeserializer())
                .addDeserializer(TimelineNotification.class, new TimelineNotificationDeserializer())
                .addDeserializer(CommentNotification.class, new CommentNotificationDeserializer())
                .addDeserializer(Achievement.class, new AchievementDeserializer());
        mapper.registerModule(deserializers);
        INSTANCE = mapper;
    }

    public static ObjectMapper getInstance() {
        return INSTANCE;
    }
}
